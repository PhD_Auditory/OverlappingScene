%% ===== CALCULATE INFERENCE STATS =====

%% ===== INPUT OUTPUT =====

root        = '/home/jnfn/Work/Projects/Auditory/OverlappingScene/Data/';
subject     = {'Subject_Nicholas', 'Subject_Mary', 'Subject_Giorgio', 'Subject_Martina', 'Subject_Folco', 'Subject_Arianna'... 
                'Subject_ChiaraDP', 'Subject_Daniele', 'Subject_Elio', 'Subject_Yara', 'Subject_Angela'};
block       = {'Block_1_enve.75/', 'Block_2_enve.75/', 'Block_3_enve.75/'};
NewAn       = 'NewAn_11';

load([root, NewAn, '/subClean.mat']);
fs = dir([root, NewAn, '/', 'stat*.mat']);
fg = dir([root, NewAn, '/', 'EXPstat*.csv']);

for ls = 1:length(fs)
    load([root, NewAn, '/', fs(ls).name])
%     load([root, NewAn, '/', fg(l).name])
end

for lg = 1:length(fg)
    readtable([root, NewAn, '/', fg(lg).name])
end


%% ===== REACTION TIMES =====

%% ===== CALCULATE REPEATED MEASURES ANOVA FOR REACTION TIMES =====

% === PREPARE THE DATA FOR THE FITRM FUNCTION === 
% one subject per row and one condition per column, conditions emerge from the interaction between the factor levels

    % create a table representing the conditions from the first subj stats
    condTable = table(statCueTarget{1}.cueValidity, statCueTarget{1}.targetID, 'VariableNames', {'cueValidity', 'targetScene'});
    
    % Create a table reflecting the within subject factors 'CueValidity' and 'targetID' and their levels
    factorNames = {'cueValidity', 'targetScene'};
    within = table( {'INV'; 'INV'; 'VAL'; 'VAL'; 'NEU'; 'NEU';}, {'BAC'; 'FOR'; 'BAC'; 'FOR';'BAC'; 'FOR'},'VariableNames',factorNames);    

    % Create tables to store the respones and the standard errors for each subject
        
    for s = 1:length(subClean)
        
%         rowNames2{s} = ['S_', s]
        
        s_mean_rt(s,:)      = statCueTarget{s}.mean_reactionTime'
        s_sem_RT(s,:)       = statCueTarget{s}.sem_reactionTime
    end

    
    varNames = {'IB', 'IF', 'VB', 'VF', 'NB', 'NF' };
    rowNames = {'S_01', 'S_02', 'S_03', 'S_04', 'S_05', 'S_06', 'S_07', 'S_08', 'S_09', 'S_10', 'S_11'};
    tdata = array2table(s_mean_rt, 'VariableNames', varNames, 'RowNames', rowNames );
    
    
% === fit the repeated measures model and call the reapeated measures anova ===

    rm_RT           = fitrm(tdata,'IB-NF~1','WithinDesign',within);
    ranovatblRT     = ranova(rm_RT, 'WithinModel', 'cueValidity*targetScene');
    
    
% === make pairwise comparisons for the two-way interactions ===

    resultsMultCompRT = multcompare(rm_RT,'cueValidity','By','targetScene');    
    
    
% === save table to HD ===
    
%     writetable(ranovatblRT, [root, NewAn, '/ranovatblRT','.csv']); % a standard table output from a matlab function has probleam been written byt the writetable function??

    save([root, NewAn, '/ranovatblRT'], 'ranovatblRT');
    

%% ===== CALCULATE T-TEST FOR REACTION TIMES =====

% === RUN THE T-TEST ===

    [h_RT_I, p_RT_I, ci_RT_I, stats_RT_I] = ttest(s_mean_rt(:,1), s_mean_rt(:,2));
    [h_RT_V, p_RT_V, ci_RT_V, stats_RT_V] = ttest(s_mean_rt(:,3), s_mean_rt(:,4));
    [h_RT_N, p_RT_N, ci_RT_N, stats_RT_N] = ttest(s_mean_rt(:,5), s_mean_rt(:,6));
    
    
%% ===== PLOT THE GRAND MEAN OF THE DATA AND STANDARD ERRORS PER CONDITIONS WITHIN SUBJECTS =====

% === CALCULATE 

% calculate standard error on transformed data to give 'within-subject standard error'
ws_se = std(s_mean_rt, 'omitnan') ./ sqrt(size(s_mean_rt,1));

% Plot data with within-subject standard error bars and stats
plotStatsRT = figure;
ylabel('RT');
xlabel('Scene');
xlim([0.8, 2.2]);
xticks([1 2]);
xticklabels({'background', 'foreground'});
hold on; 

errorbar([mean(s_mean_rt(:,1)) mean(s_mean_rt(:,2))], ws_se(1:2), 'linewidth', 2); 
errorbar([mean(s_mean_rt(:,3)) mean(s_mean_rt(:,4))], ws_se(3:4), 'r--', 'linewidth', 2);
errorbar([mean(s_mean_rt(:,5), 'omitnan') mean(s_mean_rt(:,6))], ws_se(5:6), 'g-.', 'linewidth', 2);
legend('Invalid', 'Valid', 'Neutral', 'Location', 'NorthWest', 'fontname', 'helvetica');
title(['Interaction RT : p = ' num2str(round(ranovatblRT.pValue(7)*1000)/1000)], 'fontsize', 20, 'fontname', 'helvetica'); 
text(1.1, 0.86, [ '* p = ' num2str(round(p_RT_I*1000)/1000)], 'fontsize', 15, 'fontname', 'helvetica'); 
text(1.1, 0.80, [ '* p = ' num2str(round(p_RT_V*1000)/1000)], 'fontsize', 15, 'fontname', 'helvetica');
text(1.1, 0.74, [ '* p = ' num2str(round(p_RT_N*1000)/1000)], 'fontsize', 15, 'fontname', 'helvetica');

saveas(plotStatsRT, [root, NewAn, '/plotStatsRT'], 'svg');
    
    
%% ===== CALCULATE REPEATED MEASURES ANOVA FOR ACCURACY =====

% === PREPARE THE DATA FOR THE FITRM FUNCTION === 
% one subject per row and one condition per column, conditions emerge from the interaction between the factor levels

    % create a table representing the conditions from the first subj stats
    condTable = table(statCueTarget{1}.cueValidity, statCueTarget{1}.targetID, 'VariableNames', {'cueValidity', 'targetScene'});
    
    % Create a table reflecting the within subject factors 'CueValidity' and 'targetID' and their levels
    factorNames = {'cueValidity', 'targetScene'};
    within = table( {'INV'; 'INV'; 'VAL'; 'VAL'; 'NEU'; 'NEU';}, {'BAC'; 'FOR'; 'BAC'; 'FOR';'BAC'; 'FOR'},'VariableNames',factorNames);    

    % Create tables to store the respones and the standard errors for each subject
        
    for s = 1:length(subClean)
        
%         rowNames2{s} = ['S_', s]
        
        s_perc_accuracy(s,:)        = statCueTarget{s}.perc_Accuracy';
        s_sem_perc_accuracy(s,:)    = statCueTarget{s}.perc_sem_accu;
        
    end
    
    varNames = {'IB', 'IF', 'VB', 'VF', 'NB', 'NF' };
    rowNames = {'S_01', 'S_02', 'S_03', 'S_04', 'S_05', 'S_06', 'S_07', 'S_08', 'S_09', 'S_10', 'S_11'};
    tdata = array2table(s_perc_accuracy, 'VariableNames', varNames, 'RowNames', rowNames );
    
    
% === fit the repeated measures model and call the reapeated measures anova ===

    rm_ACC          = fitrm(tdata,'IB-NF~1','WithinDesign',within);
    [ranovatblACC, A, C, D]    = ranova(rm_ACC, 'WithinModel', 'cueValidity*targetScene');    
    
    
% === make pairwise comparisons for the two-way interactions ===

    resultsMultCompACC = multcompare(rm_ACC,'cueValidity','By','targetScene');

    
% === save table to HD ===
    
%     writetable(ranovatblACC, [root, NewAn, '/ranovatblACC.txt']);  % a standard table output from a matlab function has probleam been written byt the writetable function??

    save([root, NewAn, '/ranovatblACC'], 'ranovatblACC');
    
%% ===== CALCULATE T-TEST FOR ACCURACY =====

% === RUN THE T-TEST ===

    [h_ACC_I, p_ACC_I, ci_ACC_I, stats_ACC_I] = ttest(s_perc_accuracy(:,1), s_perc_accuracy(:,2));
    [h_ACC_V, p_ACC_V, ci_ACC_V, stats_ACC_V] = ttest(s_perc_accuracy(:,3), s_perc_accuracy(:,4));
    [h_ACC_N, p_ACC_N, ci_ACC_N, stats_ACC_N] = ttest(s_perc_accuracy(:,5), s_perc_accuracy(:,6));
    
    
%% ===== PLOT THE GRAND MEAN OF THE DATA AND STANDARD ERRORS PER CONDITIONS WITHIN SUBJECTS =====

% === CALCULATE 

% calculate standard error on transformed data to give 'within-subject standard error'
ws_se = std(s_perc_accuracy) ./ sqrt(size(s_perc_accuracy,1));

% Plot data with within-subject standard error bars and stats
plotStatsACC = figure; 
ylabel('Accuracy');
xlabel('Scene');
xlim([0.8, 2.2]);
xticks([1 2]);
xticklabels({'background', 'foreground'});
hold on;  

errorbar([mean(s_perc_accuracy(:,1)) mean(s_perc_accuracy(:,2))], ws_se(1:2), 'linewidth', 2); 
errorbar([mean(s_perc_accuracy(:,3)) mean(s_perc_accuracy(:,4))], ws_se(3:4), 'r--', 'linewidth', 2);
errorbar([mean(s_perc_accuracy(:,5)) mean(s_perc_accuracy(:,6))], ws_se(5:6), 'g-.', 'linewidth', 2);
legend('Invalid', 'Valid', 'Neutral', 'Location', 'NorthWest', 'fontname', 'helvetica');
title(['Interaction Accuracy: p = ' num2str(round(ranovatblACC.pValue(7)*1000)/1000)], 'fontsize', 20, 'fontname', 'helvetica'); 
text( 1.1, 21, ['* p = ' num2str(round(p_ACC_I*1000)/1000)], 'fontsize', 15, 'fontname', 'helvetica'); 
text( 1.1, 57, ['* p = ' num2str(round(p_ACC_V*1000)/1000)], 'fontsize', 15, 'fontname', 'helvetica');
text( 1.1, 41, ['* p = ' num2str(round(p_ACC_N*1000)/1000)], 'fontsize', 15, 'fontname', 'helvetica'); 

saveas(plotStatsACC, [root, NewAn, '/plotStatsACC'], 'svg');

