#!/bin/bash

# this script computes FreeSurfer cortical surface recontruction of MRI images taking a .nii file as an input (should work with raw DCM folders as well, just delete the .nii file extension from the last line)
# no input required
# 2018.02.09 luca,ronconi


export SUBJECTS_DIR=/mnt/storage/tier2/davmel/MAHBRA001T6F/LabData/Luca/MEG_exp/MRI/
# if you cannot specify ‘-b y’ as an argument of qsub, you can replace the line above with a call to another script (. extra_script.sh) that contain only the line above:
#. fs_var_set.sh

recon-all -i /mnt/storage/tier2/davmel/MAHBRA001T6F/LabData/Luca/MEG_exp/MRI/s"$participant_ID".nii -s /mnt/storage/tier2/davmel/MAHBRA001T6F/LabData/Luca/MEG_exp/MRI/s"$participant_ID" -all

