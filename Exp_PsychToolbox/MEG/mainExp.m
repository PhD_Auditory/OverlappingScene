%% ===== MAIN SCRIPT TO RUN THE EXPERIMENT AND CALL THE OTHER FUNCTIONS =====

% AUTHOR: GIORGIO MARINATO @ CIMeC - University of Trento
% DATE : 30/09/2017
% LAST MODS : 

% CREDITS: 



%% ===== OVERVIEW OF THE EXPERIMENT ===== %%

% comments here for the future

%% ===== GETTING READY ===== %%

clear all;                       % Clean Workspace
clc;                             % Clean command windos
AssertOpenGL;                    % Make sure the script is running on Psychtoolbox-3

% === INPUT GLOBAL INFOS ===
	
	global  subjectID runID recordEyes;
	
	% Input: subject number 				
	subjectID = input('SubjectID: ','s'); 	% enter subject id number
	subjectNR = str2double(subjectID);
	
	% Input Block 
	runID = input('Block: ', 's'); 			% enter block number
	StartBlockNR = str2double(runID);

	% input Eye Tracker
	recordEyes = input('recordEyes using eyelink? Press [Y] for "Yes", [N] for "No": ' ,'s');


%% ===== INPUT PARAMETERS (e.g. keyboard and paths) ===== %%

% === KEYBOARD MAPPING ===

 	% Enable unified mode of KbName, so KbName accepts identical key names on
	% all operating systems:
    KbName('UnifyKeyNames');                   

%     escapeKey = KbName('Escape');  
    quitKey = KbName('q'); 
    ResponseButton = KbName('Space');
%     RestrictKeysForKbCheck([escapeKey, quitkey, ResponseButton]);    
    
%     if strcmp(recordEyes,'y')
        calibrate = KbName('c');
        validate  = KbName('v');
%     end
    
    activeKeys = [quitKey, ResponseButton, calibrate, validate];
    keysOfInterest=zeros(1,256);  
    keysOfInterest(activeKeys)=1;  
   
    KbQueueCreate([], keysOfInterest);
    KbQueueStart;    
       
% === EXPERIMENT PARAMETERS ===
% the rest of the parameters are in the functions

	ntrials = 60;                  	% number of trial
	nblocks = 1;                    % one block at a time

%% ===== FILE HANDLING (input and output) ===== 

% === INPUT DIRECTORY === 

    BgroundFolder       = ('C:\Users\gm_standard\Documents\OverlappingScene\NewSounds2\Background\Ambient_Human_Places\5sec_cut\repSounds_enveloped_75');
    FgroundFolder       = ('C:\Users\gm_standard\Documents\OverlappingScene\NewSounds2\Foreground\5sec_cut\repSounds_enveloped_75');
     
    BcleanFolder        = ('C:\Users\gm_standard\Documents\OverlappingScene\NewSounds2\Background\Ambient_Human_Places\5sec_cut\clean5sec_enveloped_75');
    FcleanFolder        = ('C:\Users\gm_standard\Documents\OverlappingScene\NewSounds2\Foreground\5sec_cut\clean5sec_enveloped_75');

    
% === OUTPUT DIRECTORY ===
       
    outdir= 'C:\Users\gm_standard\Documents\OverlappingScene\Data\';
             
       
% === SUBJECT DIRECTORY ===

    subjectDir = [outdir, 'Subject_', subjectID]; % make subject folder
    mkdir(subjectDir);
    
    
% === RUN DIRECTORY ===

    runDir = [subjectDir, '\Block_', runID]; % make block folder
    mkdir(runDir);
    
%% ===== BASIC PSYCHTOOLBOX SETTING =====

	% Force KbCheck GetSecs WaitSecs into memory to avoid latency later on:
    KbCheck;
    GetSecs;
    WaitSecs(0.1);

    % Set priority
    Priority(2);                        % 0 = normal; 1 = high priority 2 = real time priority
    %priorityLevel = MaxPriority(w);
    %Priority(priorityLevel);
    
    % open PsychPortAudio for eyelink functions that use beeper
    InitializePsychSound(1);

    if ~IsLinux
    PsychPortAudio('Verbosity', 10);
    end

    devices = PsychPortAudio('GetDevices');
    pahandle = PsychPortAudio('Open', [], [], [], [], 1);
    
%% ===== SET THE SCREEN =====

	% init a struct to store parameters for screen
	scr = struct;

    % Screen Preferences
%        scr.whichScreen = max(Screen('Screens'));
        scr.whichScreen = 1; % in MEG lab
        
        scr.framerate = Screen(scr.whichScreen,'FrameRate');
        fprintf('screenNumber= %d and framerate=%d\n', scr.whichScreen, scr.framerate)
        Screen('Preference', 'SkipSyncTests', 0);                       % 1=skip! Use the value 1 with caution
%        Screen('Preference', 'VisualDebuglevel', 3)                     % No white startup screen
%        Screen('Preference', 'VBLTimestampingMode', 3);                 % Add this to avoid timestamping problems

    % Screen text preference    
        Screen('Preference', 'DefaultFontName', 'Geneva');
        Screen('Preference', 'DefaultFontSize', 20);                    % fontsize
        Screen('Preference', 'DefaultFontStyle', 0);                    % 0=normal,1=bold,2=italic,4=underline,8=outline,32=condense,64=extend,1+2=bold and italic.
        Screen('Preference', 'DefaultTextYPositionIsBaseline', 1);      % align text on a line
        
    % 'w' is the handle used to direct all drawing commands to that window.
    % 'screenRect' is a rectangle defining the size of the window.
    % See "help PsychRects" for help on such rectangles and useful helper functions:
        [w, scr.screenRect] = Screen('OpenWindow', scr.whichScreen); 	% remove the rect parameter for real experiment		, [], [0 0 640 480]    

    % get the flip interval of the monitor
    	scr.flipint         = Screen('GetFlipInterval', w);
    	
    % measure monitor for eyeLink
    	[scr.width, scr.height] = Screen('WindowSize', w);
    
	% Colors definition
		scr.white 	= [255 255 255]; 
		scr.black 	= [0 0 0]; 
		scr.grey 	= [150 150 150];
        scr.darkGr  = [75 75 75];
		scr.red 	= [255 0 0];
		scr.green 	= [0 255 0];   
     
    % hide the mouse cursor   
        HideCursor;                         
	
%% ===== RUN EXPERIMENT ===== %%

% Embed core of code in try ... catch statement. If anything goes wrong
% inside the 'try' block (Matlab error), the 'catch' block is executed to
% clean up, save results, close the onscreen window etc. This will often 
% prevent you from getting stuck in the PTB full screen mode.

% try 

%% ===== START BLOCK =====

for nb = 1:nblocks
    try
% === CALL FUNCTION TO PREPARE STIMULI'S RANDOM LIST ===

        % prepare a random list of sounds scene at every block    
        [expStruct] = stimList(BgroundFolder, FgroundFolder, BcleanFolder, FcleanFolder);


%% ===== INITIALIZE DATAPIXX === 

        % open connection, set digital out to 0, init audio, init response buttons
        rbMask = datapixx_INIT;


%% ===== INITIALIZE EYELINK =====

        if strcmp(recordEyes,'y')

            % init defaults
            el = EyelinkInitDefaults(w);

            % Call the function to set all the parameters
            el = eyelink_INIT( el, w, scr );

            % open file to record data to
            edfFile=[subjectID,'_', runID '.edf'];
            success = Eyelink('Openfile', edfFile)
            
            if success ~= 0; 
                error('Failed to create EDF file: ''%s'' ', edfFile); 
            end

% === CALIBRATE EYELINK ===

            % Calibrate the eye tracker
            EyelinkDoTrackerSetup(el);

            % do a final check of calibration using driftcorrection
            EyelinkDoDriftCorrection(el);

            eye_used = Eyelink('EyeAvailable');

            % make sure we're still connected.
            if Eyelink('IsConnected')~=1
                return;
            end;

        end


%% ===== RUN THE EXPERIMENT ===== %%

% === WRITE INITIAL INSTRUCTIONS ===

        % fill the screen
        Screen('FillRect', w, scr.grey);

        % write instruction message for subject (The special character '\n' introduces a line-break): 
        beginningMessage = ['Please Pay Attention To The Visual Cues' ... 
                            '\n \n \n \n Answer With A Button Press As Soon As You Hear A Repetion' ... 
                            '\n \n Press A Button When You Are Ready To Start']; 
        %  visual cues are indicating where more likely you are going to hear a repetition in a stream of sounds';

        % Draw the message on screen
        DrawFormattedText(w, beginningMessage, 'center', 'center', scr.black);    
        Screen('Flip', w);

        % wait for button press to proceed
%         [secs, keyCode] = KbWait;
% 
%         while keyCode(ResponseButton) == 0
%                 [secs, keyCode] = KbWait;
%         end

        Datapixx('SetDinLog');              % Configure logging with default values
        Datapixx('StartDinLog');
        Datapixx('RegWrRd');
        
        buttons = 0
        while ~buttons
            
            Datapixx('RegWrRd');
            status = Datapixx('GetDinStatus');
            
            if status.newLogFrames>0
                [buttons] = Datapixx('ReadDinLog',1);
                pressed = bitand(buttons,rbMask);
            end
        end
        
        Datapixx('StopDinLog'); %D 
            

% === CALL THE PRESENTATION FUNCTION ===

            onsetExpe=GetSecs; % time at the beginning of the experiment  
            
            if strcmp(recordEyes,'y')
                
                [RT, expStruct] = stimPres_DAC(expStruct, ntrials, w, scr, rbMask, el, pahandle);
            else
                [RT, expStruct] = stimPres_DAC(expStruct, ntrials, w, scr, rbMask);
            end
            
            

%% ===== SAVE =====

        % Write saving message  
            Screen('TextSize', w, 20);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect', w, scr.grey);

            saveMessage = 'End Of The Run \n \n \n Wait For Data Saving And Session Closing Routines';
            DrawFormattedText(w, saveMessage, 'center', 'center', scr.black);
            Screen('Flip', w);


        % save the Struct
            save([runDir,'/expData_',subjectID, runID, '.mat'], 'expStruct', '-v7.3');
            save([runDir,'/expRT_',subjectID, runID, '.mat'], 'RT', '-v7.3');

        % convert struct as table without the sounds
            data = struct2table(expStruct);
            dataNoSounds = data(:,{'cueValidity','cueDir', 'targetID','targetName','targetLog', 'notargetName', 'reactionTime', 'trialOnsetTime', 'soundOnsetTime', 'dPixxTarget'});

            writetable(dataNoSounds,[runDir,'/expData_',subjectID, runID, '.txt']);    

        % Duration of the experiment: 
            offsetExpe = GetSecs; 					% time at the end of the experiment
            DurExpe = (offsetExpe-onsetExpe)/60; 	% compute duration of the run  


%% ===== FINISH EYELINK RECORDING, SAVE FILE =====
        if strcmp(recordEyes,'y')

            % Finish up: stop recording eye-movements,
            % close graphics window, close data file and shut down tracker
            Eyelink('Command', 'set_idle_mode');
            WaitSecs(0.5);
            Eyelink('CloseFile');
            % Download data file
    %             try
                fprintf('Receiving data file ''%s''\n', edfFile );
                status=Eyelink('ReceiveFile');
                if status > 0
                    fprintf('ReceiveFile status %d\n', status);
                end
                if 2==exist(edfFile, 'file')
                    fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, runDir );                    
                    WaitSecs(0.5);
%                     movefile([pwd filesep edfname],edfdir);
                end
    %             catch
                fprintf('Problem receiving data file ''%s''\n', edfFile );
    %             end

            Eyelink('ShutDown');

        end

%% ===== CLOSE =====

        % Write ending message  
            Screen('TextSize', w, 20);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect', w, scr.grey);

            endMessage = 'Thanks For Participating \n \n \n Press "Q" To Quit';  
            DrawFormattedText(w, endMessage, 'center', 'center', scr.black);
            Screen('Flip', w);

            [secs, keyCode] = KbWait;
                while keyCode(quitKey) == 0
                        [secs, keyCode] = KbWait;
                end


%% === DATAPixx CLOSE ===

        Datapixx('Close');


%% ===== CLEAN UP ===== 

        % Cleanup at the end of a regular session...
            ShowCursor;
            Screen('CloseAll');
            PsychPortAudio('Close', pahandle)
            fclose('all');
            Priority(0);


    % Catch error: this is executed in case something goes wrong in the
    % 'try' part due to programming error etc...
    % if PTB crashes it will land here, allowing us to reset the screen to normal.
    catch    
    disp(['CRITICAL ERROR: ' lasterr ])
    disp(['Exiting program ...'])

    % Do same cleanup as at the end of a regular session...
        ShowCursor;
        Screen('CloseAll');
        PsychPortAudio('Close', pahandle)
        fclose('all');
        Priority(0);

    % Output the error message that describes the error:
        psychrethrow(psychlasterror);

    end % try
end % for (blocks)

