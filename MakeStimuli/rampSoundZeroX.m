function [ rmpSnd , ramp ] = rampSoundZeroX( inputSound, rampDirection, startSample, stopSample, rampDur)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% better to do with case switch and nargin to have a defenitive ramping
% function also for a simple case when zero crosing is not necessary

% AUTHOR: Giorgio Marinato @ CIMeC - University of Trento


    % length of sound
    inLength = 1:(length(inputSound));
	
	% start, stop and length of the ramping and the input sound in samples
    startS = startSample
    stopS  = stopSample
    rampDur = stopS - startS  
    inDur = (length(inputSound));   
    
    % ramping vector
    ramp = ones(size(inLength));	
    
    % ramp up 
    if strcmp (rampDirection, 'rampUp')        
        ramp(inLength<rampDur) = linspace(0,1,sum(inLength<rampDur));

    % ramp down
    elseif strcmp (rampDirection, 'rampDown')        
        ramp(inLength>inDur-rampDur) = linspace(1,0,sum(inLength>inDur-rampDur));
    
    % ramp starts at 0, becomes 1 over the course of the rampDur and ramps down again at the end
    % here the stopSmaple is used as starting point for the ramp Down
    elseif strcmp (rampDirection, 'rampUpandDown')
        startS = startSample
        stopS  = stopSample
            
        rampDur1 = length(inputSound(1:startS,:))
        rampDur2 = length(inputSound(stopS:end,:))
        
        ramp(inLength<rampDur1) = linspace(0,1,sum(inLength<rampDur1));
        ramp(inLength>inDur-rampDur2) = linspace(1,0,sum(inLength>inDur-rampDur2));    
    end
    
    % sound ramped
    rmpSnd = inputSound.*ramp';

end

