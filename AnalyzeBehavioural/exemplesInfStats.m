%% ===== EXAMPLES =====

% generate random data for the example
alpha_power = randn(24,8);

% Create a table storing the respones
varNames = {'Y1','Y2','Y3','Y4','Y5','Y6','Y7','Y8'};
t = array2table(alpha_power,'VariableNames',varNames);

% Create a table reflecting the within subject factors 'TestCond', 'Attention', and 'TMS' and their levels
factorNames = {'TestCond','Attention','TMS'};
within = table({'M';'M';'M';'M';'N';'N';'N';'N'},{'A';'A';'V';'V';'A';'A';'V';'V'},{'T';'S';'T';'S';'T';'S';'T';'S'},'VariableNames',factorNames);

% fit the repeated measures model
rm = fitrm(t,'Y1-Y8~1','WithinDesign',within);

% run my repeated measures anova here
[ranovatbl] = ranova(rm, 'WithinModel','TestCond*Attention*TMS');

% make pairwise comparisons for the two-way interactions
%
% see: help RepeatedMeasuresModel/multcompare
multcompare(rm,'TestCond','By','Attention')
multcompare(rm,'TestCond','By','TMS')
multcompare(rm,'Attention','By','TMS')

% but how can I make pairwise comparisons for the 3-way interaction?
%
% this does not work (it ignores the 'Attention' factor)
multcompare(rm,'TestCond','By','Attention','By','TMS')

%%

% Suppose we want to compare levels of Attention for each combination
% of levels of TestCond and TMS.

% 1. Convert factors to categorical.
within2 = within;
within2.Attention = categorical(within2.Attention);
within2.TestCond = categorical(within2.TestCond);
within2.TMS = categorical(within2.TMS);

% 2. Create an interaction factor capturing each combination of levels
% of TestCond and TMS.
within2.TestCond_TMS = within2.TestCond .* within2.TMS;

% 3. Call fitrm with the modified within design.
rm2 = fitrm(t,'Y1-Y8~1','WithinDesign',within2);
ranovatbl2 = ranova(rm2, 'WithinModel','TestCond*Attention*TMS')

% 4. Use interaction factor TestCond_TMS as the 'By' variable in multcompare.
multcompare(rm2,'Attention','By','TestCond_TMS')

