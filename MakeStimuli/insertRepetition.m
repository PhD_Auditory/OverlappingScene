function [logRepSound, repSound, lastChunkRamp] = insertRepetition(inputSound, Fs, repLength)
%% ===== RANDOMLY INSERT A REPETITION INSIDE AN AUDIO FILE ====
% insert the sound, the sample frequency and the desired length of the repeated segment
% N.B. the function call other custom functions ("zeroCrossing", "rampSound", "crossFade")  

% AUTHOR: GIORGIO MARINATO @ CIMeC - University of Trento
% DATE: 26/09/2017


if length(inputSound) ~= 220500
    return        
end


%% ===== DEFINE THE SOUND SEGMENT THAT WILL BE REPEATED =====
% maybe define here more clearly the segments

	startRepeatChunk    = randi([Fs, (3.45*Fs)]);
	chunkLength         = repLength*Fs;
	stopRepeatChunk     = startRepeatChunk + chunkLength;

	soundSegment        = inputSound(startRepeatChunk:stopRepeatChunk-1,:);
    
    firstChunk          = inputSound(1:stopRepeatChunk-1,:);
    repSegment          = soundSegment;
    lastChunk           = inputSound(stopRepeatChunk+chunkLength:end,:);
    
    nSamplesWin         = 220;

 %% ===== ZERO CROSSING  VERSION =====
% 
% 	% select the very last zero crossing sample between up and down at the
% 	% preferred time (expressed in samples)
%     % then compute the first zero crossing of the next segment according to previous directon
% 		  
% 	[ s1_indx_up, s1_indx_down, best_s1, direction ] = zeroCrossing(firstChunk, 'last', 220);
%     
%     if strcmp (direction, 'indx_up') %direction == 'indx_up'
%         s1_0X 							= s1_indx_up;
%         [sSegment_fst_0X, ~, ~, ~]      = zeroCrossing(repSegment, 'first', 220);
% 
%     else        
%         s1_0X 							= s1_indx_down;
%         [~, sSegment_fst_0X, ~, ~]		= zeroCrossing(repSegment, 'first', 220);
%     end
%     
% 	% \\ compute the zero crossing for the repetead sound segment	
% 	[ sSegment_indx_up, sSegment_indx_down, best_sSegment, direction ] = zeroCrossing(repSegment, 'last', 220);
% 	
%     if strcmp (direction, 'indx_up') %direction == 'indx_up'
%         sSegment_lst_0X 				= sSegment_indx_up;
%         [s2_0X, ~, ~, ~]				= zeroCrossing(lastChunk, 'first', 220);
% 
%     else        
%         sSegment_lst_0X 				= sSegment_indx_down;
%         [~, s2_0X, ~, ~]				= zeroCrossing(lastChunk, 'first', 220);
%     end
% 
%     s2_0X2 = s2_0X + length(inputSound(1:stopRepeatChunk+chunkLength-1,:)) % s2_0X2 = s2_0X + length([inputSound(1:s1_0X,:); soundSegment(sSegment_fst_0X:sSegment_lst_0X,:)]);
% 	
% %% ===== MIX THE NEW FILE WITH THE REPETITION INSERTED AND LOG THE REPETITION SAMPLE =====
% 	
% 	logRepSound 							= s1_0X;
%     
%     repSound            					= [inputSound(1:s1_0X,:); soundSegment(sSegment_fst_0X:sSegment_lst_0X); inputSound(s2_0X:end,:)];

% % ===== DO THE RAMPING FOR ALL THE CHUNKS =====
%     
%     firstChunkRamp                          = rampSoundZeroX( firstChunk, 'rampDown', s1_0X, length(firstChunk) );
%     repSegmentRamp                          = rampSoundZeroX( repSegment, 'rampUpandDown', sSegment_fst_0X, sSegment_lst_0X );
%     lastChunkRamp                           = rampSoundZeroX( lastChunk, 'rampUp', 1, s2_0X );
%     
%% ===== DO THE RAMPING FOR ALL THE CHUNKS =====
    
    firstChunkRamp      = rampSoundSimple(firstChunk, 'rampDown', nSamplesWin);
    repSegmentRamp      = rampSoundSimple(repSegment, 'rampUpandDown', nSamplesWin );
    lastChunkRamp       = rampSoundSimple(lastChunk, 'rampUp', nSamplesWin);
    
%% ===== CONCATENATE AND CROSSFADE ALL THE CHUNKS =====
            
    firstCrossFade      = firstChunkRamp(end-nSamplesWin+1:end)+repSegmentRamp(1:nSamplesWin);
    
    if length(lastChunkRamp) <= nSamplesWin
        secondCrossFade = repSegmentRamp(end-nSamplesWin+1:end)+lastChunkRamp(1:length(lastChunkRamp));
        
    else        
    secondCrossFade     = repSegmentRamp(end-nSamplesWin+1:end)+lastChunkRamp(1:nSamplesWin);
    end
    
    repSound            = [firstChunkRamp(1:end-nSamplesWin+1); firstCrossFade; repSegmentRamp(nSamplesWin:end-nSamplesWin+1); secondCrossFade; lastChunkRamp(nSamplesWin:end)];
    logRepSound         = stopRepeatChunk;

end
