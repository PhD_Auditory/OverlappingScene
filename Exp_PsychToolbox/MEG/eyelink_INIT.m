function [el] = eyelink_init(el, w, scr)

% AUTHOR: GIORGIO MARINATO @ CIMeC - University of Trento
%
%-------------------------------------------------------------------------------
    
    
    % here more or less following the steps in the EyelinkExample.m in Pychtoolbox
    
%% ===== STEP 1 - OPEN A SCREEN WINDOW ===== 
    % was opening a window with Screen and I did in the "set the
    % screen" section
    
            
%% ===== STEP 2 - INIT DEFAULTS =====
    % Provide Eyelink with details about the graphics environment
    % and perform some initializations. The information is returned
    % in a structure that also contains useful defaults
    % and control codes (e.g. tracker state bit and Eyelink key values).
    % el=EyelinkInitDefaults(w);
    % already done in the "initialize eye link" section
    
    
%% ===== STEP 3 - OPEN CONNECTION =====
    % Initialization of the connection with the Eyelink Gazetracker.
    % exit program if this fails.
    if EyelinkInit()~= 1;
        return;
    end;
    
    % jasper's way
%    if Eyelink('Initialize', 'PsychEyelinkDispatchCallback') ~=0
%		error('Eyelink initialization failed...')
%	end
    
    % get the version of the Eyelink to which we're connected
    [v vs]=Eyelink('GetTrackerVersion');
    fprintf('Running experiment on a ''%s'' tracker.\n', vs );

    % make sure that we get gaze data from the Eyelink
    Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');    
    Eyelink('command', 'add_file_preamble_text ''Recorded by EyelinkToolbox demo-experiment''');
    

%% ===== STEP 4 - CALIBRATION =====
	% Better if done outside after this initialization
	
%    % Calibrate the eye tracker
%    EyelinkDoTrackerSetup(el);

%    % do a final check of calibration using driftcorrection
%    EyelinkDoDriftCorrection(el);

    
%% ===== STEP 5 - TRACKER CONFIGURATION =====
    % Setting the proper recording resolution, proper calibration type, 
    % as well as the data file content;
    
    % Set standard settings
	Eyelink('Command', 'binocular_enabled = YES');
	Eyelink('Command', 'active_eye = BINOCULAR');
	Eyelink('Command', 'sample_rate = 1000');
%	Eyelink('Command', 'pupil_size_diameter = DIAMETER');
%	Eyelink('Command', 'corneal_mode = YES');
%	Eyelink('Command', 'use_ellipse_fitter = NO');
%	Eyelink('Command', 'initial_thresholds = 66, 66, 40, 210, 210')	
    
% === SET CALIBRATION COORDINATES ===

	% map gaze position from tracker to screen pixel positions
    Eyelink('command', 'screen_pixel_coords = %ld %ld %ld %ld', 0, 0, scr.width-1, scr.height-1);
    Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, scr.width-1, scr.height-1);
                    
    % set default calibration type.
    Eyelink('command', 'calibration_type = HV9');
    Eyelink('Command', 'generate_default_targets = YES');
    
    % general calibration settings
	Eyelink('Command', 'enable_automatic_calibration = YES');	% YES default
	Eyelink('Command', 'automatic_calibration_pacing = 1000');	% 1000 ms default
	Eyelink('Command', 'randomize_calibration_order = YES');    % YES default
	Eyelink('Command', 'randomize_validation_order = YES');     % YES default
	Eyelink('Command', 'cal_repeat_first_target = YES');
	Eyelink('Command', 'val_repeat_first_target = YES');
	
	% setup the proper calibration foreground and background colors
    el.backgroundcolour = 67;
    el.foregroundcolour = 200;
    
     
% === SET RECORDING CONFIGURATION ===

	% set parser (conservative saccade thresholds)
    Eyelink('command', 'saccade_velocity_threshold = 35');
    Eyelink('command', 'saccade_acceleration_threshold = 9500');
        
    % set EDF file contents
    Eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
    Eyelink('command', 'file_sample_data  = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS');
    
    % set link data (used for gaze cursor)
    Eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
    Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS');
    
    % allow to use the big button on the eyelink gamepad to accept the 
    % calibration/drift correction target
    Eyelink('command', 'button_function 5 "accept_target_fixation"');


    % make sure we're still connected.
    if Eyelink('IsConnected')~=1
        return;
    end;
        
       
end % function 
