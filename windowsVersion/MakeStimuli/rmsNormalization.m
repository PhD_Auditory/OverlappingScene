
% take the rms of the signal as reference
	rms_target = sqrt(mean(target.^2));
	
% convert result to decibel scale
	rms_target_dB = 20*log10(rms_target/1);	
	
% deired level on dB scale
	R_dB = -6;
	
% convert the desired level to linear scale 
	R = 10^(R_dB/20);
	
% determine linear scaling factor 
	a = sqrt((length(target)*R^2)/(sum(target.^2)));


	
% go back and scale the amplitude of the input signal with a linear gain change factor
	targetScaled = target*a 
	
	
 

