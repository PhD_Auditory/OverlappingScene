%% ===== COME BACK =====

fNorm = dir(fullfile(root, fgndFolder, '/clean5sec2/', '*.wav')); 
bNorm = dir(fullfile(root, bgndFolder, '/clean5sec2/', '*.wav'));

for n = 1:length(snd.foreground)
    
    snd.foreground(n).monoNorm         = audioread([root, fgndFolder, '/clean5sec2/', fNorm(n).name]);
    snd.background(n).monoNorm         = audioread([root, bgndFolder, '/clean5sec2/', bNorm(n).name]);
end
    
   
%% ===== CALL THE REPETITION FUNCTION =====  

    for nsnd = 1:length(snd.foreground)
        
        for ntime = 1:50
            
                [snd.foreground(nsnd).repLog{ntime}, snd.foreground(nsnd).repSound{ntime}] = insertRepetition(snd.foreground(nsnd).mono, Fs, default);
                [snd.background(nsnd).repLog{ntime}, snd.background(nsnd).repSound{ntime}] = insertRepetition(snd.background(nsnd).mono, Fs, default);            
        end       
    end
                
            
%% ===== SAVE ON DISK THE SOUNDS AND IT'S LOGS =====

    repFolder = 'repSounds2/';

    if ~exist([root, fgndFolder, repFolder], 'dir') & ~exist([root, bgndFolder, repFolder], 'dir')

            mkdir([root, fgndFolder, repFolder]);
            mkdir([root, bgndFolder, repFolder]);
    end
    
%     no_repFolder = 'clean5sec2/';
% 
%     if ~exist([root, fgndFolder, no_repFolder], 'dir') & ~exist([root, bgndFolder, no_repFolder], 'dir')
% 
%             mkdir([root, fgndFolder, no_repFolder]);
%             mkdir([root, bgndFolder, no_repFolder]);
%     end
    
% _____SAVE THE STRUCT_____

    save([root,'soundsStruct'], 'snd', '-v7.3');



    for s = 1:length(snd.foreground)
    
% _____SAVE THE SOUNDS WITH NO REPETITION_____

% 		[~, namef, ext] = fileparts(snd.foreground(s).name);
%         [~, nameb, ext] = fileparts(snd.background(s).name);
% 
%         audiowrite(sprintf([root, fgndFolder, no_repFolder, namef, '_clean5sec_mono', ext]), snd.foreground(s).mono, 44100);
%         audiowrite(sprintf([root, bgndFolder, no_repFolder, nameb, '_clean5sec_mono', ext]), snd.background(s).mono, 44100);    

        
        for n = 1:length(snd.background(s).repSound)
            
            [~, namef, ext] = fileparts(snd.foreground(s).name);
            [~, nameb, ext] = fileparts(snd.background(s).name);
            
            
            
% _____SAVE THE SOUNDS_____
    
            if n < 10
                audiowrite(sprintf([root, fgndFolder, repFolder, namef, '_repSnd_0',num2str(n), ext]), snd.foreground(s).repSound{n}, 44100);
                audiowrite(sprintf([root, bgndFolder, repFolder, nameb, '_repSnd_0',num2str(n), ext]), snd.background(s).repSound{n}, 44100);
            
% _____SAVE THE LOGS_____ 
                dlmwrite([root, fgndFolder, repFolder, namef, '_repLog_0', num2str(n), '.txt'], snd.foreground(s).repLog{n}, 'precision', 8); 
                dlmwrite([root, bgndFolder, repFolder, nameb, '_repLog_0', num2str(n), '.txt'], snd.background(s).repLog{n}, 'precision', 8);
            
            else
% _____SAVE THE SOUNDS_____            
                audiowrite(sprintf([root, fgndFolder, repFolder, namef, '_repSnd_',num2str(n), ext]), snd.foreground(s).repSound{n}, 44100);
                audiowrite(sprintf([root, bgndFolder, repFolder, nameb, '_repSnd_',num2str(n), ext]), snd.background(s).repSound{n}, 44100);
            
% _____SAVE THE LOGS_____ 
                dlmwrite([root, fgndFolder, repFolder, namef, '_repLog_', num2str(n), '.txt'], snd.foreground(s).repLog{n}, 'precision', 8); 
                dlmwrite([root, bgndFolder, repFolder, nameb, '_repLog_', num2str(n), '.txt'], snd.background(s).repLog{n}, 'precision', 8);
            end
        
        end

    end
