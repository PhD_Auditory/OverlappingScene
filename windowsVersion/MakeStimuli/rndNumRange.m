function [rndNum] = rndNumRange(startRange, stopRange, lengthVector)
%% ===== GENERATE RANDOM NUMBERS FROM A UNIFORM DISTRIBUTION WITHIN A SPECIFIED RANGE =====
% since I need it all the time, put the two limit of the range and the
% length of the vector I need

% AUTHOR: GIORGIO MARINATO @ CIMeC -UNIVERSITY OF TRENTO
% DATE: 26 SEPT 2017
% CREDITS: https://it.mathworks.com/help/matlab/math/floating-point-numbers-within-specific-range.html

a = startRange;
b = stopRange;

rndNum = (b-a).*rand(lengthVector,1) + a;

end

