function [wantedLength] = makeSameLength_Center(theSound, newlength, Fs)

% cut a certain length from the center for the samples
%
% AUTHOR: GIORGIO MARINATO @ CIMeC - UNIVERSITY OF TRENTO

        lenIdx = length(theSound); 
        chop   = newlength*Fs;        
        remain = round((lenIdx-chop)/2, 0);        
        
        wantedLength = theSound(remain:(chop+remain)-1,:);   

end