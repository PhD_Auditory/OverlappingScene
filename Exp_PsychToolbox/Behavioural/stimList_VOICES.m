function [expStruct] = stimList_VOICES(RepSpFolder, CleanSpFolder)

%% ===== HAVE ALL THE SOUNDS WITH THE RELATIVES LOGS IN MEMORY FROM WHICH I CAN RANDOMLY PICK UP 100 TRIALS EVERY BLOCK =====

	ntrials = 100


% === LIST SOUNDS AND LOGS OF SCENES WITH REPETITION ===

    all_RepSpSounds            = dir(fullfile(RepSpFolder, '*.wav'));
    all_RepSpLogs              = dir(fullfile(RepSpFolder, '*.txt')); 


    % extract random 100 trials from the list of 1000   

    randPick_RepSp              = randi(1000,100,1);


    % index the sounds and logs with the previous trial vector

    all_RepSpSounds_Array       = {all_RepSpSounds.name}; 
    all_RepSpLogs_Array         = {all_RepSpLogs.name};

    pool_RepSpSounds            = transpose(all_RepSpSounds_Array(randPick_RepSp));
    pool_RepSpLogs              = transpose(all_RepSpLogs_Array(randPick_RepSp));


    % read from hard drive

    array_RepSpSounds           = cell(ntrials,1);

    for n = 1:ntrials

        array_RepSpSounds{n}    = audioread(fullfile(RepSpFolder, pool_RepSpSounds{n}));
        array_RepSpLogs(n)      = (dlmread(fullfile(RepSpFolder, pool_RepSpLogs{n}))) / 44100; %get the values in seconds
    end

    array_RepSpLogs             = transpose(array_RepSpLogs);


% === READ THE SCENES WITHOUT REPETITION AND BUILD THE ARRAY TOO ===

    SpClean                     = dir(fullfile(CleanSpFolder, '*.wav'));


    % replicate the array to have a pool of 1000 to again pick up a pool of random 100 sounds

    all_SpClean                 = reshape(repmat({SpClean.name},[50,1]), [1000,1]);


    % extract random 100 trials from the list of 1000   

    randPick_SpClean            = randi(1000,100,1);


    % index the sounds

    pool_SpClean                = all_SpClean(randPick_SpClean);


    % read the sounds from hard drive

    array_SpClean                = cell(ntrials,1);

    for n = 1:ntrials

        array_SpClean{n}         = audioread(fullfile(CleanSpFolder, pool_SpClean{n}));
    end


%% ===== CREATE THE EXPERIMENTAL STRUCTURE WHERE ALL THE DATA ARE STORED (THEN STORE THE DATA ON DISK TOO!) =====

    expStruct                   = struct;
    tid                         = (1:100)';

% === NOW CREATE THE CUE DIRECTION AND THE TARGET DIRECTION ===

    cueDir                      = Shuffle([zeros(50, 1); ones(50,1)]); % 1 for SPEAKER A  0 for SPEAKER B
    cueValid                    = 1;
    cueInvalid                  = 0; 
	cueNeutral                  = 2;

	cueValidity                 = Shuffle([repmat(cueValid,70,1); repmat(cueInvalid,20,1); repmat(cueNeutral, 10,1)]);

	count 						= 0;


    for t = 1:length(tid)
		expStruct(t).tid                    = t;
        count                               = count +1;

		% === SPEAKER A ===

        if cueDir(t) == 1 % it's speaker A

			expStruct(t).cueDir             = cueDir(t);


			% === valid ===

			if cueValidity(t) 	== 1 % it's valid

				targetID          	= 1; % Speaker A
				targetName        	= pool_RepSpSounds(t);
				targetSound       	= array_RepSpSounds(t);
				targetLog         	= array_RepSpLogs(t);

				noTarget          	= array_SpClean(t);
				notargetName      	= pool_SpClean(t);

				cueSnippet 			= array_RepSpSounds{t}(1:1*44100);


			% === invalid ===

            elseif cueValidity(t) == 0 % it's invalid

				targetID          	= 0; % Speaker B
				targetName        	= pool_RepSpSounds(t);
				targetSound       	= array_RepSpSounds(t);
				targetLog         	= array_RepSpLogs(t);

				noTarget          	= array_SpClean(t);
				notargetName      	= pool_SpClean(t);

                cueSnippet          = array_SpClean{t}(1:1*44100);


			% === neutral ===

            elseif cueValidity(t) == 2 %it's neutral

				targetID            = 1; % Speaker A
				targetName        	= pool_RepSpSounds(t);
				targetSound       	= array_RepSpSounds(t);
				targetLog         	= array_RepSpLogs(t);

				noTarget          	= array_SpClean(t);
				notargetName      	= pool_SpClean(t);

				cueSnippet 			= [];

			end

			expStruct(t).cueValidity        = cueValidity(t);

        end


		% === SPEAKER B ===

        if cueDir(t) == 0 % it's speaker B

			expStruct(t).cueDir             = cueDir(t);


			% === valid ===

            if cueValidity(t) 	== 1 % it's valid

				targetID          	= 0; % Speaker B
				targetName        	= pool_RepSpSounds(t);
				targetSound       	= array_RepSpSounds(t);
				targetLog         	= array_RepSpLogs(t);

				noTarget          	= array_SpClean(t);
				notargetName      	= pool_SpClean(t);

				cueSnippet 			= array_RepSpSounds{t}(1:1*44100);


			% === inalid ===

            elseif cueValidity(t) == 0 % it's invalid

				targetID          	= 1; % Speaker A
				targetName        	= pool_RepSpSounds(t);
				targetSound       	= array_RepSpSounds(t);
				targetLog         	= array_RepSpLogs(t);

				noTarget          	= array_SpClean(t);
				notargetName      	= pool_SpClean(t);

				cueSnippet 			= array_SpClean{t}(1:1*44100);


			% === neutral ===

            elseif cueValidity(t) == 2 %it's neutral

				targetID            = 0; % Speaker A
				targetName        	= pool_RepSpSounds(t);
				targetSound       	= array_RepSpSounds(t);
				targetLog         	= array_RepSpLogs(t);

				noTarget          	= array_SpClean(t);
				notargetName      	= pool_SpClean(t);

				cueSnippet 			= [];

            end

			expStruct(t).cueValidity        = cueValidity(t);

        end


% === FILL UP THE REST OF THE STRUCTURE ===

		expStruct(t).targetID           = targetID;
        expStruct(t).targetName         = targetName;
        expStruct(t).targetLog          = targetLog;
        expStruct(t).targetSound        = targetSound;
        expStruct(t).notargetName       = notargetName;
        expStruct(t).noTarget           = noTarget;
        expStruct(t).cueSnippet         = cueSnippet;

    end

end
