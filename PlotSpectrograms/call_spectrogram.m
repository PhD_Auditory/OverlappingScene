function call_spectrogram(stim)

% this function is meant to pass parameters along to the spectrogram()
% function, producing spectrograms in both .svg and .jpg format from
% sounds we consider most stationary and of a length of 4-5 secs

% input (stim) is simply the name of the stimulus
% e.g. 'stim31_blender'


%% parameter values

pathname    = pwd;
subfolder   = '\Spectrograms\';
sound       = audioread(strcat(stim));
window      = 4410;
overlap     = 3000;
nfft        = 256  %2^nextpow2(length(sound));
F           = [0:5:7000];
fs          = 44100;


%% make/save spectrogram

spectrogram(sound,window,overlap,F,fs,'yaxis');

spect 			= gcf
ax 			= spect.CurrentAxes
ax.FontSize 		= 20;
colormap(spect(hot))



saveas(gcf, [pwd, subfolder, stim], 'jpeg');
saveas(gcf, [pwd, subfolder, stim], 'svg');

end

