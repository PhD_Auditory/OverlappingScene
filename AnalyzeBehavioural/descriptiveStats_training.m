%% ===== INPUT OUTPUT =====

root        = '/home/giorgio/Projects/Auditory/OverlappingScene/Data/';
subject     = 'Subject_Giorgio/';
block       = 'Block_training_enveloped_2/';
data        = 'trainData_Giorgiotraining_enveloped_2.txt';

output = [root, subject, block];
% mkdir(output)

%% ===== LOAD THE TABLE =====

    trainTable = readtable([root, subject, block, data]);

%% ===== CALCUALTE PERFORMANCE VARIABLES ===== 

    % convert the struct to table keeping the sounds for spectrograms
%     trainTableS = struct2table(trainStruct);

    % clean from ealrier response time = <0
    trainTableClean = trainTable
    
    for n = 1:length(trainTableClean.reactionTime)
        if trainTableClean.reactionTime(n)<0
            trainTableClean.reactionTime(n) = NaN
        end
    end        
    
    % you can calcualte grouping as many variableS as needed 
    perfBF = varfun(@nanmean, trainTableClean, 'Inputvariables', 'reactionTime', 'GroupingVariables',  {'cueDir'} );

%% ===== SORT THE SOUNDS BASED ON REACTION TIMES AND GROUP BY BACKGROUND AND FOREGROUND =====     
      
    % sort rows based on targetID (foreground/background) and reactionTimes
    rankedTable = sortrows(trainTableClean,[2 5]); 
    
    rankedBground = rankedTable(1:50,:);
    rankedFground = rankedTable(51:100,:);
    
%     ranked = table(rankedBground,rankedFground);

%% ===== SAVE THE RANKED STRUCT =====

    writetable(perfBF,[output, '/performance.txt']);
    writetable(rankedBground,[output, '/rankedBground.txt']);
    writetable(rankedFground,[output, '/rankedFground.txt']);       
    
%% ===== PLOT SPECTROGRAMS AS RANKED BY REACTION TIME AND TARGET ID =====  

for n = 1:50
    %for rkd = width(ranked);
        
    %% _____PLOT WAVEFORMS_____
    wave = figure(1);

    % Wave2 = subplot(211)
    plot(rankedBground.targetSound{n,:});
    title(sprintf('%s',num2str(n), '_', char(rankedBground.targetName(n))));
    xlabel('samples');
    ylabel('Scaled Normalized Amplitude')
    ax                          = wave.CurrentAxes
    ax.FontSize                 = 15
    ax.YLim                     = [-1 1]
    ax.YTick                    = [-1:.2:1]
    ax.XLim                     = [0 220500]
    ax.XTick                    = [0:0.5*44100:5*44100]


    % ax.title.FontWeight = 'normal';   
    % ax.FontSize = 30

    %% _____PLOT SPECTROGRAMS_____ 	

    % parameters
    sound       = rankedBground.targetSound{n,:}; %array_Fclean{10};
    window      = 1000;
    overlap     = 900;
    nfft        = 256;  %2^nextpow2(length(sound));
    F           = [0:5:7000];
    fs          = 44100;

    % plot spectrogram
    spect = figure(2);

    spectrogram(sound,window,overlap,F,fs,'yaxis');

    ax                  = spect.CurrentAxes;
    ax.FontSize 		= 20;
    colormap(spect,hot);
    caxis([-70 -30]);

    % save

    % saveas(gcf, [pwd, subfolder, stim], 'jpeg');
    % saveas(gcf, [pwd, subfolder, stim], 'svg');


    %% _____PREPARE COMBINED PLOT_____

%     combo = figure(3)
% 
%     combo(1) = subplot(211); 
% 
% 
%     combo(2) = subplot(212);
% 
%     % Paste figures on the subplots
%     copyobj(allchild(get(wave,'CurrentAxes')),combo(1));
%     copyobj(allchild(get(spect,'CurrentAxes')),combo(2));

    %% save
    
    filename = [output, '/', num2str(n), '_wave_', char(rankedBground.targetName(n))];    
    [pathstr,name,ext] = fileparts(filename);
    
    saveas(wave, [pathstr,name], 'svg');
    
    filename = [output, '/', num2str(n), '_spect_', char(rankedBground.targetName(n))];
    [pathstr,name,ext] = fileparts(filename);
    
    saveas(spect, [pathstr,name], 'svg');
    
    
%     saveas(combo,pwd,'jpeg')
end
    
    
%% ===== NOTES =====
    
%     % _____grouping variables
%     rkdSounds = findgroups(targetID, reactionTime);
%     
%     % _____varfun take out the NaN but made groping easier
%     rankedSounds = varfun(@sort, trainTable, 'GroupingVariables',{'reactionTime', 'targetID'})
%     
%     % _____sortrows
%     rankedSounds2 = sortrows(trainTable, 'reactionTime')
% 
%     % _____now I want to group based on target
%     [groupTarget, g] = findgroups(rankedSounds2.targetID);
%     
%     % then rank based on reaction times
%     func = @sortrows(trainTable, 'reactionTime')
%     s = splitapply(@(x)sortrows(x, 5), trainTable, groupTarget)
%     
%     func = @(trainTable) sortrows(trainTable, 'reactionTime')
%     rankBackground = splitapply(func, trainTable, g)

    