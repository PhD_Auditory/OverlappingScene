function [wantedLength] = makeSameLength_Center(theSound, newLength, Fs)

% cut a certain length from the center for the samples
%
% AUTHOR: GIORGIO MARINATO @ CIMeC - UNIVERSITY OF TRENTO

    lenIdx = length(theSound);
    
    if nargin < 3 
        chop    = newLength;
    else
        chop    = newLength*Fs;
    end  
            
    remain = round((lenIdx-chop)/2, 0);        

    wantedLength = theSound(remain:(chop+remain)-1,:);   

end