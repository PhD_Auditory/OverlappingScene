function [rbMask] = datapixx_INIT

% AUTHOR: GIORGIO MARINATO @ CIMeC - University of Trento
%
%--------------------------------------------------------------------------

% === OPEN DATAPixx ===
	
	% open connection
	Datapixx('Open');
    Datapixx('StopAllSchedules');
    
    % make sure that all the TTL digital outputs are low before we start
    Datapixx('SetDoutValues', 0);
    
    % Show how many TTL input bits are in the Datapixx
    nBits = Datapixx('GetDinNumBits');
    fprintf('\nDatapixx has %d TTL input bits\n', nBits);
    
    % Synchronize Datapixx registers to local register cache   						
    Datapixx('RegWrRd');										

% === INITIALIZE AUDIO ===
    
    Datapixx('InitAudio');
    Datapixx('SetAudioVolume', 1.00);    					% Not too loud       								
    
% === CONFIGURE DIGITAL INPUT FOR BUTTONS RESPONSE === 

    %D Datapixx('SetDinDataDirection', hex2dec('1F0000'));
	%D Datapixx('SetDinDataOut', hex2dec('1F0000'));
	%D Datapixx('SetDinDataOutStrength', 0);   				% Set brightness of buttons
    Datapixx('EnableDinDebounce');      					% Filter out button bounce
    Datapixx('SetDinLog');                                  % Log button presses to default address
    Datapixx('StopDinLog');                                 % Turn off logging
    Datapixx('RegWrRd');									% Synchronize Datapixx registers to local register cache
    
    % response button mask of unused input channels
    % when we query the ReadDinLog the logData is a row vector of
    % acquired digital input values. each column of the vector contains
    % an unsigned integer with the new state of digital inputs 0 to 15        
    rbMask = 2^0 + 2^1 + 2^2 + 2^3;                         % response triggers = red=1, yellow=2, green=4, blue=8 		
    
    
end % function
