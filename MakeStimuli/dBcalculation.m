%% calculate the decibel level

for n = 1:snd.foreground
    
    rmsVector = snd(n).foreground.monoNorm; 
    
    snd.foreground(n).rms = sqrt(mean(rmsVector.^2));
    snd.foreground(n).dB = mag2db(snd.foreground(n).rms2);
    
end

for n = 1:snd.background
    
    rmsVector = snd(n).background.monoNorm; 
    
    snd.foreground(n).rms = sqrt(mean(rmsVector.^2));
    snd.foreground(n).dB = mag2db(snd.foreground(n).rms2);
    
end
   


