%% ===== MAIN SCRIPT TO RUN THE EXPERIMENT AND CALL THE OTHER FUNCTIONS =====

% AUTHOR: GIORGIO MARINATO @ CIMeC - University of Trento
% DATE : 30/09/2017
% LAST MODS : 

% CREDITS: 



%% ======= OVERVIEW OF THE EXPERIMENT ======= %%

% comments here for the future

%% ======= GETTING READY ======= %%

clear all;                       % Clean Workspace
clc;                             % Clean command windos
AssertOpenGL;                    % Make sure the script is running on Psychtoolbox-3
rand('state',sum(100*clock));    % Reseed the random-number generator for each exp

%% ===== INPUT PARAMETERS (e.g. keyboard and paths) ===== %%

% Enable unified mode of KbName, so KbName accepts identical key names on
% all operating systems:

% _____KEYBOARD MAPPING_____
    KbName('UnifyKeyNames');

    escapeKey = KbName('Escape');  
    %quitkey = KbName('Cancel'); 
    ResponseButton = KbName('Space');
    
	RestrictKeysForKbCheck([escapeKey, ResponseButton]); %quitkey,
       
%     KbQueueCreate([], RestrictKeysForKbQueueCheck);
%     KbQueueStart;

% _____EXPERIMENT PARAMETERS_____
% the rest of the parameters are in the functions

ntrials = 100;                  % number of trial
nblocks = 1;                     % update when is ready

%% ======= FILE HANDLING (input and output) ======= %%

% Input: subject number
global  subjectID runID; % set subject variable
subjectID = input('SubjectID: ','s'); % enter subject id number
subjectNR = str2double(subjectID);
runID = input('Block: ', 's'); % enter block number
StartBlockNR = str2double(runID);

    
% ______Output directory______
   
%       outdir=['/home/giorgio/Projects/Auditory/OverlappingScene/Data/'];
       
      outdir=['/home/jnfn/Work/Projects/Auditory/OverlappingScene/Data/'];
       
       
% ______Subject directory______
    subjectDir = [outdir, 'Subject_', subjectID]; % make subject folder
    mkdir(subjectDir);
    % cd(['Subject_', subjectID]) % change directory to subject folder

% ______Run directory______
    runDir = [subjectDir, '/Block_', runID]; % make block folder
    mkdir(runDir); 
    % cd(['Block_', blockID]) % change directory to block folder
    
% _____Input directory_____ 

%     BgroundFolder       = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/Ambient_Human_Places/repSounds_enveloped.50');
%     FgroundFolder       = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/repSounds_enveloped.50');
%      
%     BcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/Ambient_Human_Places/clean5sec_enveloped.50');
%     FcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/clean5sec_enveloped.50');

    BgroundFolder       = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/Ambient_Human_Places/repSounds_enveloped');
    FgroundFolder       = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/repSounds_enveloped');
     
    BcleanFolder        = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/Ambient_Human_Places/clean5sec_enveloped');
    FcleanFolder        = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/clean5sec_enveloped');
     
%      o


%     BgroundFolder       = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/repSounds');
%     FgroundFolder       = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/repSounds');
%
%     BcleanFolder        = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/clean5sec');
%     FcleanFolder        = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/clean5sec');
%
%     BgroundFolder       = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut2/repSounds3');
%     FgroundFolder       = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut2/repSounds3');
% 
%     BcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut2/clean5sec3');
%     FcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut2/clean5sec3');



%% ===== CALL FUNCTION TO PREPARE STIMULI'S RANDOM LIST =====
% Here the alternative is to prepare 10 structure in advance and load a ready structure pre-randomized. 

for nb = 1:nblocks

    [expStruct] = stimList(BgroundFolder, FgroundFolder, BcleanFolder, FcleanFolder)

%% ===== RUN EXPERIMENT ===== %%


% Embed core of code in try ... catch statement. If anything goes wrong
% inside the 'try' block (Matlab error), the 'catch' block is executed to
% clean up, save results, close the onscreen window etc. This will often 
% prevent you from getting stuck in the PTB full screen mode.

    try

    % _____SET THE SCREEN_____

    % Screen Preferences
        whichScreen = max(Screen('Screens'));
        framerate = Screen(whichScreen,'FrameRate');
        fprintf('screenNumber= %d and framerate=%d\n', whichScreen, framerate)
        Screen('Preference', 'SkipSyncTests', 0);                       % 1=skip! Use the value 1 with caution
        Screen('Preference', 'VisualDebuglevel', 3)                     % No white startup screen
        Screen('Preference', 'VBLTimestampingMode', 3);                 % Add this to avoid timestamping problems
    % Screen text preference    
        Screen('Preference', 'DefaultFontName', 'Geneva');
        Screen('Preference', 'DefaultFontSize', 20);                    % fontsize
        Screen('Preference', 'DefaultFontStyle', 0);                    % 0=normal,1=bold,2=italic,4=underline,8=outline,32=condense,64=extend,1+2=bold and italic.
        Screen('Preference', 'DefaultTextYPositionIsBaseline', 1);      % align text on a line

    % Force KbCheck GetSecs WaitSecs into memory to avoid latency later on:
        KbCheck;
        GetSecs;
        WaitSecs(0.1);

    % Set priority
        %priorityLevel = MaxPriority(w);
        %Priority(priorityLevel);
        Priority(2);                        % set high priority for max performance

    % Colors definition
        white = [255 255 255]; 
        black = [0 0 0]; 
        red = [255 0 0];
        grey = [150 150 150];

    % 'w' is the handle used to direct all drawing commands to that window.
    % 'screenRect' is a rectangle defining the size of the window.
    % See "help PsychRects" for help on such rectangles and useful helper functions:

        [w, screenRect] = Screen('OpenWindow', whichScreen); % remove the rect parameter for real experiment		, [], [0 0 640 480]
        HideCursor;                         % hide the mouse cursor
        Screen(w, 'WaitBlanking');

    %     dispW   = screenRect(3) - screenRect(1);
    %     dispH   = screenRect(4) - screenRect(2);
    %     x0      = dispW/2;
    %     y0      = dispH/2;
    %     white = WhiteIndex(w);
    %     black = BlackIndex(w);

    % _____INITIALIZE THE AUDIO_____

        InitializePsychSound(1);

        if ~IsLinux
        PsychPortAudio('Verbosity', 10);
        end

        devices = PsychPortAudio('GetDevices');
        pahandle = PsychPortAudio('Open', [], [], [], [], 1);

    %% ===== START THE EXPERIMENT =====

    % % Check for abortion:
    %     while abortit ~= 1;
    %             [keyIsDown,secs,keyCode]=KbCheck; %#ok<ASGLU>
    %             if (keyIsDown==1 && keyCode(esc))
    %                 % Set the abort-demo flag.
    %                 abortit=2;
    %                 break;
    %             end;

    % _____WRITE INITIAL INSTRUCTIONS_____
    % Write instruction message for subject (centered, white color).  
    % The special character '\n' introduces a line-break:

        Screen('FillRect', w, grey);  
        beginningMessage = 'Pay attention. \n \n \n \n Wait for cues \n \n press the spacebar to start'; % \n \n \n \n Please pay attention to the visual cues \n \n these are indicating where more likely you are going to hear a repetition in a stream of sounds ';
        DrawFormattedText(w, beginningMessage, 'center', 'center', black);
        Screen('Flip', w); 
        [secs, keyCode] = KbWait;
        while keyCode(ResponseButton) == 0
                [secs, keyCode] = KbWait;
        end

    % _____CALL THE PRESENTATION FUNCTION_____

        onsetExpe=GetSecs; % time at the beginning of the experiment  

        [RT, expStruct] = stimPres(expStruct, ntrials, w, grey, black, pahandle);
        
    % save the Struct
        save([runDir,'/expData_',subjectID, runID, '.mat'], 'expStruct', '-v7.3');
        save([runDir,'/expRT_',subjectID, runID, '.mat'], 'RT', '-v7.3');
        
    % convert struct as table without the sounds
    	data = struct2table(expStruct);
    	dataNoSounds = data(:,{'cueValidity','cueDir', 'targetID','targetName','targetLog', 'notargetName','reactionTime'});
    	
    	writetable(dataNoSounds,[runDir,'/expData_',subjectID, runID, '.txt']);
       
    % Write ending message  
        Screen('TextSize', w, 20);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect', w, grey);  
        endmessage = 'End of the run. \n \n Press ESC';
        DrawFormattedText(w, endmessage, 'center', 'center', black);
        Screen('Flip', w);
        [secs, keyCode] = KbWait;
            while keyCode(escapeKey) == 0
                    [secs, keyCode] = KbWait;
            end

    % Duration of the experiment: 
        offsetExpe = GetSecs; % time at the end of the experiment
        DurExpe = (offsetExpe-onsetExpe)/60; % compute duration of the run  

    % Cleanup at the end of a regular session...
        ShowCursor;
        Screen('CloseAll');
        PsychPortAudio('Close', pahandle)
        fclose('all');
        Priority(0);  

    catch  % Catch error: this is executed in case something goes wrong in the
           % 'try' part due to programming error etc...
           % if PTB crashes it will land here, allowing us to reset the screen to normal.

        disp(['CRITICAL ERROR: ' lasterr ])
        disp(['Exiting program ...'])

        % Do same cleanup as at the end of a regular session...
        ShowCursor;
        Screen('CloseAll');
        PsychPortAudio('Close', pahandle)
        fclose('all');
        Priority(0);

        % Output the error message that describes the error:
        psychrethrow(psychlasterror);

    end %try
end % for (blocks)

