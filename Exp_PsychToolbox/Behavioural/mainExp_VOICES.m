%% ===== MAIN SCRIPT TO RUN THE EXPERIMENT AND CALL THE OTHER FUNCTIONS =====

% AUTHOR: GIORGIO MARINATO @ CIMeC - University of Trento
% DATE : 30/09/2017
% LAST MODS : go check the git repo


%% ======= OVERVIEW OF THE EXPERIMENT ======= %%

% comments here for the future

%% ======= GETTING READY ======= %%

    clear all;                       % Clean Workspace
    clc;                             % Clean command windos
    AssertOpenGL;                    % Make sure the script is running on Psychtoolbox-3

%% ===== INPUT PARAMETERS (e.g. keyboard and paths) ===== %%

% === KEYBOARD MAPPING ===

    % Enable unified mode of KbName, so KbName accepts identical key names on
    % all operating systems:

    KbName('UnifyKeyNames');


    % list keys of interest

    escapeKey					= KbName('Escape');  
    quitKey						= KbName('Cancel'); 
    ResponseButton				= KbName('Space');


    % whitelist of keys of interest
	
	activeKeys					= [escapeKey, quitKey, ResponseButton];
    keysOfInterest				= zeros(1,256);  
    keysOfInterest(activeKeys)	= 1;

% 	RestrictKeysForKbQueueCheck([escapeKey, ResponseButton, quitkey]);


    % put on queue

	KbQueueCreate([], keysOfInterest);
	
%     KbQueueCreate([], RestrictKeysForKbQueueCheck);


%% ===== EXPERIMENT TRAILS AND BLOCK PARAMETERS ====

% the rest of the parameters are in the functions

    ntrials = 60;						% number of trial
    nblocks = 1;						

%% ===== FILE HANDLING (input and output) ===== %%

	% Input: subject number

	global  subjectID runID;						% set subject variable
	subjectID		= input('SubjectID: ','s');		% enter subject id number
	subjectNR		= str2double(subjectID);
	runID			= input('Block: ', 's');		% enter block number
	StartBlockNR	= str2double(runID);


	% === OUTPUT DIRECTORY ===

    outdir=['/home/jnfn/Work/Projects/Auditory/OverlappingScene/Data/'];	     
% 	outdir=['/home/giorgio/Projects/Auditory/OverlappingScene/Data/'];


	% Subject directory

    subjectDir = [outdir, 'Subject_', subjectID]; % make subject folder
    mkdir(subjectDir);
    % cd(['Subject_', subjectID]) % change directory to subject folder


	% Run directory

    runDir = [subjectDir, '/Block_', runID]; % make block folder
    mkdir(runDir); 
    % cd(['Block_', blockID]) % change directory to block folder


	% === INPUT DIRECTORY === 

    RepSpFolder     = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/NewSounds2/Foreground/5sec_cut/repSounds_enveloped_75');
    CleanSpFolder   = ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/NewSounds2/Foreground/5sec_cut/clean5sec_enveloped_75');

%% ===== CALL FUNCTION TO PREPARE STIMULI'S RANDOM LIST =====

% Here the alternative is to prepare 10 structure in advance and load a ready structure pre-randomized. 

	for nb = 1:nblocks

		[expStruct] = stimList_VOICES_simple(RepSpFolder, CleanSpFolder, ntrials);

%% ===== RUN EXPERIMENT ===== %%

% Embed core of code in try ... catch statement. If anything goes wrong
% inside the 'try' block (Matlab error), the 'catch' block is executed to
% clean up, save results, close the onscreen window etc. This will often 
% prevent you from getting stuck in the PTB full screen mode.

		try

			% === SET THE SCREEN ===

			scr = struct;

			% Screen Preferences

			scr.whichScreen = max(Screen('Screens'));
			scr.framerate = Screen(scr.whichScreen,'FrameRate');
			fprintf('screenNumber= %d and framerate=%d\n', scr.whichScreen, scr.framerate)
			Screen('Preference', 'SkipSyncTests', 0);                       % 1=skip! Use the value 1 with caution
			Screen('Preference', 'VisualDebuglevel', 3)                     % No white startup screen
			Screen('Preference', 'VBLTimestampingMode', 3);                 % Add this to avoid timestamping problems


			% Screen text preference

			Screen('Preference', 'DefaultFontName', 'Geneva');
			Screen('Preference', 'DefaultFontSize', 20);                    % fontsize
			Screen('Preference', 'DefaultFontStyle', 0);                    % 0=normal,1=bold,2=italic,4=underline,8=outline,32=condense,64=extend,1+2=bold and italic.
			Screen('Preference', 'DefaultTextYPositionIsBaseline', 1);      % align text on a line


			% Force KbCheck GetSecs WaitSecs into memory to avoid latency later on:

			KbCheck;
			GetSecs;
			WaitSecs(0.1);


			% Set priority

			%priorityLevel = MaxPriority(w);
			%Priority(priorityLevel);
			Priority(2);                        % set high priority for max performance


			% Colors definition

			scr.white 	= [255 255 255]; 
			scr.black 	= [0 0 0]; 
			scr.grey 	= [150 150 150];
			scr.darkGr  = [75 75 75];
			scr.red 	= [255 0 0];
			scr.green 	= [0 255 0];


			% 'w' is the handle used to direct all drawing commands to that window.
			% 'screenRect' is a rectangle defining the size of the window.
			% See "help PsychRects" for help on such rectangles and useful helper functions:

			[w, scr.screenRect] = Screen('OpenWindow', scr.whichScreen); % remove the rect parameter for real experiment		, [], [0 0 640 480]
			HideCursor;                         % hide the mouse cursor
 			Screen(w, 'WaitBlanking');


			% === INITIALIZE THE AUDIO ===

			InitializePsychSound(1);

			if ~IsLinux
			PsychPortAudio('Verbosity', 10);
			end

			devices		= PsychPortAudio('GetDevices');
			pahandle	= PsychPortAudio('Open', [], [], [], [], 1);

%% ===== START THE EXPERIMENT =====

			% % Check for abortion:
			%     while abortit ~= 1;
			%             [keyIsDown,secs,keyCode]=KbCheck; %#ok<ASGLU>
			%             if (keyIsDown==1 && keyCode(esc))
			%                 % Set the abort-demo flag.
			%                 abortit=2;
			%                 break;
			%             end;

			% === WRITE INITIAL INSTRUCTIONS ===

			% Write instruction message for subject (centered, white color).  
			% The special character '\n' introduces a line-break:

			% draw a grey background
			Screen('FillRect', w, scr.grey);

			% write the message
			beginningMessage = 'Please Pay attention. \n \n \n \n Wait for auditory cues \n \n Press the spacebar to start and respond';
			DrawFormattedText(w, beginningMessage, 'center', 'center', scr.black);

			% display the message
			Screen('Flip', w); 
			[secs, keyCode] = KbWait;
			while keyCode(ResponseButton) == 0
					[secs, keyCode] = KbWait;
			end

			% === CALL THE PRESENTATION FUNCTION ===

			% get time at the beginning of the experiment
			onsetExpe=GetSecs;   

			% call the function that take care of running the experiment
			[RT, expStruct] = stimPres_VOICES(expStruct, ntrials, w, scr, pahandle);


%% ===== END & SAVE =====

			% Write saving message

            Screen('TextSize', w, 20);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect', w, scr.grey);

            saveMessage = 'End Of The Run \n \n \n Wait For Data Saving And Session Closing Routines';
            DrawFormattedText(w, saveMessage, 'center', 'center', scr.black);
            Screen('Flip', w);


			% save the Struct

			save([runDir,'/expData_',subjectID, runID, '.mat'], 'expStruct', '-v7.3');
			save([runDir,'/expRT_',subjectID, runID, '.mat'], 'RT', '-v7.3');


			% convert struct as table without the sounds

			data = struct2table(expStruct);
			dataNoSounds = data(:,{'cueValidity', 'targetID','targetName','targetLog', 'notargetName','cueName','reactionTime'});

			writetable(dataNoSounds,[runDir,'/expData_',subjectID, runID, '.txt']);


			% Write ending message

			Screen('TextSize', w, 20);
			Screen('TextFont', w, 'Geneva');
			Screen('FillRect', w, scr.grey);  
			endmessage = 'Data saved. \n \n Thank you for participating \n \n Press ESC';
			DrawFormattedText(w, endmessage, 'center', 'center', scr.black);
			Screen('Flip', w);
			[secs, keyCode] = KbWait;
				while keyCode(escapeKey) == 0
						[secs, keyCode] = KbWait;
				end


			% Duration of the experiment:

			offsetExpe = GetSecs;					% time at the end of the experiment
			DurExpe = (offsetExpe-onsetExpe)/60;	% compute duration of the run


			% Cleanup at the end of a regular session

			ShowCursor;
			Screen('CloseAll');
			PsychPortAudio('Close', pahandle)
			fclose('all');
			Priority(0);


		catch
			% Catch error: this is executed in case something goes wrong in the
			% 'try' part due to programming error etc...
			% if PTB crashes it will land here, allowing us to reset the screen to normal.

			disp(['CRITICAL ERROR: ' lasterr ])
			disp(['Exiting program ...'])

			% Do same cleanup as at the end of a regular session

			ShowCursor;
			Screen('CloseAll');
			PsychPortAudio('Close', pahandle)
			fclose('all');
			Priority(0);


			% Output the error message that describes the error:
			psychrethrow(psychlasterror);

		end %try
end % for (blocks)

