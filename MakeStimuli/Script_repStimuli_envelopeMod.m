%% ===== CALL SCRIPT TO PRODUCE THE SOUND STIMULI FOR THE OVERLAPPING SCENE EXPERIMENT =====

% AUTHOR: GIORGIO MARINATO @ CIMeC - UNIVERSITY OF TRENTO
% DATE: 27/11/2017 


clear all
% close all
clc
%% ===== SET THE DEFAULT LENGTH OF THE REPETITION INTERVAL =====
 
% === set default at 750ms for now ===

    default = .750; %sec
    
% deprecated manual input here because of no trace

%% === Prompt length repetition interval ===

%    repInterval = input(sprintf('Enter the new length of the repetition interval, seconds are the unit: ', default));
%   
%% === if the user hits 'return' without writing anything, tmp is empty and the default is used ===

%    if ~isempty(repInterval)
%       default = repInterval;
%    end  
    
%% ===== INPUTS AND OUTPUTS DIRS =====

    Fs = 44100;

    root                    = '/home/giorgio/Projects/Auditory/OverlappingScene/sounds/NewSounds2/';
    %root                    = '/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/'
    stimuli                 = 'Ambient_Human_Places/';
    bgndFolder              = ['Background/', stimuli, '5sec_cut/'];
    fgndFolder              = 'Foreground/5sec_cut/';
    
    
%    monoForNorm             = 'Norm/'

    snd = struct;
    
% === build the structure that I'm going to use for the experiment === 
    
    % foreground speech
    snamesf = dir(fullfile(root, fgndFolder, '*.wav'));
        
    for n = 1:length(snamesf)
        
        snd.foreground(n).name          = snamesf(n).name;
        snd.foreground(n).sound         = audioread([root, fgndFolder, snd.foreground(n).name]);
        snd.foreground(n).audioinfo     = audioinfo([root, fgndFolder, snd.foreground(n).name]);
        
    end
    
    % background environment
    snamesb = dir(fullfile(root, bgndFolder, '*.wav'));
        
    for n = 1:length(snamesb)
        
        snd.background(n).name          = snamesb(n).name;
        snd.background(n).sound         = audioread([root, bgndFolder, snd.background(n).name]);
        snd.background(n).audioinfo     = audioinfo([root, bgndFolder, snd.background(n).name]);
        
    end
    
    
%% ===== CALL THE MAKE SOUNDS SAME LENGTH FROM CENTER FUNCTION =====
% chop a length from the center of each sample

    for n = 1:length(snd.foreground)

        snd.foreground(n).soundLength       = makeSameLength_Center(snd.foreground(n).sound, 5, Fs);    
        snd.background(n).soundLength       = makeSameLength_Center(snd.background(n).sound, 5, Fs);
    
    end
    
%% ===== CALL THE MAKE MONO AND NORMALIZE FUNCTION =====
% to be sure that all the sounds are in mono and normalized to -23dB

    for n = 1:length(snd.foreground)

        snd.foreground(n).monoNorm      = makeMonoAndNorm(snd.foreground(n).soundLength);    
        snd.background(n).monoNorm      = makeMonoAndNorm(snd.background(n).soundLength);

    end
    
%% ===== CALL THE ENVELOPE FUNCTION =====    
% we use the speech envelope to modulate the background sound envelope, 
% each background has 20 different enveloped version  

    for nb = 1:length(snd.background)
        
        for nf = 1:length(snd.foreground)
        
            snd.background(nb).enveloped{nf}        = makeEnvelope( snd.foreground(nf).monoNorm, snd.background(nb).monoNorm, 4410 );
        end
        
    end
        
%% ===== NORMALIZE THE ENVELOPE MODULATED BACKGROUND AGAIN =====
    
    for nb = 1:length(snd.background)
        
        for nf = 1:length(snd.foreground)
        
            snd.background(nb).envelopedNorm{nf}        = makeMonoAndNorm( snd.background(nb).enveloped{nf} );
        end
    end        

%% ===== INSERT THE REPETITION IN FOREGROUND AND BACKGROUND =====
% Use a pool of 50 insertions each speech and 

    % foreground
    for nsnd = 1:length(snd.foreground)
        
        for ntime = 1:50
            
            [snd.foreground(nsnd).repLog{ntime}, snd.foreground(nsnd).repSound{ntime}] = insertRepetition(snd.foreground(nsnd).monoNorm, Fs, default);
        end       
    end
    
    % background
    for nsnd = 1: length(snd.background)
        
        for nenv = 1:length(snd.background(nsnd).envelopedNorm)
            
            for ntime = 1:4 
                
                [snd.background(nsnd).repLog{ntime,nenv}, snd.background(nsnd).repSound{ntime,nenv}] = insertRepetition(snd.background(nsnd).envelopedNorm{nenv}, Fs, default);
            end
        end
    end
    
    
%% ===== MAKE THE CLEAN SOUNDS SAME LENGTH OF ONES WITH REPETITION AGAIN =====
% chop a length from the center of each sample

    repLength = length(snd.foreground(1).repSound{1});    

    for n = 1:length(snd.foreground)

        snd.foreground(n).monoNorm       = makeSameLength_Center(snd.foreground(n).monoNorm, repLength); 
        
        for ntime = 1:length(snd.background(n).envelopedNorm)
            snd.background(n).envelopedNorm{ntime}       = makeSameLength_Center(snd.background(n).envelopedNorm{ntime}, repLength);
        end
    end

    
%% ===== SAVE ON DISK THE SOUNDS AND IT'S LOGS =====

% === create the foders ===

    repFolder = 'repSounds_enveloped_75/';

    if ~exist([root, bgndFolder, repFolder], 'dir') || ~exist([root, bgndFolder, repFolder], 'dir')

            mkdir([root, fgndFolder, repFolder]);
            mkdir([root, bgndFolder, repFolder]);
    end
    
    no_repFolder = 'clean5sec_enveloped_75/';

    if ~exist([root, fgndFolder, no_repFolder], 'dir') || ~exist([root, bgndFolder, no_repFolder], 'dir')

            mkdir([root, fgndFolder, no_repFolder]);
            mkdir([root, bgndFolder, no_repFolder]);
    end
    
    
% === SAVE THE STRUCT ===

     save([root,'soundsStruct'], 'snd', '-v7.3');


    for s = 1:length(snd.foreground)
    
        % === SAVE THE SOUNDS WITH NO REPETITION ===

		[~, namef, ext] = fileparts(snd.foreground(s).name);
        [~, nameb, ext] = fileparts(snd.background(s).name);

        audiowrite([root, fgndFolder, no_repFolder, namef, '_clean5sec_monoNorm', ext], snd.foreground(s).monoNorm, 44100);
        
        for bc = 1:length(snd.background(s).envelopedNorm)
            audiowrite([root, bgndFolder, no_repFolder, nameb, '_clean5sec_envelopedNorm', num2str(bc), ext], snd.background(s).envelopedNorm{bc}, 44100); 
        end        
                    
                      
        % === SAVE THE SOUNDS WITH REPETITION AND THEIR LOGS ===                  
        
            
        % === Foreground Speech ===
        for n = 1:length(snd.foreground(s).repSound)

            if n < 10   % save the sounds for numbers <10 
                % sounds
                audiowrite([root, fgndFolder, repFolder, namef, '_repSnd_0',num2str(n), ext], snd.foreground(s).repSound{n}, 44100);
                % logs
                dlmwrite([root, fgndFolder, repFolder, namef, '_repLog_0', num2str(n), '.txt'], snd.foreground(s).repLog{n}, 'precision', 8);

            else        % save the sounds for numbers >10
                % sounds
                audiowrite([root, fgndFolder, repFolder, namef, '_repSnd_',num2str(n), ext], snd.foreground(s).repSound{n}, 44100);
                % logs
                dlmwrite([root, fgndFolder, repFolder, namef, '_repLog_', num2str(n), '.txt'], snd.foreground(s).repLog{n}, 'precision', 8);

            end
        end

        % === Background Environment ===            
        for n = 1:length(snd.background(s).repSound)

            for ntime = 1:4

                if n < 10   % save the sounds for numbers <10 
                    % sounds
                    audiowrite([root, bgndFolder, repFolder, nameb, '_repSnd_0',num2str(n),'_0', num2str(ntime), ext], snd.background(s).repSound{ntime,n}, 44100);
                    % logs
                    dlmwrite([root, bgndFolder, repFolder, nameb, '_repLog_0', num2str(n),'_0', num2str(ntime), '.txt'], snd.background(s).repLog{ntime,n}, 'precision', 8);

                else        % save the sounds for numbers >10
                    % sounds
                    audiowrite([root, bgndFolder, repFolder, nameb, '_repSnd_',num2str(n),'_0', num2str(ntime), ext], snd.background(s).repSound{ntime,n}, 44100);
                    % logs
                    dlmwrite([root, bgndFolder, repFolder, nameb, '_repLog_', num2str(n), '_0', num2str(ntime), '.txt'], snd.background(s).repLog{ntime,n}, 'precision', 8);
                end
            end
        end
        
    end
    
