function [indx_up, indx_down, bestSample, direction] = zeroCrossing(inputSound, direction, nSamples)
%% ===== FIND ZERO CROSSING AT THE BEGINNING OR THE END OF A FILE =====
% the function finds all the zero crossing in a signal and then select the first
% or the last sample. It also return the direction of the signal after the zero

% AUTHOR: GIORGIO MARINATO @ CIMeC - UNIVERSITY OF TRENTO ITALY 
% DATE: 10/11/2017

	% find all the zero crossing 
	zeroCross            = diff(sign(inputSound));
    
    % default nSamples
    % nSamples = 1;
	
	switch direction
		
		case 'first'
		
		% find the first useful zero crossing sample going positive 
		indx_up           	= find(zeroCross>0, nSamples, 'first');
        indx_up             = indx_up(find(indx_up<nSamples, 1, 'last'));

		% find the first useful zero crossing sample going negative  
		indx_down         	= find(zeroCross<0, nSamples, 'first');
        indx_down           = indx_down(find(indx_down<nSamples, 1, 'last'));

		% find the bigger between the two
		cross 				= max([indx_up, indx_down]);

		% select the 
			if cross == indx_up
				bestSample	= indx_up;
				direction 	= 'indx_up';
			else
				bestSample	= indx_down;
				direction 	= 'indx_down';
			end 
			
		
		case 'last'

		% find the last useful zero crossing sample going positive 
		indx_up           = find(zeroCross>0, nSamples, 'last');
        indx_up           = indx_up(find(indx_up>(length(inputSound)-nSamples), 1, 'first'));

		% find the last useful zero crossing sample going negative  
		indx_down         = find(zeroCross<0, nSamples, 'last');
        indx_down         = indx_down(find(indx_down>(length(inputSound)-nSamples), 1, 'first'));
	
		% find the smaller between the two
		cross 			= min([indx_up, indx_down]);
	
			if cross == indx_up
				bestSample	= indx_up;
				direction 	= 'indx_up';
			else
				bestSample	= indx_down;
				direction 	= 'indx_down';
			end 
    end
end