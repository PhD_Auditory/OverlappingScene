function [RT, trainStruct] = trainingPres(trainStruct, trial, w, grey, black, pahandle)    

%% ===== INPUT PARAMETERS (e.g. keyboard) ===== %%


% _____KEYBOARD MAPPING_____
	% Enable unified mode of KbName, so KbName accepts identical key names on
	% all operating systems:
    KbName('UnifyKeyNames');                   

    escapeKey = KbName('Escape');  
%    quitkey = KbName('Cancel'); 
    ResponseButton = KbName('Space');
    
%     RestrictKeysForKbCheck([escapeKey, quitkey, ResponseButton]);
       
    keysOfInterest=zeros(1,256);  
    keysOfInterest(escapeKey)=1;  
%    keysOfInterest(quitkey)=1;  
    keysOfInterest(ResponseButton)=1;
    
    KbQueueCreate([], keysOfInterest);
    KbQueueStart;
    
    % Colors definition
        white 	= [255 255 255]; 
        black 	= [0 0 0]; 
        grey 	= [150 150 150];
        red 	= [255 0 0];
        green 	= [0 255 0];
    

%% ===== STIMULI PARAMETERS =====
    
    % Cue Duration
    cueDur = 0.500;

    % Stimuli Duration 
    stimDur = 5; % secs
    
    % More Window for Response
    responseExtra = 1.5;

    % Inter Trial Interval
    ITI = (1:0.05:2)';
    listITI = Shuffle(repmat(ITI, round(100/length(ITI)), 1));
    
    % Stimulus Onset Asynchrony   
    SOA=(0.500:0.050:0.750)'; 
    listSOA = Shuffle(repmat(SOA, round(100/length(SOA)), 1));
    

%% ===== STIMULI PRESENTATION ===== 

% _____Start Collecting Onset And Response_____
   
    RT = NaN(100, 1); 
    
    % Perform one warmup trial, to get the sound hardware fully up and running,
    % performing whatever lazy initialization only happens at real first use.
    % This "useless" warmup will allow for lower latency for start of playback
    % during actual use of the audio driver in the real trials:
    % PsychPortAudio('Start', pahandle, 1, 0, 1);
    % PsychPortAudio('Stop', pahandle, 1);

    % Ok, now the audio hardware is fully initialized and our driver is on
    % hot-standby, ready to start playback of any sound with minimal latency.

    for t = 1:trial;        
        
        % select the sound                
        target      = transpose(cell2mat(trainStruct(t).targetSound));
                    
        % fill buffer
        PsychPortAudio('FillBuffer', pahandle, target);     
        
% _____Cue Presentation_____

		if trainStruct(t).cueDir == 1    % cue foreground            
            
            % present visual cue
            
            cue = 'FOREGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end
            
        elseif trainStruct(t).cueDir == 0
            
            % present visual cue
            
            cue = 'BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end
                
        end % if trial
        
% _____Fixation Cross During Soa_____
        
        fixcross = '+';
        Screen('TextSize', w, 60);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, grey);
        DrawFormattedText(w, fixcross, 'center', 'center', black);
        [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);
            while (GetSecs - fixSoaOnset)<=listSOA(t)
            end
                
%             WaitSecs(listSOA(trial));
            
% _____Present The Sound Scene And Take Care Of The Timing And The Reaction Times_____

        % when the sound should start with the reference of the cue 
        startTime = cueOnset + cueDur + listSOA(t)
        
        % onset of the sound 
        soundOnset = PsychPortAudio('Start', pahandle, 1, startTime, 1)
        
        maxStimTime = soundOnset + stimDur + responseExtra 
        
        KbQueueFlush;
        
        while GetSecs <= maxStimTime
          
%              [ KeyIsDown, responseSecs, keyCode ] = KbCheck;           
               
           [pressed, firstPress, firstRelease, lastPress, lastRelease] = KbQueueCheck(-1);                  
            %disp('Test1');        
                    
           if pressed  == 1 % KeyIsDown %pressed     
                %disp('Test2')                
                % calculate the reaction time 
                RT(t) = (firstPress(ResponseButton) - soundOnset) - trainStruct(t).targetLog % firstPress(ResponseButton) responseSecs  
           end
         end % end while  
        
                
        % Stop playback:
        PsychPortAudio('Stop', pahandle, 1);          
        
        % store the reaction time in the struct 
        trainStruct(t).reactionTime = RT(t); 
        
        
% _____Fixation cross during ITI_____

        if isnan(RT(t)) | RT(t)<0
		    fixcross = '+';
		    Screen('TextSize', w, 60);
		    Screen('TextFont', w, 'Geneva');
		    Screen('FillRect',w, grey);
		    DrawFormattedText(w, fixcross, 'center', 'center', red);
		    [VBLTimestamp, start]= Screen('Flip', w);
   		        while (GetSecs - start)<=listITI(t)
                end
		        
        elseif RT(t) > 0
        	fixcross = '+';
		    Screen('TextSize', w, 60);
		    Screen('TextFont', w, 'Geneva');
		    Screen('FillRect',w, grey);
		    DrawFormattedText(w, fixcross, 'center', 'center', green);
		    [VBLTimestamp, start]= Screen('Flip', w);
		        while (GetSecs - start)<=listITI(t)
		        end
	    end    
		               
    end % ends for      

end % ends function          
