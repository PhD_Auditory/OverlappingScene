#!/bin/bash

##################################################################
#
# Freesurfer anatomy extraction - MEG Lab Trento
#
# Davide Tabarelli & Giorgio Marinato & Daniel Baldauf
#
# Last revision: 2016/10/26
#
##################################################################

PROJECT=CFS

# RAW_DATA_DIR="$HOME/DT/$PROJECT/raw/dicom"
RAW_DATA_DIR="/mnt/storage/tier2/danbal/BRACHR002N3B/Giorgio_M/Auditory/OverlappingScene/MRIs/"
SUBJECTS_DIR="$HOME/DT/$PROJECT/anatomy"

SUBJECT=$1
MPRAGE=$2	# A DICOM file from the MPRAGE sequence or a Nifti file with the whole MPRAGE

if [ -d "$SUBJECTS_DIR/$SUBJECT" ]; then
	echo
	read -p "Subject directory for $SUBJECT already exists. Do you wan to delete it? " -n 1 -r
	echo
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		rm -rf $SUBJECTS_DIR/$SUBJECT
	fi
fi

recon-all -autorecon1 -sd $SUBJECTS_DIR -subjid $SUBJECT -i $RAW_DATA_DIR/$SUBJECT/$MPRAGE
recon-all -autorecon2 -sd $SUBJECTS_DIR -subjid $SUBJECT
recon-all -autorecon3 -sd $SUBJECTS_DIR -subjid $SUBJECT

# Skin & skull using watershed algorithm (MNE convenient wrapper for FreeSurfer mri_watershed)
#mne_watershed_bem --subject $SUBJECT --overwrites

# Prepare surface representation for tksurfer
#mkheadsurf -s $SUBJECT

