function [mono] = makeMonoAndNorm(stereo)
% This function convert the stero sounds in mono
% It does so taking in consideration to not do a mean if in one channel there are zeros neither %%% if the two channels differ of a limited number of samples. 

% AUTHOR: GIORGIO MARINATO @ CIMeC -UNIVERSITY OF TRENTO
 
%% ===== CONVERT STEREO TO MONO =====

 % _____if the sound is already in mono keep the sound like it is_____ 
        
    if size(stereo(1,:)) == 1

        mono = stereo(:,1); 
            
  % _____if the two channles are identical or differ of a very limited number of samples just choose one channel and don't perform any mean_____                     
        
    elseif all(stereo(:,1) == stereo(:,2)) || length(stereo(stereo(:,1) ~= stereo(:,2)))<500 
        % 500 is a reasonable number of samples in which two channels
        % otherwise identical differ

       mono = stereo(:,1);            
 
 % _____mean of the two channles if the two channels differ working around zeros_____  
      
    else 

%    noZero       = ~sum(stereo == 0, 2);       % Rows Without Zeros (Logical Vector)
%    mono         = sum(stereo, 2);             % Sum Across Columns
%    mono(noZero) = mono(noZero)/2;             % Divide Rows Without Zero

    mono         = sum(stereo, 2)/2;

       %array_snd_f_mono{mono} = (array_snd_f{mono}(:,1) + array_snd_f{mono}(:,2))/2

    end
    
%% ===== NORMALIZE =====

    mono = mono*.707/max(abs(mono)); 
end
            
