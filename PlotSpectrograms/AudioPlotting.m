%% ===== READ SOUNDS =====

BcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut2/clean5sec2');
FcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut2/clean5sec2');


    Bclean         = dir(fullfile(BcleanFolder, '*.wav'));
    Fclean         = dir(fullfile(FcleanFolder, '*.wav'));
    
    array_Bclean       = cell(20,1);
    array_Fclean       = cell(20,1);
    
     for n = 1:20
        array_Bclean{n} = audioread(fullfile(BcleanFolder, Bclean(n).name)); 
        array_Fclean{n} = audioread(fullfile(FcleanFolder, Fclean(n).name));
    end


%% ===== PLOT WAVEFORMS =====
for n = 1:20
    wave = figure(1);

    % Wave2 = subplot(211)
    plot(array_Fclean{n})
    title('sound normalized with matlab at -1dB')
    xlabel('time in samples');
    ylabel('Scaled Normalized Amplitude')
    ax                          = wave.CurrentAxes
    ax.FontSize                 = 15
    ax.YLim                     = [-1 1]
    ax.YTick                    = [-1:.2:1]
    ax.XLim                     = [0 220500]
    ax.XTick                    = [0:0.5*44100:5*44100]


    % ax.title.FontWeight = 'normal';   
    % ax.FontSize = 30

    %% ===== PLOT SPECTROGRAMS ===== 	

    % parameters
    sound       = soundRep10F %array_Fclean{10};
    window      = 1000;
    overlap     = 900;
    nfft        = 256  %2^nextpow2(length(sound));
    F           = [0:5:7000];
    fs          = 44100;

    % plot spectrogram
    spect = figure(2);

    spectrogram(sound,window,overlap,F,fs,'yaxis');

    ax                  = spect.CurrentAxes
    ax.FontSize 		= 20;
    colormap(spect,hot)
    caxis([-70 -30]);

    % save

    % saveas(gcf, [pwd, subfolder, stim], 'jpeg');
    % saveas(gcf, [pwd, subfolder, stim], 'svg');


    %% ===== PREPARE COMBINED PLOT =====

    combo = figure(3)

    combo(1) = subplot(211); 


    combo(2) = subplot(212);

    % Paste figures on the subplots
    copyobj(allchild(get(wave,'CurrentAxes')),combo(1));
    copyobj(allchild(get(spect,'CurrentAxes')),combo(2));

    %% save

    saveas(wave, pwd, 'jpeg')
    saveas(spect, pwd, 'jpeg')
    saveas(combo,pwd,'jpeg')
end
%% NOTES

% subplot(311)
% plot(snd.foreground(1).mono)
% title('sound just read in with audioread and scaled')
% 
% subplot(312)
% plot(snd.foreground(1).monoNorm)
% title('sound normalized with audacity at -1dB')
% 
% subplot(313)

%     h                       = gca;
%     h.YDir                  = 'reverse';
%     h.XGrid                 = 'on';
%     h.YGrid                 = 'on';
%     h.GridColor             = [.5 .5 .5];
%     h.XLim                  = [1 35];
%     h.YLim                  = [1 16];
%     xticks([0:1:35]);
%     yticks([0:1:16]);
%     title('Random Trajectory');
%     xlabel('Degrees of Rotation (10)');
%     ylabel('Vertical Speaker Position');
%     xticklabel
