function [ rmpSnd , ramp ] = rampSoundSimple( inputSound, rampDirection, rampDur)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% better to do with case switch and nargin to have a defenitive ramping
% function also for a simple case when zero crosing is not necessary

% AUTHOR: Giorgio Marinato @ CIMeC - University of Trento


    % length of sound
    inLength = 1:(length(inputSound));
	
	% length of the ramping and the input sound in samples      
    inDur = (length(inputSound));
    rampDur = rampDur; 
   
    
    % ramping vector
    ramp = ones(size(inLength));	
    
    % ramp up 
    if strcmp (rampDirection, 'rampUp')        
        ramp(inLength<rampDur) = linspace(0,1,sum(inLength<rampDur));

    % ramp down
    elseif strcmp (rampDirection, 'rampDown')        
        ramp(inLength>inDur-rampDur) = linspace(1,0,sum(inLength>inDur-rampDur));
    
    % ramp starts at 0, becomes 1 over the course of the rampDur and ramps down again at the end
    elseif strcmp (rampDirection, 'rampUpandDown')
                
        ramp(inLength<rampDur) = linspace(0,1,sum(inLength<rampDur));
        ramp(inLength>inDur-rampDur) = linspace(1,0,sum(inLength>inDur-rampDur));     
    end
    
    % sound ramped
    rmpSnd = inputSound.*ramp';

end

