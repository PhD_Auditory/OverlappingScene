%% ===== CALL SCRIPT TO PRODUCE THE BACKGROUND MUSIC STIMULI FOR THE OVERLAPPING SCENE EXPERIMENT =====

% AUTHOR: GIORGIO MARINATO @CIMeC - UNIVERSITY OF TRENTO
% DATE: 03/11/2017 


clear all
close all
clc
%% ===== SET THE DEFAULT LENGTH OF THE REPETITION INTERVAL =====
 
% _____set default at 500ms for now_____

    default = .500; %sec
    
% _____Prompt length repetition interval_____

    repInterval = input(sprintf('Enter the new length of the repetition interval, seconds are the unit: ', default));
    
% _____if the user hits 'return' without writing anything, tmp is empty and the default is used_____

    if ~isempty(repInterval)
       default = repInterval;
    end  
    
%% ===== INPUTS AND OUTPUTS DIRS =====

    Fs = 44100;

    root                    = '/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/';
    %root                    = '/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/New_Sounds/'
    bgndFolder              = 'Background/5sec_cut/';
    stimuli                 = 'Music_classic/';

    snd = struct;
    
% _____build the structure that I'm going to use for the experiment_____
    
    snamesb = dir(fullfile(root, bgndFolder, stimuli, '*.wav'));
        
    for n = 1:length(snamesb)
        
        snd.background(n).name          = snamesb(n).name;
        snd.background(n).sound         = audioread([root, bgndFolder, stimuli, snd.background(n).name]);
        snd.background(n).audioinfo     = audioinfo([root, bgndFolder, stimuli, snd.background(n).name]);
        
    end
    
%% ===== CALL THE MAKE SOUNDS SAME LENGTH FROM CENTER FUNCTION =====
% chop a lenth from the center of each sample

    for n = 1:length(snd.background)
        snd.background(n).soundLength = makeSameLength_Center(snd.background(n).sound, 5, Fs); 
    end
    
%% ===== CALL THE MAKE MONO AND NORMALIZE FUNCTION =====
% to be sure that all the sounds are in mono

    for n = 1:length(snd.background)
        snd.background(n).monoNorm = makeMonoAndNorm(snd.background(n).soundLength);
    end
   
%% ===== CALL THE REPETITION FUNCTION =====  

    for nsnd = 1:length(snd.background)
        
        for ntime = 1:50
            [snd.background(nsnd).repLog{ntime}, snd.background(nsnd).repSound{ntime}] = insertRepetition(snd.background(nsnd).monoNorm, Fs, default);            
        end       
    end
                            
%% ===== SAVE ON DISK THE SOUNDS AND IT'S LOGS =====

    repFolder = 'repSounds_Music_classic/';

    if ~exist([root, bgndFolder, stimuli, repFolder], 'dir')
            mkdir([root, bgndFolder, stimuli, repFolder]);
    end
    
    no_repFolder = 'clean5sec_Music_classic/';

    if ~exist([root, bgndFolder, stimuli, no_repFolder], 'dir')
            mkdir([root, bgndFolder, stimuli, no_repFolder]);
    end
    
%% _____SAVE THE STRUCT_____

    %save([root,'soundsStruct3'], 'snd', '-v7.3');
    

    for s = 1:length(snd.background)
    
% _____SAVE THE SOUNDS WITH NO REPETITION_____

        [~, nameb, ext] = fileparts(snd.background(s).name);

        audiowrite([root, bgndFolder, stimuli, no_repFolder, nameb, '_clean5sec_monoNorm', ext], snd.background(s).monoNorm, 44100);    
        
        for n = 1:length(snd.background(s).repSound)
            
            [~, nameb, ext] = fileparts(snd.background(s).name);            
            
            
% _____SAVE THE SOUNDS_____    
            if n < 10
                audiowrite([root, bgndFolder, stimuli, repFolder, nameb, '_repSnd_0',num2str(n), ext], snd.background(s).repSound{n}, 44100);
            
% _____SAVE THE LOGS_____ 
                dlmwrite([root, bgndFolder, stimuli, repFolder, nameb, '_repLog_0', num2str(n), '.txt'], snd.background(s).repLog{n}, 'precision', 8);
            
            else
% _____SAVE THE SOUNDS_____            
                audiowrite([root, bgndFolder, stimuli, repFolder, nameb, '_repSnd_',num2str(n), ext], snd.background(s).repSound{n}, 44100);
            
% _____SAVE THE LOGS_____ 
                dlmwrite([root, bgndFolder, stimuli, repFolder, nameb, '_repLog_', num2str(n), '.txt'], snd.background(s).repLog{n}, 'precision', 8);
            end
        
        end

    end
