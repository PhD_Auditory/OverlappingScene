%% ===== CREATE THE EXPERIMENTAL STRUCTURE WHERE ALL THE DATA ARE STORED (THEN STORE THE DATA ON DISK TOO!) =====   

    expStruct                     = struct;
    tid                           = (1:100)';
           
% _____NOW CREATE THE CUE DIRECTION AND THE TARGET DIRECTION_____

    cueDir                        = Shuffle([zeros(50, 1); ones(50,1)]); %1 for foreground 0 for background
    cueValid                      = 1;
    cueInvalid                    = 0; 
    cueNeutral                    = 2;
%     cueValidity                   = Shuffle([repmat(cueValid,70,1); repmat(cueInvalid,20,1); repmat(cueNeutral, 10,1)]);

    cueValidityF                  = Shuffle([repmat(cueValid,35,1); repmat(cueInvalid,10,1); repmat(cueNeutral, 5,1)]);
    cueValidityB                  = Shuffle([repmat(cueValid,35,1); repmat(cueInvalid,10,1); repmat(cueNeutral, 5,1)]);
    
	countF = 0;
	countB = 0;
	
    for tid = 1:length(tid)
        expStruct(tid).tid                  = tid;
	
		% _____FOREGROUND_____    
        if cueDir(tid) == 1
            
            expStruct(tid).cueDir           = cueDir(tid);		
			countF = countF +1;
			
			% _____VALID_____     
			if cueValidityF(countF) == 1				
				targetID          = 1; % foreground
				targetName        = poolFsounds(tid);
				targetSound       = array_Fsounds(tid);
				targetLog         = array_Flogs(tid);
		  
					
				noTarget          = array_Bclean(tid);
				notargetName      = poolBclean(tid);
				
			% _____INVALID_____		
			elseif cueValidityF(countF) == 0			
				targetID          = 0; % background
				targetName        = poolBsounds(tid);
				targetSound       = array_Bsounds(tid);
				targetLog         = array_Blogs(tid);
			
				noTarget          = array_Fclean(tid);
				notargetName      = poolFclean(tid);
				
			% _____NEUTRAL_____
			elseif cueValidityF(countF) == 2
				targetID          = 1; % foreground
				targetName        = poolFsounds(tid);
				targetSound       = array_Fsounds(tid);
				targetLog         = array_Flogs(tid);
					
				noTarget          = array_Bclean(tid);
				notargetName      = poolBclean(tid);				
            end
			
            expStruct(tid).cueValidity      = cueValidityF(countF);
            
		% _____BACKGROUND_____    
        else cueDir(tid) == 0;
            
            expStruct(tid).cueDir           = cueDir(tid);		
			countB = countB +1;
            
			% _____VALID_____     
			if cueValidityB(countB) == 1
				targetID          = 0; % background
				targetName        = poolBsounds(tid);
				targetSound       = array_Bsounds(tid);
				targetLog         = array_Blogs(tid);
					
				noTarget          = array_Fclean(tid);
				notargetName      = poolFclean(tid);
					
			% _____INVALID_____		
			elseif cueValidityB(countB) == 0
				targetID          = 1; % foreground
				targetName        = poolFsounds(tid);
				targetSound       = array_Fsounds(tid);
				targetLog         = array_Flogs(tid);
					
				noTarget          = array_Bclean(tid);
				notargetName      = poolBclean(tid);
			
			% _____NEUTRAL_____
			elseif cueValidityB(countB) == 2
				targetID          = 0; % background
				targetName        = poolBsounds(tid);
				targetSound       = array_Bsounds(tid);
				targetLog         = array_Blogs(tid);
					
				noTarget          = array_Fclean(tid);
				notargetName      = poolFclean(tid);
            end
            
            expStruct(tid).cueValidity      = cueValidityB(countB);

		end
		
	% _____FILL UP THE STRUCTURE_____               
        
        expStruct(tid).targetID         = targetID;
        expStruct(tid).targetName       = targetName;
        expStruct(tid).targetLog        = targetLog;
        expStruct(tid).targetSound      = targetSound;
        expStruct(tid).notargetName     = notargetName;
        expStruct(tid).noTarget         = noTarget;
		
	end

% 200 invalid
% 200 neutral
% 467 valid --> CAN BE 500?

% TOTAL: 867 IN 9 BLOCKS --> 100 TRIALS PER BLOCK.         
            
            
            