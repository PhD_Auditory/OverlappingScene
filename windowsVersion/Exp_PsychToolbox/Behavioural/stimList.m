function [expStruct] = stimList(BgroundFolder, FgroundFolder, BcleanFolder, FcleanFolder)

%% ===== HAVE ALL THE SOUNDS FOR BACKGROUND AND FOREGROUND WITH THE RELATIVES LOG IN MEMORY FROM WHICH I CAN RANDOMLY PICK UP 100 TRIALS EVERY BLOCK =====

%     BgroundFolder       = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/repSounds');
%     FgroundFolder       = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/repSounds');
%     
%     BcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Background/5sec_cut/clean5sec');
%     FcleanFolder        = ('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/New_Sounds/Foreground/5sec_cut/clean5sec');

% _____READ FROM FOLDERS AND PREPARE THE VARIABLES (CONSIDER ALSO TO READ FROM THE STRUCTURE...)_____

    ntrials = 100
    
% _____LIST SOUNDS AND LOGS OF SCENES WITH REPETITION_____
    all_Bsounds             = dir(fullfile(BgroundFolder, '*.wav'));
    all_Blogs               = dir(fullfile(BgroundFolder, '*.txt'));

    all_Fsounds             = dir(fullfile(FgroundFolder, '*.wav'));
    all_Flogs               = dir(fullfile(FgroundFolder, '*.txt')); 
    
%_____EXTRACT RANDOM 100 TRIALS FROM THE LIST OF 1000_____
    randPickBack            = randi(1000,100,1);
    randPickFore            = randi(1000,100,1);  
    
%_____INDEX THE SOUNDS AND LOGS WITH THE PREVIOUS VECTOR_____ 
    all_Bsounds_Array       = {all_Bsounds.name};
    all_Blogs_Array         = {all_Blogs.name};

    all_Fsounds_Array       = {all_Fsounds.name}; 
    all_Flogs_Array         = {all_Flogs.name};
    
    poolBsounds             = transpose(all_Bsounds_Array(randPickBack));
    poolBlogs               = transpose(all_Blogs_Array(randPickBack));
    
    poolFsounds             = transpose(all_Fsounds_Array(randPickFore));
    poolFlogs               = transpose(all_Flogs_Array(randPickFore));
    
%_____READ FROM HARD DRIVE_____     
    array_Bsounds           = cell(ntrials,1);% _____READ THE SCENES WITHOUT REPETITION AND BUILD THE ARRAY TOO_____
    %array_Blogs         = [1:100]; %cell(ntrial,1);
    
    array_Fsounds           = cell(ntrials,1);
    %array_Flogs         = [1:100];
    
    
    for n = 1:ntrials        
        array_Bsounds{n}    = audioread(fullfile(BgroundFolder, poolBsounds{n}));
        array_Blogs(n)      = (dlmread(fullfile(BgroundFolder, poolBlogs{n}))) / 44100; %get the values in seconds
        
        array_Fsounds{n}    = audioread(fullfile(FgroundFolder, poolFsounds{n}));
        array_Flogs(n)      = (dlmread(fullfile(FgroundFolder, poolFlogs{n}))) / 44100;
    end
    
    array_Blogs             = transpose(array_Blogs);
    array_Flogs             = transpose(array_Flogs);
    
    
% _____READ THE SCENES WITHOUT REPETITION AND BUILD THE ARRAY TOO_____

    Bclean         = dir(fullfile(BcleanFolder, '*.wav'));
    Fclean         = dir(fullfile(FcleanFolder, '*.wav'));
    
%_____REPLICATE THE ARRAY TO HAVE A POOL OF 1000 TO AGAIN PICK UP A POOL OF RANDOM 100 SOUNDS_____     
    all_Bclean     = reshape(repmat({Bclean.name},[50,1]), [1000,1]);  
    all_Fclean     = reshape(repmat({Fclean.name},[50,1]), [1000,1]);
    
%_____INDEX THE SOUNDS_____
    poolBclean         = all_Bclean(randPickBack);
    poolFclean         = all_Fclean(randPickFore);
    
%_____READ THE SOUNDS FROM HARD DRIVE_____
    array_Bclean       = cell(ntrials,1);
    array_Fclean       = cell(ntrials,1);
    
    for n = 1:ntrials
        array_Bclean{n} = audioread(fullfile(BcleanFolder, poolBclean{n})); 
        array_Fclean{n} = audioread(fullfile(FcleanFolder, poolFclean{n}));
    end


%% ===== CREATE THE EXPERIMENTAL STRUCTURE WHERE ALL THE DATA ARE STORED (THEN STORE THE DATA ON DISK TOO!) =====   

    expStruct                     = struct;
    tid                           = (1:100)';
           
% _____NOW CREATE THE CUE DIRECTION AND THE TARGET DIRECTION_____

    cueDir                        = Shuffle([zeros(50, 1); ones(50,1)]); %1 for foreground 0 for background
    cueValid                      = 1;
    cueInvalid                    = 0; 
    cueNeutral                    = 2;
%     cueValidity                   = Shuffle([repmat(cueValid,70,1); repmat(cueInvalid,20,1); repmat(cueNeutral, 10,1)]);

    cueValidityF                  = Shuffle([repmat(cueValid,35,1); repmat(cueInvalid,10,1); repmat(cueNeutral, 5,1)]);
    cueValidityB                  = Shuffle([repmat(cueValid,35,1); repmat(cueInvalid,10,1); repmat(cueNeutral, 5,1)]);
    
	countF = 0;
	countB = 0;
	
    for tid = 1:length(tid)
        expStruct(tid).tid                  = tid;
	
	% _____FOREGROUND_____    
        if cueDir(tid) == 1
            
            expStruct(tid).cueDir           = cueDir(tid);		
			countF = countF +1;
			
		% _____VALID_____     
			if cueValidityF(countF) == 1				
				targetID          = 1; % foreground
				targetName        = poolFsounds(tid);
				targetSound       = array_Fsounds(tid);
				targetLog         = array_Flogs(tid);
		  
					
				noTarget          = array_Bclean(tid);
				notargetName      = poolBclean(tid);
				
		% _____INVALID_____		
			elseif cueValidityF(countF) == 0			
				targetID          = 0; % background
				targetName        = poolBsounds(tid);
				targetSound       = array_Bsounds(tid);
				targetLog         = array_Blogs(tid);
			
				noTarget          = array_Fclean(tid);
				notargetName      = poolFclean(tid);
				
		% _____NEUTRAL_____
			elseif cueValidityF(countF) == 2
				targetID          = 1; % foreground
				targetName        = poolFsounds(tid);
				targetSound       = array_Fsounds(tid);
				targetLog         = array_Flogs(tid);
					
				noTarget          = array_Bclean(tid);
				notargetName      = poolBclean(tid);				
            end
			
            expStruct(tid).cueValidity      = cueValidityF(countF);
            
	% _____BACKGROUND_____    
        else cueDir(tid) == 0;
            
            expStruct(tid).cueDir           = cueDir(tid);		
			countB = countB +1;
            
		% _____VALID_____     
			if cueValidityB(countB) == 1
				targetID          = 0; % background
				targetName        = poolBsounds(tid);
				targetSound       = array_Bsounds(tid);
				targetLog         = array_Blogs(tid);
					
				noTarget          = array_Fclean(tid);
				notargetName      = poolFclean(tid);
					
		% _____INVALID_____		
			elseif cueValidityB(countB) == 0
				targetID          = 1; % foreground
				targetName        = poolFsounds(tid);
				targetSound       = array_Fsounds(tid);
				targetLog         = array_Flogs(tid);
					
				noTarget          = array_Bclean(tid);
				notargetName      = poolBclean(tid);
			
		% _____NEUTRAL_____
			elseif cueValidityB(countB) == 2
				targetID          = 0; % background
				targetName        = poolBsounds(tid);
				targetSound       = array_Bsounds(tid);
				targetLog         = array_Blogs(tid);
					
				noTarget          = array_Fclean(tid);
				notargetName      = poolFclean(tid);
            end
            
            expStruct(tid).cueValidity      = cueValidityB(countB);

		end
		
	% _____FILL UP THE REST OF THE STRUCTURE_____               
        
        expStruct(tid).targetID         = targetID;
        expStruct(tid).targetName       = targetName;
        expStruct(tid).targetLog        = targetLog;
        expStruct(tid).targetSound      = targetSound;
        expStruct(tid).notargetName     = notargetName;
        expStruct(tid).noTarget         = noTarget;
		
	end

% 200 invalid
% 200 neutral
% 467 valid --> CAN BE 500?

% TOTAL: 867 IN 9 BLOCKS --> 100 TRIALS PER BLOCK.       


end
