%% ANALYZE AND PLOT EXPERIMENT

% TO DO


clear all
close all

%% ===== INPUT OUTPUT =====

root        = '/home/jnfn/Work/Projects/Auditory/OverlappingScene/Data/';
subject     = {'Subject_Nicholas', 'Subject_Mary', 'Subject_Giorgio', 'Subject_Martina', 'Subject_Folco', 'Subject_Arianna'... 
                'Subject_ChiaraDP', 'Subject_Daniele', 'Subject_Elio', 'Subject_Yara', 'Subject_Angela'};
block       = {'Block_1_enve.75/', 'Block_2_enve.75/', 'Block_3_enve.75/'};
NewAn       = 'NewAn_11';
% data        = 'expData_Nicholas1_enve.75.txt';

% output = [root, subject, block];
% mkdir(output)
% /home/jnfn/Work/Projects/Auditory/OverlappingScene/Data/Subject_Nicholas/Block_1_enve.75/expData_Nicholas1_enve.75.txt
% ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/Data/Subject_Nicholas/Block_1_enve.75/expData_Nicholas1_enve.75.txt')

%% ===== LOAD THE TABLES =====
    for sub = 1:length(subject)

        for bk = 1:length(block)
        
            data             = dir([root, subject{sub}, '/', block{bk}, '*.txt']);        
            runTables{bk}    = readtable([data.folder, '/', data.name]);
            
            %subTable{sub} = vertcat(runTables{bk});
        end
        
        subTable{sub} = vertcat(runTables{:});
        
        
        %% histfit to check the distribution of all conditions together
%         figure(1); 
%         histfit(subTable.reactionTime, 50); 
%         xlabel('RT'); ylabel('# instances'); 
%         title(['Distribution for subject ' sub ': all data against normal']); 
%         
        
        %% show range of values in each condition. 
        
%         stats = grpstats(subTable,{'cueValidity','targetID'},{'min','max','range'}, 'DataVars',{'reactionTime'});
%         
%         % boxplot to have a sense of the data
%         % subject =  
%         figure(2)
%         boxplot(subTable.reactionTime, subTable.cueValidity) %'notch', 'on'); 
%         % notch gives you 95% CI, box gives you 25% to 75%, whiskers give you 1.5 * interquartile range 
%         ylimits = get(gca, 'Ylim'); % for use after outlier rejection for comparison
%         ylabel('RT'); 
        %title(['All responses for S' num2str(subject)]); 
        
%% ===== MAKE ACCURACY VECTOR ===== 
        
        % produce the basic accuracy vector marking early, late and not answer response as
        % invalid
        
        subTable{sub}.accuracy = zeros(length(subTable{sub}.reactionTime),1);
                    
        for n = 1:length(subTable{sub}.reactionTime)
            
            % identify the early and late response (<0.2, >1.3) as clearly invalid, mark the NaN as invalid
            if subTable{sub}.reactionTime(n)<.2 | isnan(subTable{sub}.reactionTime(n)) | subTable{sub}.reactionTime(n)>1.3
                subTable{sub}.accuracy(n) = 0;
                
            else
                subTable{sub}.accuracy(n) = 1;
            end
        end
        
        % now calculate accuracy per condition
        
%         % split-apply-combine workflow
%         accTable = grpstats(subTable,{'cueValidity','targetID'},{''}, 'DataVars',{'accuracy'});
%         
%         accTable3 = grpstats(subTable,{'cueValidity', 'targetID'},{'tabulate'}, 'DataVars', {'accuracy'});
%         
%         T1              = subTable(:,{'cueValidity','targetID'});
%         [G,accuTable]   = findgroups(T1);
%         percFcn         = @(x)((x,'omitnan')); 
%         accuTable.perc  = splitapply(@(x1){tabulate(x1)},subTable.accuracy,G);

%         statarray = grpstats(subTable, {'cueValidity', 'targetID'}, {'mean','sem', 'tabulate'}, 'DataVars', {'reactionTime', 'accuracy'})
        
        
%         cueInvalid_subTable     = subTable( subTable{:,1} == 0 , 2:end);
%         cueValid_subTable       = subTable( subTable{:,1} == 1 , 2:end);
%         cueNeutral_subTable     = subTable( subTable{:,1} == 2 , 2:end);
%         % mean and multiply 100
%         mean(cueInvalid_subTable.accuracy) 
%         
%         total   = numel(cueInvalid_subTable.accuracy)
%         ninvalid = numel(find(cueInvalid_subTable.accuracy == 0))
%         nvalid = numel(find(cueInvalid_subTable.accuracy == 1))
%         % equal to mean OK 
%         percValid = ((total - ninvalid)/ total)*100        
        
       
%% ===== CLEAN THE DATA ===== 

        % clean earlier response time = <0
        subClean{sub} = subTable{sub};

        for n = 1:length(subClean{sub}.reactionTime)
            if subClean{sub}.accuracy(n) == 0   % --> reactionTime(n)<0 | subTable.reactionTime(n)>1.8
                subClean{sub}.reactionTime(n) = NaN;
            end
        end
        
        % write table to HD
            %create the folder if not exixstent
            outS = [root, subject{sub}, '/', NewAn];
            if ~exist(outS)
                mkdir(outS);
            end                
        
%         writetable(subClean{sub},[root, subject{sub}, '/subCleanSTRICT_', subject{sub}, '.csv']);
        writetable(subClean{sub},[outS, '/subClean_', subject{sub}, '.csv']);

        
        
%% ===== CALCULATE MEAN REACTION TIME AND ACCURACY (WITHIN SUBJECT) =====

        % you can calcualte grouping as many variableS as needed 
        % perfBF = varfun( @nanmean, subClean, 'Inputvariables', 'reactionTime', 'GroupingVariables',  {'cueValidity' 'cueDir'} );
        
        
        % _____Mean RT_____
        
        % group stats just for cueValidity
        statCueValidity{sub} = grpstats(subClean{sub}, {'cueValidity'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
        
        % group stats just for target direction
        statTarget{sub} = grpstats(subClean{sub}, {'targetID'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
        
        % group stats for cue and target
        statCueTarget{sub} = grpstats(subClean{sub}, {'cueValidity', 'targetID'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});

        
        % _____Accuracy_____
        
        % group stats just for cueValidity
%         statCueValidity{sub}= grpstats(subClean{sub}, {'cueValidity'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
            % calcualte percentage just for cueValidity
            statCueValidity{sub}.perc_Accuracy = statCueValidity{sub}.mean_accuracy .*100;
            statCueValidity{sub}.perc_sem_accu = statCueValidity{sub}.sem_accuracy .*100;
            % reorder columns
            statCueValidity{sub} = statCueValidity{sub}(:, [1:5 7 6 8]);
            
        % group stats just for target direction
%         statTarget{sub} = grpstats(subClean{sub}, {'targetID'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
            % calcualte percentage just for target
            statTarget{sub}.perc_Accuracy = statTarget{sub}.mean_accuracy .*100;
            statTarget{sub}.perc_sem_accu = statTarget{sub}.sem_accuracy .*100;
            % reorder columns
            statTarget{sub} = statTarget{sub}(:, [1:5 7 6 8]);              
        
        % group stats for cue and target
%         statCueTarget{sub} = grpstats(subClean{sub}, {'cueValidity', 'targetID'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
            % calcualte percentage for cue and target
            statCueTarget{sub}.perc_Accuracy = statCueTarget{sub}.mean_accuracy .*100;
            statCueTarget{sub}.perc_sem_accu = statCueTarget{sub}.sem_accuracy .*100;
            % reorder columns
            statCueTarget{sub} = statCueTarget{sub}(:, [1:6 8 7 9]);
            
                   
        % write tables to HD
         
        writetable(statCueValidity{sub},[outS, '/statCueValidity_',subject{sub},'.csv']);
        writetable(statTarget{sub},[outS, '/statTarget_', subject{sub}, '.csv']);
        writetable(statCueTarget{sub},[outS, '/statCueTarget_', subject{sub}, '.csv']);
        
%%
        
%% ===== RECALCULATE MEAN REACTION TIME AND ACCURACY (WITHIN SUBJECT) =====

% this is a double check if I used and understood the grpstats correctly
% ---> YES IT GIVES THE SAME RESULTS AS BEFORE

    % find Groups and calculate
    RT_groups{sub} = findgroups(subClean{sub}.cueValidity, subClean{sub}.targetID);

    RT_mean_S{sub} = splitapply(@nanmean, subClean{sub}.reactionTime, RT_groups{sub});
    
    ACC_groups{sub} = findgroups(subClean{sub}.cueValidity, subClean{sub}.targetID);

    ACC_mean_S{sub} = splitapply(@nanmean, subClean{sub}.accuracy, RT_groups{sub});

 %% ....and tripple --> check worked again ...
       
        count1 = 0;
        count2 = 0;
        count3 = 0;
        count4 = 0;
        count5 = 0;
        count6 = 0;
        
        RT{sub} = cell(6,1)
        
         for W = 1:height(subClean{sub})
            
            if subClean{sub}.cueValidity(W) == 0 & subClean{sub}.targetID(W) == 0                
                
                count1 = count1+1
                RT{sub}{1,1}(count1) = subClean{sub}.reactionTime(W); 
            
            elseif subClean{sub}.cueValidity(W) == 0 & subClean{sub}.targetID(W) == 1
                
                count2 = count2+1
                RT{sub}{2,1}(count2) = subClean{sub}.reactionTime(W);
                
            elseif subClean{sub}.cueValidity(W) == 1 & subClean{sub}.targetID(W) == 0
                
                count3 = count3+1
                RT{sub}{3,1}(count3) = subClean{sub}.reactionTime(W);
                
            elseif subClean{sub}.cueValidity(W) == 1 & subClean{sub}.targetID(W) == 1
                
                count4 = count4+1
                RT{sub}{4,1}(count4) = subClean{sub}.reactionTime(W);
                
            elseif subClean{sub}.cueValidity(W) == 2 & subClean{sub}.targetID(W) == 0
                
                count5 = count5+1
                RT{sub}{5,1}(count5) = subClean{sub}.reactionTime(W);
                
            elseif subClean{sub}.cueValidity(W) == 2 & subClean{sub}.targetID(W) == 1
                
                count6 = count6+1
                RT{sub}{6,1}(count6) = subClean{sub}.reactionTime(W);

            end        
         end
    
        
        %%
        
            for str = 1:length(RT)
                
                RT_mean(str) = nanmean(RT{str}{1});                
            end      
        
        
                       
%% ===== PLOT ACCURACY AND MEAN (WITHIN SUBJECT) WITH BACKGROUND AND FOREGROUND =====

        tp = 0.2

        % _____Mean RT_____
        
        % group variables
        G = findgroups(statCueTarget{sub}.cueValidity, statCueTarget{sub}.targetID);
%         newG = G([1 2; 3 4; 5 6;]);       
        newG = G([ 3 4; 5 6; 1 2]);
                
        
        % === bar plot ===
        
        subBarErrRT = figure;
        title(strrep(sprintf(['within ',subject{sub}, ' mean RT']),'_', ' '));
        xlabel('Valid _ Neutral _ Invalid');
        xticks([1 2 3])
        xticklabels({'Backg Foreg', 'Backg Foreg', 'Backg Foreg'});
        ylabel('RT');

                
        hold on
        
        hb = bar(statCueTarget{sub}.mean_reactionTime(newG));
        pause(tp); %pause allows the figure to be created
        
        % For each set of bars, find the centers of the bars, and write error bars
        for ib = 1:numel(hb)
            %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
            xData = hb(ib).XData+hb(ib).XOffset;
            errorbar(xData',statCueTarget{sub}.mean_reactionTime(newG(:,ib)),statCueTarget{sub}.sem_reactionTime(newG(:,ib)),'k.')
        end 
        
        hold off
              
        %save figure
        saveas(subBarErrRT, [outS, '/subBarErrRT_BF_', subject{sub}], 'svg');
        
        pause(tp); 
        
        % === line plot ===
        
        subLineErrRT = figure;
        title(strrep(sprintf(['within ',subject{sub}, ' mean RT']),'_', ' '));
        xlabel('Cue Validity');
        xlim([0.5, 3.5])
        xticks([1 2 3])
        xticklabels({'Valid', 'Neutral', 'Invalid'});
        ylabel('RT');
        
        hold on 
        
        e = errorbar(statCueTarget{sub}.mean_reactionTime(newG), statCueTarget{sub}.sem_reactionTime(newG));
        
        e(1).LineStyle = '-';
        e(1).Color = 'b'
        e(1).Marker = 's'
        e(1).MarkerSize = 20;
        e(1).MarkerEdgeColor = 'b';
        e(1).MarkerFaceColor = 'b';
        e(1).XData = e(1).XData + 0.01
        
        e(2).LineStyle = '--';
        e(2).Color = 'r'
        e(2).Marker = 's'
        e(2).MarkerSize = 20;
        e(2).MarkerEdgeColor = 'r';
        e(2).MarkerFaceColor = 'r';
        e(2).XData = e(2).XData - 0.01 
        
        legend('Background', 'Foreground', 'Location', 'NorthEast', 'fontname', 'helvetica');
        
        hold off
                
        %save figure
        saveas(subLineErrRT, [outS, '/subLineErrRT_BF_', subject{sub}], 'svg');
        
        pause(tp); 
        
        
        % _____Accuracy_____
        
        % group variables
        G = findgroups(statCueTarget{sub}.cueValidity, statCueTarget{sub}.targetID);
        newG = G([ 3 4; 5 6; 1 2]);
        
        
        % === bar plot ===
        
        subBarErrAcc = figure;
        title(strrep(sprintf(['within ',subject{sub}, ' perc. accuracy']),'_', ' '));
        xlabel('Valid _ Neutral _ Invalid');
        xticks([1 2 3])
        xticklabels({'Backg Foreg', 'Backg Foreg', 'Backg Foreg'});
        ylabel('percentage accuracy');
        
        hold on
        
        hb = bar(statCueTarget{sub}.perc_Accuracy(newG));
        pause(0.1); %pause allows the figure to be created
        
        % For each set of bars, find the centers of the bars, and write error bars
        for ib = 1:numel(hb)
            %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
            xData = hb(ib).XData+hb(ib).XOffset;
            errorbar(xData',statCueTarget{sub}.perc_Accuracy(newG(:,ib)),statCueTarget{sub}.perc_sem_accu(newG(:,ib)),'k.')
        end 
        
        hold off
        
        %save figure
        saveas(subBarErrAcc, [outS, '/subBarErrACC_BF_', subject{sub}], 'svg');
        
        pause(tp);
        
               
        % === line plot ===
        
        subLineErrACC = figure;
        title(strrep(sprintf(['within ',subject{sub}, ' perc Accuracy']),'_', ' '));
        xlabel('Cue Validity');
        xlim([0.5, 3.5])
        xticks([1 2 3])
        xticklabels({'Valid', 'Neutral', 'Invalid'});
        ylabel('percentage accuracy');
        
        hold on 
        
        e = errorbar(statCueTarget{sub}.perc_Accuracy(newG), statCueTarget{sub}.perc_sem_accu(newG));
        
        e(1).LineStyle = '-';
        e(1).Color = 'b'
        e(1).Marker = 's'
        e(1).MarkerSize = 20;
        e(1).MarkerEdgeColor = 'b';
        e(1).MarkerFaceColor = 'b';
        e(1).XData = e(1).XData + 0.01
        
        e(2).LineStyle = '--';
        e(2).Color = 'r'
        e(2).Marker = 's'
        e(2).MarkerSize = 20;
        e(2).MarkerEdgeColor = 'r';
        e(2).MarkerFaceColor = 'r';
        e(2).XData = e(2).XData - 0.01
        
        legend('Background', 'Foreground', 'Location', 'NorthEast', 'fontname', 'helvetica');
        
        hold off
        
        %save figure
        saveas(subLineErrACC, [outS, '/subLineErrACC_BF_', subject{sub}], 'svg');
                
        pause(tp);
        
        
        %% ===== PLOT ACCURACY AND MEAN (WITHIN SUBJECT) JUST FOR CUE VALIDITY =====
        
        % _____Mean RT_____
        
        % group variables
        G = findgroups(statCueValidity{sub}.cueValidity);
        newG = G([ 2; 3; 1;]);
        
        
        % === bar plot ===
        
        subBarErrRT = figure;
        title(strrep(sprintf(['within ',subject{sub}, ' mean RT']),'_', ' '));
        xlabel('Cue Validity');
        xticks([1 2 3])
        xticklabels({'Valid', 'Neutral', 'Invalid'});
                
        hold on
        
        hb = bar(statCueValidity{sub}.mean_reactionTime(newG));
        pause(0.1); %pause allows the figure to be created
        
        % For each set of bars, find the centers of the bars, and write error bars
        for ib = 1:numel(hb)
            %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
            xData = hb(ib).XData+hb(ib).XOffset;
            errorbar(xData',statCueValidity{sub}.mean_reactionTime(newG(:,ib)),statCueValidity{sub}.sem_reactionTime(newG(:,ib)),'k.')
        end 
        
        hold off
        
        %save figure
        saveas(subBarErrRT, [outS, '/subBarErrRT_', subject{sub}], 'svg');
                
        pause(tp);
        
        
        % _____Accuracy_____
        
        % group variables
        G = findgroups(statCueValidity{sub}.cueValidity);
        newG = G([ 2; 3; 1;]);       
        
        % === bar plot ===
        
        subBarErrAcc = figure;
        title(strrep(sprintf(['within ',subject{sub}, ' perc. accuracy']),'_', ' '));
        xlabel('Cue Validity');
        xticks([1 2 3])
        xticklabels({'Valid', 'Neutral', 'Invalid'});
        
        hold on
        hb = bar(statCueValidity{sub}.perc_Accuracy(newG));
        pause(0.1); %pause allows the figure to be created
        
        % For each set of bars, find the centers of the bars, and write error bars
        for ib = 1:numel(hb)
            %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
            xData = hb(ib).XData+hb(ib).XOffset;
            errorbar(xData',statCueValidity{sub}.perc_Accuracy(newG(:,ib)),statCueValidity{sub}.perc_sem_accu(newG(:,ib)),'k.')
        end
        
        hold off
        
        %save figure
        saveas(subBarErrAcc, [outS, '/subBarErrAcc_', subject{sub}], 'svg');
        
        pause(tp);
               
%% ===== OUTLIER REMOVAL =====
% try that once you've just taken out the values <0 and want a better cut
% off for the values >0
% 
%         % calculate thresholds based on all data /// add (sub) to the varaible name for the loop through subjects
%         
%         cutoff = 2; % SD units
%         %num_outliers = 
%      
%         
%         low_thresh = nanmean(subTable.reactionTime) - cutoff * nanstd(subTable.reactionTime); 
%         high_thresh = nanmean(subTable.reactionTime) + cutoff * nanstd(subTable.reactionTime); 
% 
%       
%         % notice change in directionality of signs below
%         num_outliers = (sum(subTable < low_thresh) + sum(subTable > high_thresh )) / numel(subTable); 
% 
%         subTable = subTable( subTable > low_thresh );
%         % one cdn for one sub = all values in that condition where value is
%         % above low_thresh
%         subTable = subTable( subTable < high_thresh(sub) );
%         % one cdn for one sub = all values in that condition where value is
%         % below high_thresh


       
    end
    
%% ===== SAVE THE SUBJECTS TABLES =====

    % create the folder if not exixstent
        outExp = [root, NewAn];
        if ~exist(outExp)
            mkdir(outExp);
        end

    % save raw data
    save([root, NewAn, '/subTable.mat'], 'subTable');
    
    % save cleaned data
    save([root, NewAn, '/subClean.mat'], 'subClean');
    
    % save stats data
    save([root, NewAn, '/statCueValidity.mat'], 'statCueValidity');
    save([root, NewAn,'/statTarget.mat'], 'statTarget');
    save([root, NewAn, '/statCueTarget.mat'], 'statCueTarget');    
    

%% ===== AGGREGATE DATA (ACROSS SUBJECTS) =====

%------------>>>> ---> NO! WRONG!!

    % concatenate subjects table to aggregate data 
%     expTable = vertcat(subClean{:});
    
%% ===== CALCULATE MEAN AND ACCURACY (ACROSS SUBJECTS) =====
% 
%     % you can calcualte grouping as many variableS as needed 
%     % perfBF = varfun( @nanmean, subClean, 'Inputvariables', 'reactionTime', 'GroupingVariables',  {'cueValidity' 'cueDir'} );
%     
%     % _____Mean RT_____
%         
%     % group stats just for cueValidity
%     EXPstatCueValidity  = grpstats(expTable, {'cueValidity'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
%     
%     % group stats just for target
%     EXPstatTarget = grpstats(expTable, {'targetID'}, {'mean','sem',}, 'DataVars', {'reactionTime', 'accuracy'});
% 
%     % group stats for cue and target
%     EXPstatCueTarget    = grpstats(expTable, {'cueValidity', 'targetID'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
%     
%     
%     % _____Accuracy_____
%     
%     % group stats just for cueValidity
% %     EXPstatCueValidity = grpstats(expTable, {'cueValidity'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
%         % calcualte percentage just for cueValidity
%         EXPstatCueValidity.perc_Accuracy = EXPstatCueValidity.mean_accuracy .*100;
%         EXPstatCueValidity.perc_sem_accu = EXPstatCueValidity.sem_accuracy .*100;
%         % reorder columns
%         EXPstatCueValidity = EXPstatCueValidity(:, [1:5 7 6 8]);
%         
%         
%     % group stats just for target
% %     EXPstatCueTarget = grpstats(expTable, {'targetID'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
%         % calculate percentage just for target
%         EXPstatTarget.perc_Accuracy = EXPstatTarget.mean_accuracy .*100;
%         EXPstatTarget.perc_sem_accu = EXPstatTarget.sem_accuracy .*100;
%         % reorder columns
%         EXPstatTarget = EXPstatTarget(:, [1:5 7 6 8]);
%         
% 
%     % group stats for cue and target
% %     EXPstatCueTarget = grpstats(expTable, {'cueValidity', 'targetID'}, {'mean','sem'}, 'DataVars', {'reactionTime', 'accuracy'});
%         % calcualte percentage for cue and target
%         EXPstatCueTarget.perc_Accuracy = EXPstatCueTarget.mean_accuracy .*100;
%         EXPstatCueTarget.perc_sem_accu = EXPstatCueTarget.sem_accuracy .*100;
%         % reorder columns
%         EXPstatCueTarget = EXPstatCueTarget(:, [1:6 8 7 9]);
%         
%             
%     % write tables to HD
%     writetable(EXPstatCueValidity,[root, NewAn, '/EXPstatCueValidity.csv']);
%     writetable(EXPstatTarget,[root, NewAn, '/EXPstatTarget.csv']);
%     writetable(EXPstatCueTarget,[root, NewAn, '/EXPstatCueTarget.csv']);
%     
%     % save table in mat format
%     save([root, NewAn, '/EXPstatCueValidity.mat'], 'EXPstatCueValidity');
%     save([root, NewAn, '/EXPstatTarget.mat'], 'EXPstatTarget');
%     save([root, NewAn, '/EXPstatCueTarget.mat'], 'EXPstatCueTarget');
    
%------------>>>> ---> NO! WRONG!!

%% ===== RECALCULATE MEAN AND ACCURACY (ACROSS SUBJECTS) =====

   for s = 1:length(subClean)
        
        s_mean_RT(s,:)      = statCueTarget{s}.mean_reactionTime';
        s_sem_RT(s,:)       = statCueTarget{s}.sem_reactionTime;
           
        s_perc_accuracy(s,:)        = statCueTarget{s}.perc_Accuracy';
        s_sem_perc_accuracy(s,:)    = statCueTarget{s}.perc_sem_accu;
        
   end
  
   varNames = {'IB', 'IF', 'VB', 'VF', 'NB', 'NF' };
   rowNames = {'S_01', 'S_02', 'S_03', 'S_04', 'S_05', 'S_06', 'S_07', 'S_08', 'S_09', 'S_10', 'S_11'};
   t_mean_RT = array2table(s_mean_RT, 'VariableNames', varNames, 'RowNames', rowNames );
   t_perc_accuracy = array2table(s_perc_accuracy, 'VariableNames', varNames, 'RowNames', rowNames );
   
   meanOF_t_mean_RT = mean(s_mean_RT(:,:),1, 'omitnan');
   meanOF_t_perc_accuracy = mean(s_perc_accuracy(:,:),1, 'omitnan');
   
   ws_sem_RT    = std(s_mean_rt, 'omitnan') ./ sqrt(size(s_mean_rt,1));
   ws_sem_ACC   = std(s_perc_accuracy, 'omitnan') ./ sqrt(size(s_perc_accuracy,1));
   
   EXPstatCueTarget = table;
   EXPstatCueTarget.cueValidity         = statCueTarget{s}.cueValidity;
   EXPstatCueTarget.targetID            = statCueTarget{s}.targetID;
   EXPstatCueTarget.mean_reactionTime   = meanOF_t_mean_RT';
   EXPstatCueTarget.perc_Accuracy       = meanOF_t_perc_accuracy';
   EXPstatCueTarget.sem_reactionTime    = ws_sem_RT';
   EXPstatCueTarget.perc_sem_accu       = ws_sem_ACC';   
            
        
%% ===== PLOT ACCURACY AND MEAN (ACROSS SUBJECTS) WITH BACKGROUND AND FOREGROUND =====

    % _____Mean RT_____
    
    % group variables
    G = findgroups(EXPstatCueTarget.cueValidity, EXPstatCueTarget.targetID);
    newG = G([ 3 4; 5 6; 1 2]);
    newG_BF = G([1 3 5; 2 4 6]);
    

    % === bar plot ===
    EXPsubBarErrRT = figure;
    title('Across Subjects mean RT');
    xlabel('Valid Neutral Invalid');
    xticks([1 2 3])
    xticklabels({'Backg Foreg', 'Backg Foreg', 'Backg Foreg'});
    ylabel('RT')

    
    hold on
    hb = bar(EXPstatCueTarget.mean_reactionTime(newG));
    pause(0.1); %pause allows the figure to be created

    % For each set of bars, find the centers of the bars, and write error bars
    for ib = 1:numel(hb)
        %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
        xData = hb(ib).XData+hb(ib).XOffset;
        errorbar(xData',EXPstatCueTarget.mean_reactionTime(newG(:,ib)),EXPstatCueTarget.sem_reactionTime(newG(:,ib)),'k.')
    end
    
    hold off
    
    %save figure
    saveas(EXPsubBarErrRT, [root, NewAn, '/EXPsubBarErrRT'], 'svg');
    
    pause(tp);
    
    % === line plot attentional cue ===
        
    EXPsubLineErrRT = figure;
    title('Across Subjects mean RT');
    xlabel('Cue Validity');
    xlim([0.5, 3.5])
    xticks([1 2 3])
    xticklabels({'Valid', 'Neutral', 'Invalid'});
    ylabel('RT')

    hold on 

    e = errorbar(EXPstatCueTarget.mean_reactionTime(newG), EXPstatCueTarget.sem_reactionTime(newG));
    

    e(1).LineStyle = '-';
    e(1).Color = 'b'
    e(1).Marker = 's'
    e(1).MarkerSize = 20;
    e(1).MarkerEdgeColor = 'b';
    e(1).MarkerFaceColor = 'b';
    e(1).XData = e(1).XData + 0.01

    e(2).LineStyle = '--';
    e(2).Color = 'r'
    e(2).Marker = 's'
    e(2).MarkerSize = 20;
    e(2).MarkerEdgeColor = 'r';
    e(2).MarkerFaceColor = 'r';
    e(2).XData = e(2).XData - 0.01
    
    legend('Background', 'Foreground', 'Location', 'NorthEast', 'fontname', 'helvetica');

    
    hold off

    %save figure
    saveas(EXPsubLineErrRT, [root, NewAn, '/EXPsubLineErrRT_BF_'], 'svg');
    
    pause(tp);
    
  %%
    
    % === line plot target scene ===
        
    EXPsubLineErrRT2 = figure;
    title('Across Subjects mean RT');
    xlabel('target');
    xlim([0.8, 2.2])
    xticks([1 2])
    xticklabels({'Background', 'Foreground'});
    ylabel('RT')

    hold on 

    e = errorbar(EXPstatCueTarget.mean_reactionTime(newG_BF), EXPstatCueTarget.sem_reactionTime(newG_BF));    

    e(1).LineStyle = '-';
    e(1).Color = 'b';
    e(1).Marker = 's';
    e(1).MarkerSize = 20;
    e(1).MarkerEdgeColor = 'b';
    e(1).MarkerFaceColor = 'b';
    e(1).CapSize = 10;
%     e(1).XData = e(1).XData + 0.01

    e(2).LineStyle = '--';
    e(2).Color = 'r';
    e(2).Marker = 's';
    e(2).MarkerSize = 20;
    e(2).MarkerEdgeColor = 'r';
    e(2).MarkerFaceColor = 'r';
    e(2).CapSize = 10;

%     e(2).XData = e(2).XData - 0.01
    
    e(3).LineStyle = '-.';
    e(3).Color = 'g'
    e(3).Marker = 's'
    e(3).MarkerSize = 20;
    e(3).MarkerEdgeColor = 'g';
    e(3).MarkerFaceColor = 'g';
    e(3).CapSize = 10;

%     e(3).XData = e(3).XData - 0.01
    
    legend('Invalid', 'Valid', 'Neutral', 'NorthEast', 'fontname', 'helvetica');

    
    hold off

    %save figure
    saveas(EXPsubLineErrRT2, [root, NewAn, '/EXPsubLineErrRT_BF2'], 'svg');
    
    pause(tp);
  
    %%
   
    % _____Accuracy_____
    
    % group variables
    G = findgroups(EXPstatCueTarget.cueValidity, EXPstatCueTarget.targetID);
    newG = G([ 3 4; 5 6; 1 2]);
    newG_BF = G([1 3 5; 2 4 6]);
    

    % === bar plot ===
    
    EXPsubBarErrAcc = figure;
    title('Across Subjects perc. Accuracy');
    xlabel('Valid Neutral Invalid');
    xticks([1 2 3])
    xticklabels({'Backg Foreg', 'Backg Foreg', 'Backg Foreg'});
    ylabel('perc accuracy')
    
    hold on
    
    hb = bar(EXPstatCueTarget.perc_Accuracy(newG));
    pause(0.1); %pause allows the figure to be created

    % For each set of bars, find the centers of the bars, and write error bars
    for ib = 1:numel(hb)
        %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
        xData = hb(ib).XData+hb(ib).XOffset;
        errorbar(xData',EXPstatCueTarget.perc_Accuracy(newG(:,ib)),EXPstatCueTarget.perc_sem_accu(newG(:,ib)),'k.')
    end
    
    hold off

    %save figure
    saveas(EXPsubBarErrAcc, [root, NewAn, '/EXPsubBarErrAcc'], 'svg');
   
    pause(tp);
    
    
    % === line plot ===
        
    EXPsubLineErrACC = figure;
    title('Across Subjects perc Accuracy');
    xlabel('Cue Validity');
    xlim([0.5, 3.5])
    xticks([1 2 3])
    xticklabels({'Valid', 'Neutral', 'Invalid'});
    ylabel('perc accuracy');

    hold on 

    e = errorbar(EXPstatCueTarget.perc_Accuracy(newG), EXPstatCueTarget.perc_sem_accu(newG));

    e(1).LineStyle = '-';
    e(1).Color = 'b'
    e(1).Marker = 's'
    e(1).MarkerSize = 20;
    e(1).MarkerEdgeColor = 'b';
    e(1).MarkerFaceColor = 'b';
    e(1).XData = e(1).XData + 0.01

    e(2).LineStyle = '--';
    e(2).Color = 'r'
    e(2).Marker = 's'
    e(2).MarkerSize = 20;
    e(2).MarkerEdgeColor = 'r';
    e(2).MarkerFaceColor = 'r';
    e(2).XData = e(2).XData - 0.01;
    
    legend('Background', 'Foreground', 'Location', 'NorthEast', 'fontname', 'helvetica');
    
    hold off

    %save figure
    saveas(EXPsubLineErrACC, [root, NewAn, '/EXPsubLineErrACC_BF'], 'svg');
    
    pause(tp);
        
     %%
    
    % === line plot target scene ===
        
    EXPsubLineErrACC2 = figure;
    title('Across Subjects perc accuracy');
    xlabel('target');
    xlim([0.8, 2.2])
    xticks([1 2])
    xticklabels({'Background', 'Foreground'});
    ylabel('perc accuracy')

    hold on 

    e = errorbar(EXPstatCueTarget.perc_Accuracy(newG_BF), EXPstatCueTarget.perc_sem_accu(newG_BF));    

    e(1).LineStyle = '-';
    e(1).Color = 'b';
    e(1).Marker = 's';
    e(1).MarkerSize = 20;
    e(1).MarkerEdgeColor = 'b';
    e(1).MarkerFaceColor = 'b';
    e(1).CapSize = 10;
%     e(1).XData = e(1).XData + 0.01

    e(2).LineStyle = '--';
    e(2).Color = 'r';
    e(2).Marker = 's';
    e(2).MarkerSize = 20;
    e(2).MarkerEdgeColor = 'r';
    e(2).MarkerFaceColor = 'r';
    e(2).CapSize = 10;

%     e(2).XData = e(2).XData - 0.01
    
    e(3).LineStyle = '-.';
    e(3).Color = 'g'
    e(3).Marker = 's'
    e(3).MarkerSize = 20;
    e(3).MarkerEdgeColor = 'g';
    e(3).MarkerFaceColor = 'g';
    e(3).CapSize = 10;

%     e(3).XData = e(3).XData - 0.01
    
    legend('Invalid', 'Valid', 'Neutral', 'Location', 'NorthWest', 'fontname', 'helvetica');

    
    hold off

    %save figure
    saveas(EXPsubLineErrACC2, [root, NewAn, '/EXPsubLineErrACC_BF2'], 'svg');
    
    pause(tp);
    
    %% ===== PLOT ACCURACY AND MEAN (ACROSS SUBJECTS) JUST FOR CUE VALIDITY =====

    % _____Mean RT_____
    
    % group variables
    G = findgroups(EXPstatCueValidity.cueValidity);
    newG = G([2; 3; 1;]);

    % bar plot
    EXPsubBarErrRT = figure;
    title('Across Subjects mean RT');
    xlabel('Cue Validity');
    xticks([1 2 3])
    xticklabels({'Valid', 'Neutral', 'Invalid'});
    ylabel('RT');
    
    hold on
    
    hb = bar(EXPstatCueValidity.mean_reactionTime(newG));
    pause(0.1); %pause allows the figure to be created

    % For each set of bars, find the centers of the bars, and write error bars
    for ib = 1:numel(hb)
        %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
        xData = hb(ib).XData+hb(ib).XOffset;
        errorbar(xData',EXPstatCueValidity.mean_reactionTime(newG(:,ib)),EXPstatCueValidity.sem_reactionTime(newG(:,ib)),'k.')
    end
    
    %save figure
    saveas(EXPsubBarErrRT, [root, NewAn, '/EXPsubBarErrRTSTRICT'], 'svg');
    
   
    % _____Accuracy_____
    
    % group variables
    G = findgroups(EXPstatCueValidity.cueValidity);
    newG = G([2; 3; 1;]);       

    % bar plot
    EXPsubBarErrAcc = figure;
    title('Across Subjects perc. Accuracy');
    xlabel('Cue Validity');
    xticks([1 2 3])
    xticklabels({'Valid', 'Neutral', 'Invalid'});
    ylabel('perc accuracy');
    
    hold on
    
    hb = bar(EXPstatCueValidity.perc_Accuracy(newG));
    pause(0.1); %pause allows the figure to be created

    % For each set of bars, find the centers of the bars, and write error bars
    for ib = 1:numel(hb)
        %XData property is the tick labels/group centers; XOffset is the offset of each distinct group
        xData = hb(ib).XData+hb(ib).XOffset;
        errorbar(xData',EXPstatCueValidity.perc_Accuracy(newG(:,ib)),EXPstatCueValidity.perc_sem_accu(newG(:,ib)),'k.')
    end
    
    hold off

    %save figure
    saveas(EXPsubBarErrAcc, [root, NewAn, '/EXPsubBarErrAccSTRICT'], 'svg');
    
    pause(tp);
    
    
    