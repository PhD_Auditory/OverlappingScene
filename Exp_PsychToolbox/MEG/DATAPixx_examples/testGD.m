%% SETUP Hardware
% 
% Collegare all'oscilloscopio l'uscita del fotodiodo, opportunamente posizionato sullo schermo MEG e alimentato (occhio alla batteria !!!).
% Configurare l'audio per usare il ProPixx e l'output speaker della MEG.
% Scollegare il monitor out del SoundPixx e collegarlo al trasformatore con BNC. collegare all'oscilloscopio.
%
% Lanciato lo script si pu� misurare la sincronizzazione audio-video e gli eventuali delay di compensazione.
% Si veda il logbook di laboratorio (Davide).

%% PTB setup

clear all
AssertOpenGL;
  
PsychDefaultSetup(2);

screenNumber = max(Screen('Screens'));
screenNumber = 1;

white = WhiteIndex(screenNumber); 
grey = GrayIndex(screenNumber);
black = BlackIndex(screenNumber);

PsychImaging('PrepareConfiguration');
PsychImaging('AddTask', 'General', 'UseDataPixx');
[window,screen_rect] = PsychImaging('OpenWindow', screenNumber);

flipint = Screen('GetFlipInterval', window);	% Flip interval of the monitor

HideCursor;

%% Datapixx
Datapixx('Open');
Datapixx('StopAllSchedules');
Datapixx('InitAudio');
Datapixx('SetAudioVolume', 0.25);
Datapixx('RegWrRd');
 
fs = 44100;

beep(1,:) = MakeBeep(1000, 0.25, fs);
beep(2,:) = beep(1,:);
 
Datapixx('WriteAudioBuffer', beep, 0);

nFrames = max(size(beep));
lrMode = 3;

%% Triggers
%Generare onda quadra stessa lunghezza di beep con valori fra 0 2^16 - 1 con fs = 44100 che corrisponder� ai trigger
triggerwave = zeros(size(beep,2));
triggerwave(1:882) = 1;
    
Datapixx('WriteDoutBuffer', triggerwave);

%%
% Screen('FillRect', window, black);
Screen('FillRect', window, grey ,[320 28 1600 1052]);
Screen('FillRect', window, grey);
Screen('TextSize', window, 32);
Screen('TextFont', window, 'Arial');
DrawFormattedText(window, 'Vai ...', 'center', 'center');
Screen('Flip',window);
KbStrokeWait;

%%
%    
Screen('Flip', window);
for k =0:4801
	    if ~mod(k,60)
			% Check out 2^nd argument !!!! 
			%
			% Il ProPixx, per garantire sincronizzazione al usec, funziona come segue: all'inizio del vertical refresh riceve
			% tutto il buffer video dalla scheda. Quando lo ha ricevuto, e solo quando lo ha ricevuto tutto, presenta sulla lampada a led tutta
			% l'immagine assieme, cos� evita il classico refresh dall'alto in basso dei monitor. Quindi la presentazione dello stimolo avviene
			% deterministicamente (precisione usec) al vertical refresh successivo ma senza jitter. Cos� garantisce che esiste un delay fra il
			% Screen('Flip') e l'arrivo dello stimolo, non nullo ma sempre uguale con una precisione del usec. Nel caso di 120 Hz sono 8.33 msec.
			% Facendo lo scheduling dell'audio bisogna che l'onset stimulus dell'audio sia quindi a 8.3 msec di ritardo. La compensation deriva dalla
			% misura con l'oscilloscopio che, per lo script in questione, vale 520 usec, probabilmente per questione di cavame vario.
            
            % Audio
            Datapixx('StopAllSchedules');
            Datapixx('SetAudioSchedule', flipint, fs, nFrames, lrMode, 0, nFrames);
            Datapixx('StartAudioSchedule');
            
            % Triggers
            Datapixx('StopDoutSchedule');
            Datapixx('SetDoutSchedule', flipint, fs, nFrames);
            Datapixx('StartDoutSchedule');

			Screen('FillRect',window , white);
			
			% Questa funzione sincronizza il registro software e quello hardware del ProPixx (quindi fa partire l'audio) al prossimo Vertical Refresh
			% disponibile (che non � detto che corrisponda col flip di PTB). Per far si che coincidano questa va messa subito prima del
			% Screen('Flip').
            Datapixx('RegWrVideoSync');
            
            
        else
            Screen('FillRect',window , black);
        end
        
        Screen('Flip', window);
end

%% Closings
ShowCursor;
KbStrokeWait;

sca;

Datapixx('Close');