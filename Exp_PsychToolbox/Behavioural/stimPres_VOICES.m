function [RT, expStruct] = stimPres_VOICES(expStruct, ntrials, w, scr, pahandle)    

%% ===== INPUT PARAMETERS (e.g. keyboard) ===== %%

% === KEYBOARD MAPPING ===

    % Enable unified mode of KbName, so KbName accepts identical key names on
    % all operating systems:

    KbName('UnifyKeyNames');


    % list keys of interest

    escapeKey					= KbName('Escape');  
    quitKey						= KbName('Cancel'); 
    ResponseButton				= KbName('Space');


    % whitelist of keys of interest
	
	activeKeys					= [escapeKey, quitKey, ResponseButton];
    keysOfInterest				= zeros(1,256);  
    keysOfInterest(activeKeys)	= 1;

% 	RestrictKeysForKbQueueCheck([escapeKey, ResponseButton, quitkey]);


    % put on queue

	KbQueueCreate([], keysOfInterest);
	KbQueueStart;

% I shouldn't do this again: when you have time pass it as global or as a structure!
    

%% ===== STIMULI PARAMETERS =====

    % Cue Duration
    cueDur          = 1;

    % Stimuli Duration 
    stimDur         = 5; % secs

    % More Window for Response
    responseExtra   = 1.5;

    % Inter Trial Interval
    ITI             = (1:0.05:1.5)';
    listITI         = Shuffle(repmat(ITI, round(100/length(ITI)), 1));

    % Stimulus Onset Asynchrony
    SOA             = (1:0.050:1.5)'; 
    listSOA         = Shuffle(repmat(SOA, round(100/length(SOA)), 1));


%% ===== GET READY ===== 

	% allocate the reaction time vector
    RT = NaN(100, 1); 

    % Perform one warmup trial, to get the sound hardware fully up and running,
    % performing whatever lazy initialization only happens at real first use.
    % This "useless" warmup will allow for lower latency for start of playback
    % during actual use of the audio driver in the real trials:
%     PsychPortAudio('Start', pahandle, 1);
%     PsychPortAudio('Stop', pahandle, 1);

    % Ok, now the audio hardware is fully initialized and our driver is on
    % hot-standby, ready to start playback of any sound with minimal latency.

%% ===== START THE TRIAL LOOP =====


    for t = 1:ntrials

		% === KEEP FIXATION CROSS ALL THE TIME === %

        fixcross = '+';
        Screen('TextSize', w, 60);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, scr.darkGr);
        DrawFormattedText(w, fixcross, 'center', 'center', scr.black);
		[VBLTimestamp, startFlip]= Screen('Flip', w);
		

%% ===== PREPARE THE SOUNDS AND THE BUFFER ====

        % select the cue snippet that has to be played
        cueSnippet  = transpose(expStruct(t).cueSnippet);


        % combine the sounds in a single scene
        target      = cell2mat(expStruct(t).targetSound);
        noTarget    = cell2mat(expStruct(t).noTarget);

        scene       = transpose(target + noTarget);


%% ===== CUE PRESENTATION =====
% I don't care about the construction of a cue structure like with the visual cues. Here I just retrieve the smippet prepared in the structure

		% delete previous buffer
		PsychPortAudio('DeleteBuffer');

		% fill the buffer with the cue snippet
		PsychPortAudio('FillBuffer', pahandle, cueSnippet);

		% start the audio cue
		cueOnset = PsychPortAudio('Start', pahandle, 1);

		% wait the cue duration
		WaitSecs(cueDur);

		% Stop playback:
        PsychPortAudio('Stop', pahandle, 1);
		
		% wait for the SOA
		WaitSecs(listSOA(t));

           
%% ===== PRESENT THE SOUND SCENE AND TAKE CARE OF THE TIMING FOR THE REACTION TIMES =====

        % delete previous buffer
		PsychPortAudio('DeleteBuffer');

		% fill a new buffer with the sound scene
        PsychPortAudio('FillBuffer', pahandle, scene);

		% when the sound should start with the reference of the cue 
        startTime = cueOnset + cueDur + listSOA(t)

        % start the sound and register its onset 
        soundOnset = PsychPortAudio('Start', pahandle, 1, startTime, 1) % , 1, startTime, 1
		

%% ===== RESPONSE COLLECTION LOOP =====

        % maximum time of the sound + extra response time 
		maxRespTime = soundOnset + stimDur + responseExtra

        KbQueueFlush;

        while GetSecs <= maxRespTime

           [pressed, firstPress, firstRelease, lastPress, lastRelease] = KbQueueCheck;
%            disp('Test1');

            if pressed  == 1 % KeyIsDown %pressed
                disp('Test2')

                % calculate the reaction time 
                RT(t) = (firstPress(ResponseButton) - soundOnset) - expStruct(t).targetLog % firstPress(ResponseButton) responseSecs
            end

        end % end while

        % Stop playback:
        PsychPortAudio('Stop', pahandle, 1);

        % store the reaction time in the struct 
        expStruct(t).reactionTime = RT(t);
		
		% wait for ITI
		WaitSecs(listITI(t));

        
    end % ends for      

end % ends function
