%% ResponsePixx response collection code
% 
% Features:
%			- Get the pressed response button in matlab
%			- Calculate reaction time from stimulus onset
%			- Encode pressed response button in MEG data as trigger value
%
% Note: simple synchronization version (synchronization precision between stimuli/trigger/responses is < 10 ms. No jitter)
%	
% Author:	davide.tabarelli@unitn.it
%
% Revision:	2017/01/16 - First version

%% Initialization
%
% Include this code in your initialization block
%

Datapixx('Open');               % Open DP (Datapixx)
Datapixx('StopAllSchedules');   % Stop all running schedulers
Datapixx('EnableDinDebounce');  % Set this to avoid fast oscillation in button press (if unsure ue it !)
Datapixx('SetDoutValues', 0);   % Set digital output (triggers channels) to zero
Datapixx('RegWrRd');            % Commit changes to/from DP

responseButtonsMask = 2^0 + 2^1 + 2^2 + 2^3; % A mask of unused input channels
maxResponseTime = 3;
interTrialInterval = 1;

%% Experiment loop: 
%
% Iclude this code in your experiment loop
%

for stimulusNumber = 1:9

	% Prepare response recording
    Datapixx('SetDinLog');          % Flush button log
    Datapixx('StartDinLog');        % Start response buttons recording 
    Datapixx('SetMarker');          % Set a time marker on stimulus onset for reaction times calculation
    
	% Prepare stimulus
    fprintf('Stimululs number = %d\t', stimulusNumber );
	
    Datapixx('RegWrRdVideoSync');				% These command has to be placed immediately before stimulus onset Screen('Flip')
    % Screen('Flip')                            % Stimulus onset Screen('Flip')    
    stimulusOnsetTime = Datapixx('GetMarker');  % Get real reaction times marker time 
    
    % Response collection loop
    while 1
		
        Datapixx('RegWrRd');					% Commit changes to/from DP 
        status = Datapixx('GetDinStatus');		% Read response recording log
       
        if status.newLogFrames > 0								% If a response button has been pressed (i.e: response data are available in log) ...
            [data, times] = Datapixx('ReadDinLog', 1);			% ... read it with response time.
            
            pressedButton = bitand(data, responseButtonsMask);  % Get button number (X -> red , N->blue etc.. completa)
            reactionTime = times - stimulusOnsetTime;           % Calculate reaction time (in seconds)
            
            % Log response and reaction time to matlab prompt
            fprintf('Pressed button = %d,\tReactionTime = %f\n', pressedButton, reactionTime);
            
            % Encode pressed response button in MEG data as trigger value
			% You will get this as (red -> 129 , blue->... etc.. completa) on ST101 MEG channel
            Datapixx('SetDoutValues', pressedButton + 128);
            Datapixx('RegWr');
            WaitSecs(0.01);
            Datapixx('SetDoutValues', 0);
            Datapixx('RegWr');
            
            break
			
        elseif (Datapixx('GetTime') - stimulusOnsetTime) > maxResponseTime	% Maximum response time reached. Give message and step to next trial.
            fprintf('Time elapsed.\n');
            break;
        end
        
    end

    % Wit Inter Trial Interval
    WaitSecs(interTrialInterval);
end


%% Closings
%
% Include this code in your closing block
%

Datapixx('StopDinLog');        % Stop response buttons recording
Datapixx('Close');             % Close DP
