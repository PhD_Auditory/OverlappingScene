function [exp_new, B_table, F_table, worst_rankB, best_rankF] = rankingScene(expTable)

%% ===== ANALYSIS WITH RANKED DATA =====

% take out the best foreground and the worst background

%% ===== RANK ALL EXPERIMENT TABLES =====

% rank by targetID and RT
rankedTableR_RT = sortrows(expTable,[3 7]);

% split by backgrounf and Foregroud 

    % RT
    rankedBground_RT = rankedTableR_RT(1:750,:);
    rankedFground_RT = rankedTableR_RT(751:1500,:);
    
%% ===== CLEAN FROM EASIER FOREGROUND AND HARDER BACKGROUND =====

best    = 10; 
worst   = 10;

% === background ===

    % index the rows with NaN for further analysis
    NaN_rankB = rankedBground_RT(any(ismissing(rankedBground_RT),2),:);
    
    % Create a new table that contains only the rows without missing values
    No_NaN_rankB = rmmissing(rankedBground_RT);   
    
    % best 10
    best_rankB = No_NaN_rankB(1:best,:);

    % worst 10
    worst_rankB = No_NaN_rankB(((end-worst)-1:end),:); 
    
        
% === foreground ===

    % index the rows with NaN for further analysis
    NaN_rankF = rankedFground_RT(any(ismissing(rankedFground_RT),2),:);
    
    % Create a new table that contains only the rows without missing values
    No_NaN_rankF = rmmissing(rankedFground_RT);
    
    % best 10
    best_rankF = No_NaN_rankF(1:best,:);

    % worst 10
    worst_rankF = No_NaN_rankF(((end-worst)-1:end),:); 
    
    
% === combined ===
 
    % concatenate vertically again the tables without the best foreground and the worst foreground
    B_table = No_NaN_rankB((1:end-worst),:);
    F_table = No_NaN_rankF((best+1:end),:);
    
    exp_new = [B_table; F_table; NaN_rankB; NaN_rankF];
    
%% ===== FIND HOW MANY OF THE SAME KIND ARE IN ALL THE TRIALS =====

    % best
    best_match = exp_new(contains(exp_new.targetName, [best_rankB.targetName; best_rankF.targetName]),:);
        
    % worst
    worst_match = exp_new(contains(exp_new.targetName, [worst_rankB.targetName; worst_rankF.targetName]),:);
    
    
    % No_NaN_rankB(contains(No_NaN_rankB.targetName, worst_rankB.targetName),:);
    end 
