%% ===== INITIALIZE EYELINK =====

    if strcmp(recordEyes,'y')
        
        % here following the steps in the EyelinkExample.m in Pychtoolbox
        
        % STEP 1 EYELINK
        % was opening a window with Screen and I did in the "set the
        % screen" section
                
        % STEP 2 EYELINK
        % Provide Eyelink with details about the graphics environment
        % and perform some initializations. The information is returned
        % in a structure that also contains useful defaults
        % and control codes (e.g. tracker state bit and Eyelink key values).
        el=EyelinkInitDefaults(w);
        
        % STEP 3 EYELINK
        % Initialization of the connection with the Eyelink Gazetracker.
        % exit program if this fails.
        if EyelinkInit()~= 1;
            return;
        end;
        
        % get the version of the Eyelink to which we're connected
        [v vs]=Eyelink('GetTrackerVersion');
        fprintf('Running experiment on a ''%s'' tracker.\n', vs );

        % make sure that we get gaze data from the Eyelink
        Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');
        
        % open file to record data to
        edfFile=[subjectID,'_', runID '.edf'];
        Eyelink('Openfile', edfFile); 
            
            %----------------------------------------------------------------
            %neu, ?bernommen aus EyeLinkPicture
%             if i~=0
%                 printf('Cannot create EDF file ''%s'' ', edffilename);
%                 Eyelink( 'Shutdown');
%                 return;
%             end
            Eyelink('command', 'add_file_preamble_text ''Recorded by EyelinkToolbox demo-experiment''');
            % STEP 5    
            % SET UP TRACKER CONFIGURATION
            % Setting the proper recording resolution, proper calibration type, 
            % as well as the data file content;
            Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0, width-1, height-1);
            Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, width-1, height-1);                
            % set calibration type.
            Eyelink('command', 'calibration_type = HV9');
            % set parser (conservative saccade thresholds)
            Eyelink('command', 'saccade_velocity_threshold = 35');
            Eyelink('command', 'saccade_acceleration_threshold = 9500');
            % set EDF file contents
            Eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
            Eyelink('command', 'file_sample_data  = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS');
            % set link data (used for gaze cursor)
            Eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
            Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS');
            % allow to use the big button on the eyelink gamepad to accept the 
            % calibration/drift correction target
            Eyelink('command', 'button_function 5 "accept_target_fixation"');


            % make sure we're still connected.
            if Eyelink('IsConnected')~=1
                return;
            end;


            % STEP 6
            % Prepare for Calibration
            % setup the proper calibration foreground and background colors
            el.backgroundcolour = 67;
            el.foregroundcolour = 200;     
           

     end        
     
%% ===== EYE LINK CALIBRATION =====

% STEP 4 EYELINK    
% Calibrate the eye tracker before each block            
    if strcmp(recordEyes,'y')
        % Hide the mouse cursor;
        Screen('HideCursorHelper', w);
        %Eyelink('Command', 'set_imaging_mode');
        
        % Calibrate the eye tracker
        EyelinkDoTrackerSetup(el);

        % do a final check of calibration using driftcorrection
        EyelinkDoDriftCorrection(el);

        eye_used = Eyelink('EyeAvailable');
    end
    
    
%% ===== START EYELINK RECORDING =====
    if strcmp(recordEyes,'y')
        EyeLinkimgfile= '../../EyeLink_analysis/GIFs4Eyelink/EyeLink_color.jpg';
        Eyelink('Message', 'TRIALID %d', t);
        WaitSecs(0.1); disp('eyelink1');
        %Eyelink('command', 'record_status_message "TRIAL %d of %d  %s"', t, 100);
        Eyelink('command', 'record_status_message "TRIAL %d/%d  %s"', t, ntrials, EyeLinkimgfile);
        WaitSecs(0.1); disp('eyelink2');
        Eyelink('Message', '!V IMGLOAD CENTER %s %d %d', EyeLinkimgfile, width/2, height/2);
        Eyelink('Message', '!V TRIAL_VAR attcue %d', expStruct(t).cueDir);
        WaitSecs(0.1); disp('eyelink3');
        Eyelink('Message', '!V TRIAL_VAR targetfeature %d', expStruct(t).targetID);
%         Eyelink('Message', '!V TRIAL_VAR probedelay %d', probedelay);
        WaitSecs(0.1); disp('eyelink4');
        Eyelink('Command', 'set_idle_mode');
%         Eyelink('command', 'draw_box %d %d %d %d 15', width/2-rmax, height/2-rmax, width/2+rmax, height/2+rmax);
        WaitSecs(0.1); disp('eyelink5');
        %Eyelink('command', 'draw_cross %d %d 7', rmax, rmax);
%        EyelinkDoDriftCorrection(el);
        %%Screen('Close');
        % BLANK DISPLAY
        Screen('FillRect', w, el.backgroundcolour);
        Screen('Flip', w);
        WaitSecs(.1);
        disp('eyelink6');
        %Eyelink('Command', 'set_idle_mode');
        %EyeLink('Command', 'clear_screen 0')
        %Eyelink('command', 'start_bitmap_transfer %d  %d %d %d %d', 0, 0, 0, width, height)
        WaitSecs(0.1);
        Eyelink('StartRecording', 1, 1, 1, 1);    
        WaitSecs(0.1); disp('eyelink7');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%                                     %  C I M E C  M E G -trigger: 
%                                     nFrames = 20
%                                     triggerwave = zeros(1,nFrames);
%                                     triggerwave(1:nFrames/2) = 5;    
% %                                     if DatapixxIsReady==1
%                                         Datapixx('WriteDoutBuffer', triggerwave);
%                                         Datapixx('StopDoutSchedule');
%                                         Datapixx('SetDoutSchedule', flipint, fsMEG, nFrames);
%                                         Datapixx('StartDoutSchedule');
%                                         Datapixx('RegWrVideoSync'); %Better just before new Screen flip!
% %                                     end
            
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % E Y E L I N K   Trigger for start trial
                if strcmp(recordEyes,'y')
                        Eyelink('Message', 'start_trial')
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
                
                
                
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%    PHILIPP    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





 % DATAPixx trigger
        Datapixx('StopDoutSchedule');
        %('WriteDoutBuffer', triggerwaveSOUND);
        trigger = [20 + expStruct(t).cueValidity , 0]
        Datapixx('WriteDoutBuffer', trigger);

        Datapixx('SetDoutSchedule', scr.flipint, 100, 2);
        Datapixx('StartDoutSchedule');
        
        
