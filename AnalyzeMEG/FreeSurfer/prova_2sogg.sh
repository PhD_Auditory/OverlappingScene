#!/bin/bash

# this script submit different MRI images for different subjects to reconstruct the cortical surface with FreeSurfer
# input required
# 2018.02.09 luca,ronconi

# Participant 29, host compute-0-2
i=29
qsub -q all.q@compute-0-2 -e "/mnt/storage/tier2/davmel/MAHBRA001T6F/LabData/Luca/MEG_exp/MRI/cluster_reg/"$USER".subj"$i".error.log" -o "/mnt/storage/tier2/davmel/MAHBRA001T6F/LabData/Luca/MEG_exp/MRI/cluster_reg/"$USER".subj"$i".output.log" -b y -v participant_ID=$i /home/luca.ronconi/run_fs_reconall.sh

# Participant 30, host compute-0-4
i=30
qsub -q all.q@compute-0-4 -e "/mnt/storage/tier2/davmel/MAHBRA001T6F/LabData/Luca/MEG_exp/MRI/cluster_reg/"$USER".subj"$i".error.log" -o "/mnt/storage/tier2/davmel/MAHBRA001T6F/LabData/Luca/MEG_exp/MRI/cluster_reg/"$USER".subj"$i".output.log" -b y -v participant_ID=$i /home/luca.ronconi/run_fs_reconall.sh
