function [monoScaled] = makeMono(stereo)
% This function convert the stero sounds in mono
% It does so taking in consideration to not do a mean if in one channel there are zeros neither %%% if the two channels differ of a limited number of samples. 

% AUTHOR: GIORGIO MARINATO @ CIMeC -UNIVERSITY OF TRENTO
 
%% ===== CONVERT STEREO TO MONO =====

 % _____if the sound is already in mono keep the sound like it is 
        
    if size(stereo(1,:)) == 1

        mono = stereo(:,1); 
            
  % _____if the two channles are identical or differ of a very limited number of samples just choose one channel and don't perform any mean                     
        
    elseif all(stereo(:,1) == stereo(:,2)) || length(stereo(stereo(:,1) ~= stereo(:,2)))<500 
        % 500 is a reasonable number of samples in which two channels
        % otherwise identical differ

       mono = stereo(:,1);            
 
 % _____mean of the two channles if the two channels differ working around zeros
      
    else 

%    noZero       = ~sum(stereo == 0, 2);       % Rows Without Zeros (Logical Vector)
%    mono         = sum(stereo, 2);             % Sum Across Columns
%    mono(noZero) = mono(noZero)/2;             % Divide Rows Without Zero

    mono         = sum(stereo, 2)/2;

       %array_snd_f_mono{mono} = (array_snd_f{mono}(:,1) + array_snd_f{mono}(:,2))/2

    end
    
%% ===== NORMALIZE AND SCALE =====

% _____take the rms of the signal as reference
	rmsMono = sqrt(mean(mono.^2));
	
% _____convert result to decibel scale
	rmsMono_dB = 20*log10(rmsMono/1);	
	
% _____desired level on dB scale
	R_dB = -23;
	
% _____convert the desired level to linear scale 
	R = 10^(R_dB/20);
	
% _____determine linear scaling factor 
	a = sqrt((length(mono)*R^2)/(sum(mono.^2)));
	
% _____go back and scale the amplitude of the input signal with a linear gain change factor
	monoScaled = mono*a;     
    
% _____check the rms of the scaled signal 
	rmsScaled = sqrt(mean(monoScaled.^2));
	
% _____check the result in decibel scale
	rmsScaled_dB = 20*log10(rmsScaled/1);
	
    
end
            
