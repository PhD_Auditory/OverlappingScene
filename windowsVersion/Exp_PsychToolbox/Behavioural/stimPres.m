function [RT, expStruct] = stimPres(expStruct, trial, w, grey, black, pahandle)    

%% ===== INPUT PARAMETERS (e.g. keyboard) ===== %%
% Enable unified mode of KbName, so KbName accepts identical key names on
% all operating systems:

% _____KEYBOARD MAPPING_____
    KbName('UnifyKeyNames');                   

    escapeKey = KbName('Escape');  
%     quitkey = KbName('Cancel'); 
    ResponseButton = KbName('Space');
    
%     RestrictKeysForKbCheck([escapeKey, quitkey, ResponseButton]);
       
    keysOfInterest=zeros(1,256);  
    keysOfInterest(escapeKey)=1;  
%     keysOfInterest(quitkey)=1;  
    keysOfInterest(ResponseButton)=1;
    
    KbQueueCreate([], keysOfInterest);
    KbQueueStart;
    

%% ===== STIMULI PARAMETERS =====
    
    % _____CUE DURATION_____
    cueDur = 0.500;

    % _____STIMULI DURATION_____ 
    stimDur = 5; % secs

    % _____INTER TRIAL INTERVAL_____
    ITI = (1:0.05:2)';
    listITI = Shuffle(repmat(ITI, round(100/length(ITI)), 1));
    
    % _____STIMULUS ONSET ASYNCHRONY_____    
    SOA=(0.500:0.050:0.750)'; 
    listSOA = Shuffle(repmat(SOA, round(100/length(SOA)), 1));

%% ===== STIMULI PRESENTATION ===== 

    % _____START COLLECTING ONSET AND RESPONSE_____
   
    RT = NaN(100, 1); 
    
    % Perform one warmup trial, to get the sound hardware fully up and running,
    % performing whatever lazy initialization only happens at real first use.
    % This "useless" warmup will allow for lower latency for start of playback
    % during actual use of the audio driver in the real trials:
%     PsychPortAudio('Start', pahandle, 1, 0, 1);
%     PsychPortAudio('Stop', pahandle, 1);

    % Ok, now the audio hardware is fully initialized and our driver is on
    % hot-standby, ready to start playback of any sound with minimal latency.

    for t = 1:trial;        
        
              
        % combine the sounds in a single scene            
        target      = cell2mat(expStruct(t).targetSound);
        noTarget    = cell2mat(expStruct(t).noTarget);
        
%         % make same volume between foreground and background:
%         rms_target = sqrt(mean(target.^2));
%         rms_noTarget = sqrt(mean(noTarget.^2));
%         
%         rms_ratio = rms_target/rms_noTarget;
%         
%         dBtarget    = mag2db(rms_target);
%         dBnoTarget  = mag2db(rms_noTarget);
%         
%         if rms_target >1
%             noTarget = noTarget * rms_ratio;
%             
%         else
%             target = target * rms_ratio;
%         end
        
        scene       = transpose(target + noTarget);
            
        % fill buffer
        PsychPortAudio('FillBuffer', pahandle, scene);     
        
% === Cue Presentation ===
        
        % _____VALID_____        
        if expStruct(t).cueValidity == 1  && expStruct(t).cueDir == 1    % cue foreground
            
            
            % present visual cue
            
            cue = 'FOREGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end
            
        elseif expStruct(t).cueValidity == 1  && expStruct(t).cueDir == 0
            
            % present visual cue
            
            cue = 'BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end          
            
        % _____INVALID_____ 
        elseif expStruct(t).cueValidity == 0  && expStruct(t).cueDir == 1
            
            
            % present visual cue
            
            cue = 'FOREGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end
            
        elseif expStruct(t).cueValidity == 0  && expStruct(t).cueDir == 0         
            
            % present visual cue
            
            cue = 'BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end
            
        % _____NEUTRAL_____     
        elseif expStruct(t).cueValidity == 2  && expStruct(t).cueDir == 1
            
            cue = 'FOREGROUND OR BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end
            
        elseif expStruct(t).cueValidity == 2  && expStruct(t).cueDir == 0
            
            % present visual cue
            
            cue = 'FOREGROUND OR BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end  
                
        end % if trial        
        
          % === Fixation cross during SOA === %
            fixcross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, fixcross, 'center', 'center', black);
            [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);
                while (GetSecs - fixSoaOnset)<=listSOA(t)
                end
                
%             WaitSecs(listSOA(trial));
            
        % present the sound scene and take care of the timing and the reaction times

        % when the sound should start with the reference of the cue 
        startTime = cueOnset + cueDur + listSOA(t)
        
        % onset of the sound 
        soundOnset = PsychPortAudio('Start', pahandle, 1, startTime, 1)
        
        maxStimTime = soundOnset + stimDur; 
        
        KbQueueFlush;
        
        while GetSecs <= maxStimTime
          
%              [ KeyIsDown, responseSecs, keyCode ] = KbCheck;           
               
           [pressed, firstPress, firstRelease, lastPress, lastRelease] = KbQueueCheck;                  
            disp('Test1');
                    
           if pressed  == 1 % KeyIsDown %pressed     
                disp('Test2')
                
                % calculate the reaction time 
                RT(t) = (firstPress(ResponseButton) - soundOnset) - expStruct(t).targetLog % firstPress(ResponseButton) responseSecs                   
           end
           
        end % end while
                
        % Stop playback:
        PsychPortAudio('Stop', pahandle, 1);          
        
        % store the reaction time in the struct 
        expStruct(t).reactionTime = RT(t);       
        
        % === Fixation cross during ITI === %
        fixcross = '+';
        Screen('TextSize', w, 60);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, grey);
        DrawFormattedText(w, fixcross, 'center', 'center', black);
        [VBLTimestamp, start]= Screen('Flip', w);
            while (GetSecs - start)<=listITI(t)
            end
            
        
    end % ends for      

end % ends function
