function [repo_SE, repo_SS] = AudioScene_stimRepo (outDir, ntrials)

%% ===== FILE AND DIRS =====


% === Input Folders ===

	BgroundFolder       	= ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/NewSounds2/Background/Ambient_Human_Places/5sec_cut/repSounds_enveloped_75');
	FgroundFolder       	= ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/NewSounds2/Foreground/5sec_cut/repSounds_enveloped_75');

	BcleanFolder        	= ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/NewSounds2/Background/Ambient_Human_Places/5sec_cut/clean5sec_enveloped_75');
	FcleanFolder        	= ('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/NewSounds2/Foreground/5sec_cut/clean5sec_enveloped_75');


% === Output Folders ===

	outRoot					=('/home/jnfn/Work/Projects/Auditory/OverlappingScene/sounds/NewSounds2/');
	out 					= [outRoot, outDir];
	
	if ~exist(out)
		mkdir(out);
	end
	
		
	

%% ===== READ THE SOUNDS =====

% === Extract The Repetiton Sounds ===

	% List Sounds And Logs Of Scenes With Repetition

    all_Bsounds             = dir(fullfile(BgroundFolder, '*.wav'));
    all_Blogs               = dir(fullfile(BgroundFolder, '*.txt'));

    all_Fsounds             = dir(fullfile(FgroundFolder, '*.wav'));
    all_Flogs               = dir(fullfile(FgroundFolder, '*.txt'));


	% Extract Random 10 Trials From The List Of 1000

    randPickBack            = randi(1000,10,1);
    randPickFore            = randi(1000,10,1);


	% Index The Sounds And Logs With The Previous Vector

    all_Bsounds_Array       = {all_Bsounds.name};
    all_Blogs_Array         = {all_Blogs.name};

    all_Fsounds_Array       = {all_Fsounds.name}; 
    all_Flogs_Array         = {all_Flogs.name};

    poolBsounds             = transpose(all_Bsounds_Array(randPickBack));
    poolBlogs               = transpose(all_Blogs_Array(randPickBack));

    poolFsounds             = transpose(all_Fsounds_Array(randPickFore));
    poolFlogs               = transpose(all_Flogs_Array(randPickFore));


	% Read From Hard Drive

    array_Bsounds           = cell(ntrials,1);

    array_Fsounds           = cell(ntrials,1);


    for n = 1:ntrials
        array_Bsounds{n}    = audioread(fullfile(BgroundFolder, poolBsounds{n}));
        array_Blogs(n)      = (dlmread(fullfile(BgroundFolder, poolBlogs{n}))) / 44100; %get the values in seconds

        array_Fsounds{n}    = audioread(fullfile(FgroundFolder, poolFsounds{n}));
        array_Flogs(n)      = (dlmread(fullfile(FgroundFolder, poolFlogs{n}))) / 44100;
    end

    array_Blogs             = transpose(array_Blogs);
    array_Flogs             = transpose(array_Flogs);


% === Read The Scenes Without Repetition And Build The Array Too ===

    Bclean         			= dir(fullfile(BcleanFolder, '*.wav'));
    Fclean         			= dir(fullfile(FcleanFolder, '*.wav'));


	% Replicate The Array To Have A Pool Of 1000 To Again Pick Up A Pool Of Random 10 Sounds

    all_Bclean     			= reshape(repmat({Bclean.name},[3,1]), [1200,1]);
    all_Fclean     			= reshape(repmat({Fclean.name},[50,1]), [1000,1]);


	% Index The Sounds

    poolBclean         		= all_Bclean(randPickBack);
    poolFclean         		= all_Fclean(randPickFore);


	% Read The Sounds From Hard Drive

    array_Bclean       		= cell(ntrials,1);
    array_Fclean       		= cell(ntrials,1);

    for n = 1:ntrials
    	array_Bclean{n} 	= audioread(fullfile(BcleanFolder, poolBclean{n})); 
        array_Fclean{n} 	= audioread(fullfile(FcleanFolder, poolFclean{n}));
    end

%% ===== BUILD THE STRUCTURES FOR BOTH EXPERIMENTS =====

	tarCh              		= Shuffle([zeros(5, 1); ones(5,1)]);

% === Speech - Environment Experiemnt ===

	repo_SE					= struct;

	for t = 1:ntrials

		if tarCh(t) == 0 % target in environmental

			repo_SE(t).target 		= array_Bsounds(t);
			repo_SE(t).targetName 	= poolBsounds(t);
			repo_SE(t).targetLog 	= poolBlogs(t);

			repo_SE(t).clean 		= array_Fclean(t);
			repo_SE(t).cleanName 	= poolFclean(t);

		elseif tarCh(t) == 1 % target in speech

			repo_SE(t).target 		= array_Fsounds(t);
			repo_SE(t).targetName 	= poolFsounds(t);
			repo_SE(t).targetLog 	= poolFlogs(t);

			repo_SE(t).clean 		= array_Bclean(t);
			repo_SE(t).cleanName	= poolBclean(t);
		end

	end


% === Speech Only Experiment

	repo_SS					= struct; 

	for t = 1:ntrials
		
		if t == 10
			
			repo_SS(t).target 		= array_Fsounds(t);
			repo_SS(t).targetName 	= poolFsounds(t);
			repo_SS(t).targetLog	= poolFlogs(t);

			repo_SS(t).clean		= array_Fclean(t-1);
			repo_SS(t).cleanName	= poolFclean(t-1);
			
		else

			repo_SS(t).target 		= array_Fsounds(t);
			repo_SS(t).targetName 	= poolFsounds(t);
			repo_SS(t).targetLog	= poolFlogs(t);

			repo_SS(t).clean		= array_Fclean(t+1);
			repo_SS(t).cleanName	= poolFclean(t+1);
			
		end
		
	end


%% ===== COMBINE THE SOUNDS IN A SINGLE SCENE =====

	for s = 1:ntrials

		% Speech - Environment Experiment
		repo_SE(s).scene 			= (cell2mat(repo_SE(s).target) + cell2mat(repo_SE(s).clean));

		% Speech - Speech Experiment
		repo_SS(s).scene 			= (cell2mat(repo_SS(s).target) + cell2mat(repo_SS(s).clean));

	end


%% ===== WRITE THE SOUNDS TO DISK =====

	save([out, '/', 'Speech_Environment_Struct'], 'repo_SE', '-v7.3');
	save([out, '/', 'Speech_Speech_Struct'], 'repo_SS', '-v7.3');

	for l = 1:size(repo_SE,2)

		audiowrite([out, '/', 'targetSE_', num2str(l), '.wav'], cell2mat(repo_SE(l).target), 44100);
		audiowrite([out, '/', 'cleanSE_', num2str(l), '.wav'], cell2mat(repo_SE(l).clean), 44100);		
		audiowrite([out, '/', 'sceneSE_', num2str(l), '.wav'], repo_SE(l).scene, 44100);

		audiowrite([out, '/', 'targetSS_', num2str(l), '.wav'], cell2mat(repo_SS(l).target), 44100);
		audiowrite([out, '/', 'cleanSS_', num2str(l), '.wav'], cell2mat(repo_SS(l).clean), 44100);
		audiowrite([out, '/', 'sceneSS_', num2str(l), '.wav'], repo_SS(l).scene, 44100);

	end


% writetable(dataNoSounds,[runDir,'/expData_',subjectID, runID, '.txt']);

	end
