function [soundScaled, rmsScaled_dB] = normalize(sound, R_dB)

% This function perform a rms normalization based on a desired level on dB scale.  
% Here is called from the stimPres function before playing the overlapped scene. 

% AUTHOR: GIORGIO MARINATO - CIMeC UNIVERSITY OF TRENTO

%% ===== INPUTS =====

  if ~exist('R_dB', 'var')
      R_dB = -23;
  end

%% ===== NORMALIZE AND SCALE =====

    % take the rms of the signal as reference
	rmsSound = sqrt(mean(sound.^2));
	
    % convert result to decibel scale
	rmsScene_dB = 20*log10(rmsSound/1);	
	
    % desired level on dB scale
	R_dB;
	
    % convert the desired level to linear scale 
	R = 10^(R_dB/20);
	
    % determine linear scaling factor 
	a = sqrt((length(sound)*R^2)/(sum(sound.^2)));
	
    % go back and scale the amplitude of the input signal with a linear gain change factor
	soundScaled = sound*a;
    
    
% === CHECKING ===    
    
    % check the rms of the scaled signal 
	rmsScaled = sqrt(mean(soundScaled.^2));
	
    % check the result in decibel scale
	rmsScaled_dB = 20*log10(rmsScaled/1);
    
end
	