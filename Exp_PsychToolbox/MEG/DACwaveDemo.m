 function DatapixxDacWaveDemoMultichannels(wavFilename, repetitions)
 
% AUTHOR: 	GIORGIO MARINATO @CIMeC - UNIVERSITY OF TRENTO
% DATE: 	29/05/2018
% 
% CREDITS: VPixx demo 9 http://www.vpixx.com/manuals/psychtoolbox/html/Demo9.html
%	 
% DatapixxDacWaveDemo([wavFilename=funk.wav] [, repetitions=0])
%
% Demonstrates how to play an audio waveform using the DATAPixx DAC
% (Digital to Analog Converter) subsystem.
%
% The precision DACs are capable of updating at up to 1 MHz, and can be used to
% generate arbitrary analog waveforms with an output swing of up to +-10V.
% One application of the DACs is for the generation of precise audio stimuli.
% The audio CODECs found in typical consumer electronics have serial interfaces.
% Internally, the CODEC processes the serial data with digital interpolation
% filters, a delta-sigma modulator, and an analog reconstruction filter.
% One result is that a single dataset played back on two different CODECs
% can result in slightly different analog waveforms. Using DACs eliminates this
% variation.
%
% A second issue is the CODEC group delay. CODECs introduce a delay between
% when they receive their serial data, and when their analog outputs update.
% This delay varies from one CODEC to another, and also varies within a single
% CODEC depending on waveform update rate. Using DACs eliminates this variation.
%
% Note that the DAC outputs are not intended to drive very heavy loads. It is
% possible to drive up to +-1V waveforms (like those used in PsychPortAudio)
% directly into 32 Ohm headphones, or up to +-10V waveforms into high impedance
% (eg: 400 Ohm) headphones like those used for studio quality sound. For higher
% power, or for driving 8 Ohm speaker loads, add an external headphone amplifier.
%
% Optional arguments:
%
% wavFilename = Name of .wav sounds file to load and play.
%               Otherwise the funk.wav provided with PsychToolbox is used
%				and Sample Music in the Windows stimulation PC.
%
% repetitions = Number of times to play the sound.
%               0 means play until the sun goes nova
%               (or a keypress, whichever comes first)
%
% Also see: DatapixxDacWaveStreamDemo
%
% History:
%
% Oct 1, 2009  paa     Written
% Oct 29, 2014 dml     Revised 
% May 29, 2018 grg     Revised, updated and modified.

AssertOpenGL;   % We use PTB-3

% Get the audio filenames
if nargin < 1
    wavFilename = [];
end
if isempty(wavFilename)
    wavFilename1 = [PsychtoolboxRoot 'PsychDemos' filesep 'SoundFiles' filesep 'funk.wav'];
    wavFilename2 = 'C:\Users\Public\Music\Sample Music\Sleep Away.mp3';
    wavFilename3 = 'C:\Users\Public\Music\Sample Music\Kalimba.mp3';
end

% Get the number of repetitions
if nargin < 2
    repetitions = [];
end
if isempty(repetitions)
    repetitions = 0;
end

% Sample frequency
freq = 44100

% Load the audio files
[audioData1, freq1] = audioread(wavFilename1);
[audioData2, freq2] = audioread(wavFilename2);
[audioData3, freq3] = audioread(wavFilename3);

% resample the first audio track to the sample freq. of the others

audioData1 = resample(audioData1, freq, freq1); 

[yOld, FsOld] = audioread('audio.wav');
FsNew = 44100;
yNew = resample(yOld,FsNew, FsOld);
audiowrite('audioNew.wav', yNew, FsNew);
	

% Transpose so that each row has 1 channel audioData1 is mono (= one channel) 
% audioData3 & 2 are stereo (= 2 channels each)
audioData1 = audioData1';               
audioData2 = audioData2';
audioData3 = audioData3';

% Build an audio data matrix made of 3 channels (one for each audio file) of same lenght
% here we are forceing a dirty mono and cut the longer audio tracks at the length of the shorter
audioData(1,:) = audioData1;
audioData(2,1:length(audioData1)) = audioData2(1,1:length(audioData1));
audioData(3,1:length(audioData1)) = audioData3(1,1:length(audioData1));

% set up channels, channel list and total frames
nChannels       = size(audioData, 1);
channelList     = [0:nChannels-1];
nTotalFrames    = size(audioData, 2);


% Open Datapixx, and stop any schedules which might already be running
Datapixx('Open');
Datapixx('StopAllSchedules');
Datapixx('RegWrRd');    % Synchronize DATAPixx registers to local register cache

% Download the entire waveform to the DATAPixx default DAC address of 0.
Datapixx('WriteDacBuffer', audioData, 0, channelList);

% Configure the DATAPixx to play the buffer at the correct frequency.
% If the audio file has a single channel, it will play on DAC channel 0.
% Additional audio channels will play on increasing DAC channel numbers.
if (repetitions > 0)    % Play a fixed number of reps
    Datapixx('SetDacSchedule', 0, freq, nTotalFrames*repetitions, channelList, 0, nTotalFrames);
else                    % Play forever
    Datapixx('SetDacSchedule', 0, freq, 0, channelList , 0, nTotalFrames); %[0: nChannels-1]
end

% Start the playback
Datapixx('StartDacSchedule');
Datapixx('RegWrRd');    % Synchronize DATAPixx registers to local register cache

% Wait until schedule stops, or until a key is pressed.
fprintf('\nWaveform playback starting, press any key to abort.\n');
if (exist('OCTAVE_VERSION'))
    fflush(stdout);
end
while 1
    Datapixx('RegWrRd');   % Update registers for GetDacStatus
    status = Datapixx('GetDacStatus');
    if ~status.scheduleRunning
        break;
    end
    if KbCheck
        Datapixx('StopDacSchedule');
        Datapixx('RegWrRd');    % Synchronize DATAPixx registers to local register cache
        break;
    end
end

% Show final status of DAC scheduler
fprintf('\nStatus information for DAC scheduler:\n');
Datapixx('RegWrRd');   % Update registers for GetDacStatus
disp(Datapixx('GetDacStatus'));

% Job done
Datapixx('Close');
fprintf('\nDemo completed\n\n');
