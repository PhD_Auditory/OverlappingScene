%%  ===== SNIPPET TO READ THE AUDIO TRACKS AND NORMALIZE SOUND
% that would be easier and more robust if we can access the matlab "audio
% system toolbox

%%  ===== SET THE PATH AND VARIABLES =====

    path_root           = '/home/giorgio/Projects/Auditory/OverlappingScene/sounds';
    path_s_foreground        = fullfile(path_root, 'foreground');
    path_s_background        = fullfile(path_root, 'background');
    path_s_b_closed          = fullfile(path_s_background, 'closed_space');
    path_s_b_open            = fullfile(path_s_background, 'open_space');
    path_s_b_open_artificial = fullfile(path_s_b_open, 'artificial');
    path_s_b_open_natural    = fullfile(path_s_b_open, 'natural');

    dir_sounds_foreground   = dir(fullfile(path_s_foreground,'*.wav'));
    dir_sounds_b_closed     = dir(fullfile(path_s_b_closed,'*wav'))
    dir_sounds_b_o_artif    = dir(fullfile(path_s_b_open_artificial,'*.wav'))
    dir_sounds_b_o_natural  = dir(fullfile(path_s_b_open_natural,'*.wav'))


%%  ===== READ BACKGROUND AND FOREGROUND =====

% ===== foreground =====

    for sf = 1:length(dir_sounds_foreground)
        
        sounds_f            = audioread(fullfile(path_s_foreground,dir_sounds_foreground(sf).name));
        sound_f_info        = audioinfo(fullfile(path_s_foreground,dir_sounds_foreground(sf).name));
        array_snd_f{sf}     = sounds_f;
        name_snd_f{sf}      = dir_sounds_foreground(sf).name
        info_snd_f{sf}      = sound_f_info
        
    end
    
% ===== background closed sapce =====

for sf = 1:length(dir_sounds_b_closed)
        
        sounds_b_c           = audioread(fullfile(path_s_b_closed,dir_sounds_b_closed(sf).name));
        sound_b_c_info       = audioinfo(fullfile(path_s_b_closed,dir_sounds_b_closed(sf).name));
        array_snd_b_c{sf}  = sounds_b_c;
        name_snd_b_c{sf}     = dir_sounds_b_closed(sf).name
        info_snd_b_c{sf}     = sound_b_c_info
        
    end
    
% ===== do the other open space and find a smarter way to handle this
% structures and path

%% ===== NORMALIZE AND AMPLIFY THE SOUNDS ===== 
% the data are already normalized by the audioread function between the
% values of -1 and 1 and 1.0 = 0 dBFS. let's try some tests


filename = 'birds_normMatlab.wav'

audiowrite(filename,array_snd_f{1},44100)
max
% same perceived volume
% Since I can't just say amplify the signal to reach a certain db value
% I should calcuate the db of each audio signal then multiply for the difference between the original db and the target db 

% since I don't know the math it's a matter of a couple of click in
% audacity --go fo r it for now

%%
[signal, Fs] = audioread('guitartune.wav'); % Open demo sound
spectrum = pwelch(array_snd_f{1});

figure(1)
plot(spectrum)
plot(10*log10(spectrum))
drawnow;

sound(array_snd_f{1}, 44100)

audioNorm= array_snd_f{1}*.891/max(abs(array_snd_f{1}(:)))

sound(audioNorm, 44100)

filename = 'birds_normMatlab.wav'

audiowrite(filename,audioNorm,44100) % checked with pyloudness and same as audacity. Good


audioAuda = audioread('/home/giorgio/Projects/Auditory/OverlappingScene/sounds/foreground/birds_normAudacity.wav')

figure(2)
subplot(311); plot (array_snd_f{1})
subplot(312); plot (audioNorm)
subplot(313); plot (audioAuda)



%% ===== CHANGE OF PITCH =====
% ===== 50 MS of change pitch happening randomly after 1s
% 1) Short time fourier transform
% 2) sample original array at a sequence of fractional time values,
% interpolating magnitude and fixing phases
% --> EXTEND WITH VOCODER AND THEN RESEMPLE AT ORIGINAL LENGTH
% 3) invert back the rsulted time-frequency
% 4) 


F1 = fft(originalSignal);
N = numel(F1);
F1a = F1(1:N/2);         % the lower half of the fft
F1b = F1(end:-1:N/2+1);  % the upper half of the fft - flipped "the right way around"
t1 = 1:N/2;              % indices of the lower half
t2 = 1+ (t1-1) / (1 + 2.^(2/12)); % finer sampling... will make peaks appear higher
F2a = interp1(t1, F1a, t2); % perform sampling of lower half
F2b = interp1(t1, F1b, t2); % resample upper half
F2 = [F2a F2b(end:-1:1)];   % put the two together again
shiftedSignal = ifft(F2);   % and do the inverse FFT


