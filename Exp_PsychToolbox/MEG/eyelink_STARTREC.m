function [eye_used,gotoQuit] = eyelink_startrec(el,hz)
%% STARTEYELINKREC Start eyelink recording with PTB

% check input
if ~exist('hz','var') || isempty(hz)
    hz = 1000;
end

% convert hz to string
if ~ischar(hz)
    hz_str = num2str(hz);
end

% make sure recording frequency is 1000 Hz
Eyelink('Command', ['sample_rate = ' hz_str]);
WaitSecs(0.1);

% start recording
status = Eyelink('StartRecording');
while status~=0
    WaitSecs(0.5);
    status = Eyelink('StartRecording');
end
WaitSecs(0.2);

% check if recording correctly
errorRecording = Eyelink('CheckRecording');
if(errorRecording~=0)
    warning( 'CheckRecording error, status: %d', errorRecording );
    gotoQuit = true;
else
    gotoQuit = false;
end

% get eye that's tracked
eye_used = Eyelink('EyeAvailable');
if eye_used == el.BINOCULAR; % if both eyes are tracked
    eye_used = el.LEFT_EYE;  % use left eye
end

end

 EyeLinkimgfile= '../../EyeLink_analysis/GIFs4Eyelink/EyeLink_color.jpg';
        Eyelink('Message', 'TRIALID %d', t);
        WaitSecs(0.1); disp('eyelink1');
        %Eyelink('command', 'record_status_message "TRIAL %d of %d  %s"', t, 100);
        Eyelink('command', 'record_status_message "TRIAL %d/%d  %s"', t, ntrials, EyeLinkimgfile);
        WaitSecs(0.1); disp('eyelink2');
        Eyelink('Message', '!V IMGLOAD CENTER %s %d %d', EyeLinkimgfile, scr.width/2, scr.height/2);
        Eyelink('Message', '!V TRIAL_VAR attcue %d', expStruct(t).cueDir);
        WaitSecs(0.1); disp('eyelink3');
        Eyelink('Message', '!V TRIAL_VAR targetfeature %d', expStruct(t).targetID);
%         Eyelink('Message', '!V TRIAL_VAR probedelay %d', probedelay);
        WaitSecs(0.1); disp('eyelink4');
        Eyelink('Command', 'set_idle_mode');
%         Eyelink('command', 'draw_box %d %d %d %d 15', width/2-rmax, height/2-rmax, width/2+rmax, height/2+rmax);
        WaitSecs(0.1); disp('eyelink5');
        %Eyelink('command', 'draw_cross %d %d 7', rmax, rmax);
%        EyelinkDoDriftCorrection(el);
        %%Screen('Close');
        % BLANK DISPLAY
        Screen('FillRect', w, el.backgroundcolour);
        Screen('Flip', w);
        WaitSecs(0.1);
        disp('eyelink6');
        %Eyelink('Command', 'set_idle_mode');
        %EyeLink('Command', 'clear_screen 0')
        %Eyelink('command', 'start_bitmap_transfer %d  %d %d %d %d', 0, 0, 0, width, height)
        WaitSecs(0.1);
        Eyelink('StartRecording', 1, 1, 1, 1);    
        WaitSecs(0.1); disp('eyelink7');
    end
