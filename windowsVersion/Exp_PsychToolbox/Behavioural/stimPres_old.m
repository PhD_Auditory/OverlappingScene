function [RT] = stimPres(expStruct, trial, w, grey, black, pahandle)    

%% ===== INPUT PARAMETERS (e.g. keyboard) ===== %%
% Enable unified mode of KbName, so KbName accepts identical key names on
% all operating systems:

% _____KEYBOARD MAPPING_____
    KbName('UnifyKeyNames');                   

    escapeKey = KbName('Escape');  
    quitkey = KbName('Cancel'); 
    ResponseButton = KbName('Space');
    
    RestrictKeysForKbCheck([escapeKey, quitkey, ResponseButton]);

%% ===== STIMULI PARAMETERS =====
    
    % _____CUE DURATION_____
    cueDur = 0.500;

    % _____STIMULI DURATION_____ 
    stimDur = 5; % secs

    % _____INTER TRIAL INTERVAL_____
    ITI = (1:0.05:2)';
    listITI = Shuffle(repmat(ITI, round(100/length(ITI)), 1));
    
    % _____STIMULUS ONSET ASYNCHRONY_____    
    SOA=(0.500:0.050:0.750)'; 
    listSOA = Shuffle(repmat(SOA, round(100/length(SOA)), 1));

%% ===== STIMULI PRESENTATION ===== 

    % _____START COLLECTING ONSET AND RESPONSE_____
   
    RT = zeros(100, 1);
    
    % Perform one warmup trial, to get the sound hardware fully up and running,
    % performing whatever lazy initialization only happens at real first use.
    % This "useless" warmup will allow for lower latency for start of playback
    % during actual use of the audio driver in the real trials:
%     PsychPortAudio('Start', pahandle, 1, 0, 1);
%     PsychPortAudio('Stop', pahandle, 1);

    % Ok, now the audio hardware is fully initialized and our driver is on
    % hot-standby, ready to start playback of any sound with minimal latency.

    for trial = 1:trial;        
        
         KeyIsDown = 0;
              
        % combine the sounds in a single scene            
        target      = cell2mat(expStruct(trial).targetSound);
        noTarget    = cell2mat(expStruct(trial).noTarget);            
        scene       = transpose(target + noTarget);
            
        % fill buffer
        PsychPortAudio('FillBuffer', pahandle, scene);     
        
% === Cue and Stimuli Presentation ===
        
        % _____VALID_____        
        if expStruct(trial).cueValidity == 1  && expStruct(trial).cue_dir == 1    % cue foreground
            
%             % combine the sounds in a single scene
%             
%             target      = cell2mat(expStruct(trial).targetSound);
%             noTarget    = cell2mat(expStruct(trial).noTarget);            
%             scene       = transpose(target + noTarget);                            % PsychPortAudio use the rows as audio channels, the opposite audioread/wavread 
            
            
            % present visual cue
            
            cue = 'FOREGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
            while (GetSecs - cueOnset)<=cueDur
                end
            
            % === Fixation cross during SOA === %
            fixcross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, fixcross, 'center', 'center', black);
            [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);
                while (GetSecs - fixSoaOnset)<=listSOA(trial)
                end
                
%             WaitSecs(listSOA(trial));
            
            % present the sound scene and take care of the timing and the reaction times
            
%             PsychPortAudio('FillBuffer', pahandle, scene);
            
            startTime = cueOnset + cueDur + listSOA(trial)
            soundOnset = PsychPortAudio('Start', pahandle, startTime);
            maxStimDuration = soundOnset + stimDur;
            
            %[ KeyIsDown, responseSecs, keyCode ] = KbCheck;
            
            while ~KeyIsDown && (GetSecs - soundOnset)<=maxStimDuration
                [ KeyIsDown, responseSecs, keyCode ] = KbQueueCheck;
                keyCode = find(keyCode, 1);

                if KeyIsDown
                    
                    % calculate the reaction time
                    RT(trial) = (soundOnset - responseSecs) - expStruct(trial).targetLog;
                    fprintf('"%s" reaction time respect to the target %.3f seconds\n', RT(trial));
                    if keyCode == escapeKey
                        break;
                    end

                    % If the user holds down a key, KbCheck will report multiple events.
                    % To condense multiple 'keyDown' events into a single event, we wait until all
                    % keys have been released.
                    KbReleaseWait;
                end
            end
            
            PsychPortAudio('Stop',pahandle);
            
        elseif expStruct(trial).cueValidity == 1  && expStruct(trial).cue_dir == 0
            
            % present visual cue
            
            cue = 'BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
                while (GetSecs - cueOnset)<=cueDur
                end
            
            % === Fixation cross during SOA === %
            fixcross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, fixcross, 'center', 'center', black);
            [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);
                while (GetSecs - fixSoaOnset)<=listSOA(trial)
                end
                
%             WaitSecs(listSOA(trial));
            
            % present the sound scene and take care of the timing and the reaction times
            
%             PsychPortAudio('FillBuffer', pahandle, scene);
            
            startTime = cueOnset + cueDur + listSOA(trial)
            soundOnset = PsychPortAudio('Start', pahandle, startTime);
            maxStimDuration = soundOnset + stimDur;
            
            %[ KeyIsDown, responseSecs, keyCode ] = KbCheck;
            
            while ~KeyIsDown && (GetSecs - soundOnset)<=maxStimDuration
                [ KeyIsDown, responseSecs, keyCode ] = KbCheck; 
                keyCode = find(keyCode, 1);

                if KeyIsDown
                    
                    % calculate the reaction time
                    RT(trial) = (soundOnset - responseSecs) - expStruct(trial).targetLog;
                    fprintf('"%s" reaction time respect to the target %.3f seconds\n', RT(trial));
                    if keyCode == escapeKey
                        break;
                    end

                    % If the user holds down a key, KbCheck will report multiple events.
                    % To condense multiple 'keyDown' events into a single event, we wait until all
                    % keys have been released.
                    KbReleaseWait;
                end
            end
           
            PsychPortAudio('Stop',pahandle);
            
        % _____INVALID_____ 
        elseif expStruct(trial).cueValidity == 0  && expStruct(trial).cue_dir == 1
            
%            % combine the sounds in a single scene
%             
%             target      = cell2mat(expStruct(trial).targetSound);
%             noTarget    = cell2mat(expStruct(trial).noTarget);            
%             scene       = transpose(target + noTarget);
%             
            
            % present visual cue
            
            cue = 'FOREGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
            while (GetSecs - cueOnset)<=cueDur
                end
            
            % === Fixation cross during SOA === %
            fixcross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, fixcross, 'center', 'center', black);
            [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);

                while (GetSecs - fixSoaOnset)<=listSOA(trial)
                end
                
%             WaitSecs(listSOA(trial));
            
            % present the sound scene and take care of the timing and the reaction times
            
%             PsychPortAudio('FillBuffer', pahandle, scene);
            
            startTime = cueOnset + cueDur + listSOA(trial)
            soundOnset = PsychPortAudio('Start', pahandle, startTime);
            maxStimDuration = soundOnset + stimDur;
            
            %[ KeyIsDown, responseSecs, keyCode ] = KbCheck;
            
            while ~KeyIsDown && (GetSecs - soundOnset)<=maxStimDuration
                [ KeyIsDown, responseSecs, keyCode ] = KbCheck;  
                keyCode = find(keyCode, 1);

                if KeyIsDown
                    
                    % calculate the reaction time
                    RT(trial) = (soundOnset - responseSecs) - expStruct(trial).targetLog;
                    fprintf('"%s" reaction time respect to the target %.3f seconds\n', RT(trial));
                    if keyCode == escapeKey
                        break;
                    end

                    % If the user holds down a key, KbCheck will report multiple events.
                    % To condense multiple 'keyDown' events into a single event, we wait until all
                    % keys have been released.
                    KbReleaseWait;
                end
            end
            
            PsychPortAudio('Stop',pahandle);
            
        elseif expStruct(trial).cueValidity == 0  && expStruct(trial).cue_dir == 0
            
%             % combine the sounds in a single scene
%             
%             target      = cell2mat(expStruct(trial).targetSound);
%             noTarget    = cell2mat(expStruct(trial).noTarget);            
%             scene       = transpose(target + noTarget);
%             
            
            % present visual cue
            
            cue = 'BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
            while (GetSecs - cueOnset)<=cueDur
                end
            
            % === Fixation cross during SOA === %
            fixcross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, fixcross, 'center', 'center', black);
            [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);
                while (GetSecs - fixSoaOnset)<=listSOA(trial)
                end
                
%             WaitSecs(listSOA(trial));
            
            % present the sound scene and take care of the timing and the reaction times
            
%             PsychPortAudio('FillBuffer', pahandle, scene);
            
            startTime = cueOnset + cueDur + listSOA(trial)
            soundOnset = PsychPortAudio('Start', pahandle, startTime);
            maxStimDuration = soundOnset + stimDur;
            
            %[ KeyIsDown, responseSecs, keyCode ] = KbCheck;
            
            while ~KeyIsDown && (GetSecs - soundOnset)<=maxStimDuration
                [ KeyIsDown, responseSecs, keyCode ] = KbCheck; 
                keyCode = find(keyCode, 1);

                if KeyIsDown
                    
                    % calculate the reaction time
                    RT(trial) = (soundOnset - responseSecs) - expStruct(trial).targetLog;
                    fprintf('"%s" reaction time respect to the target %.3f seconds\n', RT(trial));
                    if keyCode == escapeKey
                        break;
                    end

                    % If the user holds down a key, KbCheck will report multiple events.
                    % To condense multiple 'keyDown' events into a single event, we wait until all
                    % keys have been released.
                    KbReleaseWait;
                end
            end
            
            PsychPortAudio('Stop',pahandle);
            
        % _____NEUTRAL_____     
        elseif expStruct(trial).cueValidity == 2  && expStruct(trial).cue_dir == 1
            
%             % combine the sounds in a single scene
%             
%             target      = cell2mat(expStruct(trial).targetSound);
%             noTarget    = cell2mat(expStruct(trial).noTarget);            
%             scene       = transpose(target + noTarget);
%             
            
            % present visual cue
            
            cue = 'FOREGROUND OR BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
            while (GetSecs - cueOnset)<=cueDur
                end
            
            % === Fixation cross during SOA === %
            fixcross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, fixcross, 'center', 'center', black);
            [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);
                while (GetSecs - fixSoaOnset)<=listSOA(trial)
                end
                
%             WaitSecs(listSOA(trial));
            
            % present the sound scene and take care of the timing and the reaction times
            
%             PsychPortAudio('FillBuffer', pahandle, scene);
            
            startTime = cueOnset + cueDur + listSOA(trial)
            soundOnset = PsychPortAudio('Start', pahandle, startTime);
            maxStimDuration = soundOnset + stimDur;
            
            %[ KeyIsDown, responseSecs, keyCode ] = KbCheck;
            
            while ~KeyIsDown && (GetSecs - soundOnset)<=maxStimDuration
                [ KeyIsDown, responseSecs, keyCode ] = KbCheck; 
                keyCode = find(keyCode, 1);

                if KeyIsDown
                    
                    % calculate the reaction time
                    RT(trial) = (soundOnset - responseSecs) - expStruct(trial).targetLog;
                    fprintf('"%s" reaction time respect to the target %.3f seconds\n', RT(trial));
                    if keyCode == escapeKey
                        break;
                    end

                    % If the user holds down a key, KbCheck will report multiple events.
                    % To condense multiple 'keyDown' events into a single event, we wait until all
                    % keys have been released.
                    KbReleaseWait;
                end
            end
            
            PsychPortAudio('Stop',pahandle);
            
        elseif expStruct(trial).cueValidity == 2  && expStruct(trial).cue_dir == 0
            
%             % combine the sounds in a single scene
%             
%             target      = cell2mat(expStruct(trial).targetSound);
%             noTarget    = cell2mat(expStruct(trial).noTarget);            
%             scene       = transpose(target + noTarget);
%             
%             
            % present visual cue
            
            cue = 'FOREGROUND OR BACKGROUND';
            Screen('TextSize', w, 40);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, cue, 'center', 'center', black);
            [VBLTimestamp, cueOnset]= Screen('Flip', w);
            while (GetSecs - cueOnset)<=cueDur
                end
%             
            % === Fixation cross during SOA === %
            fixcross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, grey);
            DrawFormattedText(w, fixcross, 'center', 'center', black);
            [VBLTimestamp, fixSoaOnset]= Screen('Flip', w);
                while (GetSecs - fixSoaOnset)<=listSOA(trial)
                end
                
%             WaitSecs(listSOA(trial));
            
            % present the sound scene and take care of the timing and the reaction times
            
%             PsychPortAudio('FillBuffer', pahandle, scene);
            
            startTime = cueOnset + cueDur + listSOA(trial)
            soundOnset = PsychPortAudio('Start', pahandle, startTime);
            maxStimDuration = soundOnset + stimDur;
            
            %[ KeyIsDown, responseSecs, keyCode ] = KbCheck;
            
            while ~KeyIsDown && (GetSecs - soundOnset)<=maxStimDuration
                [ KeyIsDown, responseSecs, keyCode ] = KbCheck; 
                keyCode = find(keyCode, 1);

                if KeyIsDown
                   
                    % calculate the reaction time
                    RT(trial) = (soundOnset - responseSecs) - expStruct(trial).targetLog;
                    fprintf('"%s" reaction time respect to the target %.3f seconds\n', RT(trial));
                    if keyCode == escapeKey
                        break;
                    end

                    % If the user holds down a key, KbCheck will report multiple events.
                    % To condense multiple 'keyDown' events into a single event, we wait until all
                    % keys have been released.
                    KbReleaseWait;
                end
            end
            
            PsychPortAudio('Stop',pahandle);
            
        end % if trial
        
        expStruct(trial).reactionTime = RT;
        
        % === Fixation cross during ITI === %
        fixcross = '+';
        Screen('TextSize', w, 20);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, grey);
        DrawFormattedText(w, fixcross, 'center', 'center', black);
        [VBLTimestamp, start]= Screen('Flip', w);
            while (GetSecs - start)<=listITI(trial)
            end
        
    end % ends for 

end % ends function
