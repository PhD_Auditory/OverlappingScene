function [ soundEnveloped ] = makeEnvelope( inputSignal , toModSound, minPeakSep, envType )

% MakeEnvelope: extract the envelope from te input sound and then modulate
% the amplitude of the second input sound on the first. It makes use of the
% handy "envelope" matlab function.
% pass the minimum peak distance argument (smooth) in samples 

% AUTHOR:   Giorgio Marinato @ CIMeC - University of Trento
% DATE:     24 nov 2017

if nargin>3
  envType = validatestring(envType,{'analytic','rms','peak'});
else
  envType = 'peak';
end

%% ===== EXTRACT THE ENVELOPE FROM THE SPEECH SIGNAL =====
    
    % Use envelope function, findpeaks at minimum distance of
    % enve = envelope(inputSound, smooth, envType);
    [enve, ~] = myEnvPeak(inputSignal, minPeakSep);
    
%% ===== APPLY THE ENVELOPE MODULATION TO THE BACKGROUND SOUND =====    
    
    soundEnveloped = enve.*toModSound;
    
end


%% ===== FIRST IMPLEMENTATION WITH HILBERT =====

% figure
% subplot(3,1,1)
% plot(inputSound)
% hold on
% 
% % y = hilbert(inputSound);
% % env = abs(y);
% % plot(env)
% 
% envel = abs(inputSound); 
% subplot(3,1,2)
% plot(envel)
% 
% % toModSound = snd.background(5).monoNorm
% 
% envelopedSound = envel.*toModSound
% 
% subplot(3,1,3)
% plot(envelopedSound)
% hold off

% envelope(inputSound)
% 
% envelope(inputSound,1000, 'peak')
% 
% plot (hi)
% 
% 
% plot(t,x)
% hold on
% plot(t,[-1;1]*env,'r','LineWidth',2)
% xlim([0 0.1])
% xlabel('Seconds')
% 
% https://it.mathworks.com/help/signal/ug/envelope-extraction-using-the-analytic-signal.html


% envelope(snd.foreground(1).monoNorm,22050, 'peak')
% envelope(snd.foreground(1).monoNorm,4410, 'peak')
% plot(soundEnveloped2)
% sound(soundEnveloped2, 44100)
% plot(soundEnveloped)
% enve2 = envelope(snd.foreground(1).monoNorm,22050, 'peak')
% soundEnveloped2 = enve2.*snd.background(1).monoNorm
% 
% enve = envelope(snd.foreground(1).monoNorm,4410, 'peak')
% soundEnveloped = enve.*snd.background(1).monoNorm