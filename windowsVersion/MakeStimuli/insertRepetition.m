function [startRepeatChunk, repSound] = insertRepeatition(inputSound, Fs, repLength)
%% ===== RANDOMLY INSERT A REPETITION INSIDE AN AUDIO FILE ====
% insert the sound, the sample frequency and the desired length of the repeated segment
% N.B. the function call another custom function for randomization that ahs to be added to the path: rndNumRange

% AUTHOR: GIORGIO MARINATO @ CIMeC - University of Trento
% DATE: 26/09/2017


if length(inputSound) ~= 220500
    return        
end

startRepeatChunk    = randi([44100, (3.5*Fs)]);
chunkLength         = repLength*Fs;
stopRepeatChunk     = startRepeatChunk + chunkLength;

soundSegment        = inputSound(startRepeatChunk:stopRepeatChunk-1,:);

repSound            = [inputSound(1:stopRepeatChunk-1,:); soundSegment; inputSound(stopRepeatChunk+chunkLength:end,:)];

if length(repSound(:,1)) ~= 220500
    return
end

end