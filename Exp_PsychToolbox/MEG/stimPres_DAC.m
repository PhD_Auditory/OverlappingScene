function [RT, expStruct] = stimPres_DAC(expStruct, ntrials, w, scr, rbMask, el, pahandle)    

%% ===== INPUT PARAMETERS =====

% === CHECK FOR FUNCTION INPUT ARGUMENTS ===

    % Set defaults empty variables when Eyelink is not used
    % eyelink configuration structure
    if ~exist('el', 'var')
        el = [];
    end
    % eyelink audio handle
    if ~exist('pahndle', 'var') 
        pahandle = [];
    end

% === RECALL GLOBAL FUNCTIONS ===
    
    % Subject
    global subjectID    
    % Run
    global runID    
    % Eye tracker
    global recordEyes
    

%% ===== STIMULI PARAMETERS =====
    
    % Cue Duration
    cueDur = 0.500;

    % Stimuli Duration 
    stimDur = 5; % secs
    
    % More Window for Response
    responseExtra = 1.5;

    % Inter Trial Interval
    ITI = (1:0.05:2)';
    listITI = Shuffle(repmat(ITI, round(100/length(ITI)), 1));
    
    % Stimulus Onset Asynchrony    
    SOA=(0.500:0.050:0.750)'; 
    listSOA = Shuffle(repmat(SOA, round(100/length(SOA)), 1));
    
    % Beginning trial Meg Baseline
    BASE = (0.400:0.050:0.600)';
    listBASE = Shuffle(repmat(BASE, round(100/length(BASE)), 1));
    
    % Sample rates 
    fsMEG           = 1000;
    Fs              = 44100; 
    

%% ===== STIMULI PRESENTATION =====

    % Allocate the reaction time vector with NaN
    RT = NaN(100, 1); 

% === START TRIAL ===

    % Start trial loop
    for t = 1:ntrials; 
        
% === START EYELINK RECORDING ===
    if strcmp(recordEyes,'y')
    
%         EyeLinkimgfile= '../../EyeLink_analysis/GIFs4Eyelink/EyeLink_color.jpg';
        Eyelink('Message', 'TRIALID %d', t);
        WaitSecs(0.1); disp('eyelink1');
        %Eyelink('command', 'record_status_message "TRIAL %d of %d  %s"', t, 100);
%         Eyelink('command', 'record_status_message "TRIAL %d/%d  %s"', t, ntrials); %, EyeLinkimgfile
        WaitSecs(0.1); disp('eyelink2');
%         Eyelink('Message', '!V IMGLOAD CENTER %s %d %d',EyeLinkimgfile, scr.width/2, scr.height/2); 
        Eyelink('Message', '!V TRIAL_VAR attcue %d', expStruct(t).cueDir);
        WaitSecs(0.01); disp('eyelink3');
        Eyelink('Message', '!V TRIAL_VAR targetfeature %d', expStruct(t).targetID);
%         Eyelink('Message', '!V TRIAL_VAR probedelay %d', probedelay);
        WaitSecs(0.01); disp('eyelink4');
        Eyelink('Command', 'set_idle_mode');
%         Eyelink('command', 'draw_box %d %d %d %d 15', width/2-rmax, height/2-rmax, width/2+rmax, height/2+rmax);
        WaitSecs(0.01); disp('eyelink5');
        %Eyelink('command', 'draw_cross %d %d 7', rmax, rmax);
%         EyelinkDoDriftCorrection(el);
        %%Screen('Close');
        % BLANK DISPLAY
        Screen('FillRect', w, scr.darkGr);
        Screen('Flip', w);
        WaitSecs(0.01);
        disp('eyelink6');
        %Eyelink('Command', 'set_idle_mode');
        %EyeLink('Command', 'clear_screen 0')
        %Eyelink('command', 'start_bitmap_transfer %d  %d %d %d %d', 0, 0, 0, width, height)
        WaitSecs(0.01);
        Eyelink('StartRecording', 1, 1, 1, 1);    
        WaitSecs(0.01); disp('eyelink7');
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
%% === PREPARE THE SOUND SCENE ===   
   
% === DATAPixx RESET RESPONSE LOG AT EVERY TRIAL ===
    
        Datapixx('SetDinLog');                                  % Log button presses to default address
        Datapixx('StartDinLog');
        Datapixx('RegWrRd');
        
        
% === FIXATION CROSS FOR MEG BASELINE === %

        fixcross = '+';
        Screen('TextSize', w, 60);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, scr.darkGr);
        DrawFormattedText(w, fixcross, 'center', 'center', scr.black);
        
        
% === DATAPixx TRIGGER --> TRIAL ONSET (MEG BASELINE) ===                                   
                                    
        % make a 10ms long square wave at fs rate
        nTrigFrames                             = 20;
        triggerwaveTRIAL                        = zeros(1,nTrigFrames);
        triggerCodeTRIAL                        = 5;
        triggerwaveTRIAL(1:nTrigFrames/2)       = triggerCodeTRIAL; 
        
        % DATAPixx trigger
        Datapixx('WriteDoutBuffer', triggerwaveTRIAL);
        Datapixx('StopDoutSchedule');
        Datapixx('SetDoutSchedule', scr.flipint, fsMEG, nTrigFrames );
        Datapixx('StartDoutSchedule');
        
        % Datapixx timing
        Datapixx('SetMarker');
        Datapixx('RegWrRdVideoSync');
        
        % present fixcross for baseline and launch the trigger time
        [VBLTimestamp, fixBaseOnset] = Screen('Flip', w);    % flip screen
        trialOnsetTime = Datapixx('GetMarker');              % get datapixx timing
        
%         fprintf('triggerTRIAL = %d',triggerCodeTRIAL);
                                                 
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % E Y E L I N K   Trigger for start trial
                if strcmp(recordEyes,'y')
                        Eyelink('Message', 'start_trial') % , triggerCode 'start_trial'
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % store the datapixx trial onset time in the struct         
        expStruct(t).trialOnsetTime = trialOnsetTime;
        
        % wait empty time for MEG baseline
        WaitSecs(listBASE(t));

                   
% === CUE SELECTION === 
       
        % _____VALID_____        
        if expStruct(t).cueValidity == 1  && expStruct(t).cueDir == 1                
            
            cross = '+';
            cue = 'S';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, scr.darkGr);
            DrawFormattedText(w, cross, 'center', 'center',scr.black);
            DrawFormattedText(w, cue, 'center', scr.height/2.5, scr.black);
            
        elseif expStruct(t).cueValidity == 1  && expStruct(t).cueDir == 0
            
            cross = '+';
            cue = 'E';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, scr.darkGr);
            DrawFormattedText(w, cross, 'center', 'center',scr.black);
            DrawFormattedText(w, cue, 'center', scr.height/2.5, scr.black);      
            
        % _____INVALID_____ 
        elseif expStruct(t).cueValidity == 0  && expStruct(t).cueDir == 1            
            
            cross = '+';
            cue = 'S';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, scr.darkGr);
            DrawFormattedText(w, cross, 'center', 'center',scr.black);
            DrawFormattedText(w, cue, 'center', scr.height/2.5, scr.black);
            
        elseif expStruct(t).cueValidity == 0  && expStruct(t).cueDir == 0         
                       
            cross = '+';
            cue = 'E';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, scr.darkGr);
            DrawFormattedText(w, cross, 'center', 'center',scr.black);
            DrawFormattedText(w, cue, 'center', scr.height/2.5, scr.black);

        % _____NEUTRAL_____     
        elseif expStruct(t).cueValidity == 2  && expStruct(t).cueDir == 1
            
            cue = 'S or E';
            cross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, scr.darkGr);
            DrawFormattedText(w, cross, 'center', 'center',scr.black);
            DrawFormattedText(w, cue, 'center', scr.height/2.5, scr.black);
            
        elseif expStruct(t).cueValidity == 2  && expStruct(t).cueDir == 0
            
            cue = 'S or E';
            cross = '+';
            Screen('TextSize', w, 60);
            Screen('TextFont', w, 'Geneva');
            Screen('FillRect',w, scr.darkGr);
            DrawFormattedText(w, cross, 'center', 'center',scr.black);
            DrawFormattedText(w, cue, 'center', scr.height/2.5, scr.black);
                
        end % if cue validity presentation
        
        
% === DATAPixx TRIGGER --> CUE ONSET ===
        
        % make a 10ms long square wave at fs rate
        nTrigFrames                             = 20;
        triggerwaveCUE                          = zeros(1,nTrigFrames);
        triggerCodeCUE                          = 10 + expStruct(t).cueDir;
        triggerwaveCUE(1:nTrigFrames/2)         = triggerCodeCUE; 
               
        % DATAPixx trigger
        Datapixx('StopDoutSchedule');
        Datapixx('WriteDoutBuffer', triggerwaveCUE); 
        Datapixx('SetDoutSchedule', scr.flipint, fsMEG, nTrigFrames);
        Datapixx('StartDoutSchedule');
        
        % Datapixx timing
        % Datapixx('SetMarker');
        Datapixx('RegWrRdVideoSync');
        
                       
% === CUE PRESENTATION === 

        [VBLTimestamp, cueOnset] = Screen('Flip', w);
        cueOnsetTime = Datapixx('GetMarker');
        
%         fprintf('triggerCUE = %d',triggerCodeCUE);
                        
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % E Y E L I N K   Trigger for attentional cue onset
                if strcmp(recordEyes,'y')
                        Eyelink('Message', 'att_cue_onset') % triggerCode
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % wait for the duration of the cue
        WaitSecs(cueDur);
        
        
% === FIXATION CROSS DURING SOA === %

        fixcross = '+';
        Screen('TextSize', w, 60);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, scr.darkGr);
        DrawFormattedText(w, fixcross, 'center', 'center', scr.black);
        [VBLTimestamp, fixSoaOnset] = Screen('Flip', w);
        
        % wait for the stimulus onset asynchrony
        WaitSecs(listSOA(t));
        

% === PREPARE THE SOUND SCENE ===

        % combine the sounds in a single scene            
        speech          = cell2mat(expStruct(t).speech);
        environment     = cell2mat(expStruct(t).environment);
        
        scene           = transpose(speech + environment);
        
        % normalize to -23dB (normalization otherwise datapixx won't play)
        
        scene           = normalize(scene);
           
       
% === DATAPixx CONFIG DAC AND BUFFER FOR AUDIO SCENE ===

		% Prepare audio data with three channels with DAC DataPixx
		audioDataDAC(1,:) 	= scene;
		audioDataDAC(2,:) 	= speech;
		audioDataDAC(3,:) 	= environment;
		
		% retrieve the basic features of the sound 
        nChannels       = size(audioDataDAC, 1);
        channelList     = [0:nChannels-1];
        nTotalFrames    = size(audioDataDAC, 2);        	

        % Download the entire waveform to address 0 of the DATAPixx buffer
        Datapixx('WriteDacBuffer', audioDataDAC, 0, channelList);
             
                         
% === DATAPixx PLAY AND RESPONSE LOG === 
        
        % We want to calculate response times from the repetition appearing
        % randomly inside the audio scene. 
        % To do so we need to get a precise marker at the next vertical sync.
         
        % prepare response log
        Datapixx('SetDinLog');                                  % Flush button log
        Datapixx('StartDinLog');                                % Start response recording
        Datapixx('SetMarker');                                  % Set a time marker for reaction time calculation it read the datapixx time at the programmed screen flip 
        
        % Audio
        Datapixx('StopAllSchedules');							% stop all running schedulers
        Datapixx('SetDacSchedule', scr.flipint, Fs, nTotalFrames, channelList , 0, nTotalFrames);                            
%        Datapixx('SetAudioSchedule', scr.flipint, Fs, nTotalFrames, lrMode, 0, nTotalFrames); % here maybe i have to put startTime = cueOnsetTime + cueDur + listSOA(t) + scr.flipint;
        Datapixx('StartDacSchedule');                         	% start the scheduled audio
        
        
% === DATAPixx TRIGGER --> STIMULI ONSET ENCODE ALSO FOR CUE VALIDITY ===

        % make a 10ms long square wave at fs rate
        nTrigFrames                                 = 20;
        triggerwaveSOUND                            = zeros(1,nTrigFrames);
        triggerCodeSOUND                            = 20 + expStruct(t).cueValidity;
        triggerwaveSOUND(1:nTrigFrames/2)           = triggerCodeSOUND;
        
        % DATAPixx trigger
        Datapixx('StopDoutSchedule');
        nextBufferAddressTrigS = Datapixx('WriteDoutBuffer', triggerwaveSOUND); % nextBufferAddress
        Datapixx('SetDoutSchedule', scr.flipint, fsMEG, nTrigFrames);
        Datapixx('StartDoutSchedule');
                 
        
% === FIXATION CROSS DURING STIMULUS === %

        fixcross = '+';
        Screen('TextSize', w, 60);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, scr.darkGr);
        DrawFormattedText(w, fixcross, 'center', 'center', scr.black);
              
% === ! FLIP SCREEN ! ===

		% At the screen flip 
		% - play the audio 
		% - latch the log values to the stimulus onset
        % - send the trigger
        
        Datapixx('RegWrRdVideoSync');                          % start play the sound at the next screen flip
        [VBLTimestamp, fixStimOnset] = Screen('Flip', w);
        soundOnsetTime = Datapixx('GetMarker')                 % retrieve reaction time marker
        
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % E Y E L I N K   Trigger for stimulus onset
                if strcmp(recordEyes,'y')
                        Eyelink('Message', 'stimulus_onset') % triggercode
                end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
        % store the datapixx sound onset time in the struct 
        expStruct(t).soundOnsetTime = soundOnsetTime;
        
%             fprintf('trigger = %d', triggerCodeSOUND);
        
                disp(['Target expected at:' ])
                expStruct(t).targetLog
                
                
% === RESPONSE COLLECTION LOOP ===  

        % maximum time to wait for a response(button press)
        maxRespTime = soundOnsetTime + stimDur + responseExtra
        
        hasResponse = false;
        
        % Wait for a keypress
        while (Datapixx('GetTime') < maxRespTime)
            
            %db2
            if round(Datapixx('GetTime'),3) == round((soundOnsetTime + expStruct(t).targetLog),3)
                
% === DATAPixx TRIGGER --> TARGET ONSET ENCODE ALSO FOR BACKGROUND OR FOREGROUND ===
                
                % make a 10ms long square wave at fs rate
                nTrigFrames                             = 20;
                triggerwaveTARGET                       = zeros(1,nTrigFrames);
                triggerCodeTARGET                       = 50 + expStruct(t).targetID;
                triggerwaveTARGET(1:nTrigFrames/2)      = triggerCodeTARGET; 

                % DATAPixx trigger
                Datapixx('WriteDoutBuffer', triggerwaveTARGET);
                Datapixx('SetDoutSchedule', scr.flipint, fsMEG, nTrigFrames);
                Datapixx('StartDoutSchedule');
                Datapixx('setMarker');
                Datapixx('RegWrRd');
                targetOnsetTime = Datapixx('getMarker')
        
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % E Y E L I N K   Trigger for target onset
                    if strcmp(recordEyes,'y') 
                            Eyelink('Message', 'target_onset') % triggercode
                    end
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                 
                % store the actual dpixx time of target presentation                
                expStruct(t).dPixxTarget = targetOnsetTime - soundOnsetTime; 
                    
                WaitSecs(0.005); %just to be sure trigger isn't sent twice
                
                fprintf('trigger = %d', triggerCodeTARGET);
                
            end 
            
            if ~hasResponse
           
                Datapixx('RegWrRd');                                    % read and write changes from/to Datapixx
                buttonLogStatus = Datapixx('GetDinStatus');             % read the response log status

                if buttonLogStatus.newLogFrames > 0                     % if a response button has been pressed there are data in the log

                    [dataResp, timetag] = Datapixx('readDinLog');        % so let's read the data response and their time                
                    pressedButton = bitand(dataResp(end), rbMask);         % Get button number (1 -> red , 8-> blue, 2-> yellow, 4-> green)
    %                 Datapixx('StopDinLog');                             % stop if we don't want calculate time from when the button is released                  


                    if pressedButton > 0
                        
                        hasResponse = true;
                        
                        % Calculate reaction time (in seconds)
                        RT(t) = (timetag(end) - soundOnsetTime) - expStruct(t).targetLog;           

                        % Log response and reaction time to matlab prompt
                        fprintf('Pressed button = %d,\tReactionTime = %f\n', pressedButton, RT(t) );


    % === DATAPixx TRIGGER --> RESPONSE ONSET 

                    % Encode pressed response button in MEG data as trigger value

                        Datapixx('SetDoutValues', pressedButton + 128);
                        Datapixx('RegWr');


                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            % E Y E L I N K   Trigger for response onset
                            if strcmp(recordEyes,'y')
                                    Eyelink('Message', 'response_onset')
                            end
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                        WaitSecs(0.01);
                        Datapixx('SetDoutValues', 0);
                        Datapixx('RegWr');                 
                        fprintf('trigger = %d', pressedButton + 128);
                    end


    %             elseif (Datapixx('GetTime') - soundOnsetTime) > maxRespTime	% Maximum response time reached. Give message and step to next trial.
    %                 fprintf('Time elapsed.\n');                
                end
            else            
                Datapixx('RegWrRd');
            end
            
        end
        
        Datapixx('StopDinLog');        % Stop response buttons recording
        
        % store the reaction time in the struct 
        expStruct(t).reactionTime = RT(t);
               
        
% === FIXATION CROSS DURING ITI === %

        fixcross = '+';
        Screen('TextSize', w, 60);
        Screen('TextFont', w, 'Geneva');
        Screen('FillRect',w, scr.darkGr);
        DrawFormattedText(w, fixcross, 'center', 'center', scr.black);
        [VBLTimestamp, start]= Screen('Flip', w);
            while (GetSecs - start)<=listITI(t)
            end
            
             
    end % ends for     

end % ends function
