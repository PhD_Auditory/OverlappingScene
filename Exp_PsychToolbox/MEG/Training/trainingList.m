function [trainStruct] = trainingList(BgroundFolder, FgroundFolder)

%% ===== HAVE ALL THE SOUNDS FOR BACKGROUND AND FOREGROUND WITH THE RELATIVES LOGS IN MEMORY FROM WHICH I CAN RANDOMLY PICK UP 100 TRIALS EVERY BLOCK =====


% _____READ FROM FOLDERS AND PREPARE THE VARIABLES (CONSIDER ALSO TO READ FROM THE STRUCTURE...)_____

    ntrials = 100
    
% _____LIST SOUNDS AND LOGS OF SCENES WITH REPETITION_____
    all_Bsounds             = dir(fullfile(BgroundFolder, '*.wav'));
    all_Blogs               = dir(fullfile(BgroundFolder, '*.txt'));

    all_Fsounds             = dir(fullfile(FgroundFolder, '*.wav'));
    all_Flogs               = dir(fullfile(FgroundFolder, '*.txt')); 
    
%_____EXTRACT RANDOM 100 TRIALS FROM THE LIST OF 1000_____
    randPickBack            = randi(1000,100,1);
    randPickFore            = randi(1000,100,1);  
    
%_____INDEX THE SOUNDS AND LOGS WITH THE PREVIOUS VECTOR_____ 
    all_Bsounds_Array       = {all_Bsounds.name};
    all_Blogs_Array         = {all_Blogs.name};

    all_Fsounds_Array       = {all_Fsounds.name}; 
    all_Flogs_Array         = {all_Flogs.name};
    
    poolBsounds             = transpose(all_Bsounds_Array(randPickBack));
    poolBlogs               = transpose(all_Blogs_Array(randPickBack));
    
    poolFsounds             = transpose(all_Fsounds_Array(randPickFore));
    poolFlogs               = transpose(all_Flogs_Array(randPickFore));
    
%_____READ FROM HARD DRIVE_____     
    array_Bsounds           = cell(ntrials,1);
    %array_Blogs         = [1:100]; %cell(ntrial,1);
    
    array_Fsounds           = cell(ntrials,1);
    %array_Flogs         = [1:100];
    
    
    for n = 1:ntrials        
        array_Bsounds{n}    = audioread(fullfile(BgroundFolder, poolBsounds{n}));
        array_Blogs(n)      = (dlmread(fullfile(BgroundFolder, poolBlogs{n}))) / 44100; %get the values in seconds
        
        array_Fsounds{n}    = audioread(fullfile(FgroundFolder, poolFsounds{n}));
        array_Flogs(n)      = (dlmread(fullfile(FgroundFolder, poolFlogs{n}))) / 44100;
    end
    
    array_Blogs             = transpose(array_Blogs);
    array_Flogs             = transpose(array_Flogs);
    
    
%% ===== CREATE THE EXPERIMENTAL STRUCTURE WHERE ALL THE DATA ARE STORED (THEN STORE THE TRAINING DATA ON DISK TOO!) =====   

    trainStruct                     = struct;
    tid                           	= (1:100)';
           
% _____NOW CREATE THE CUE DIRECTION AND THE TARGET DIRECTION_____

    cueDir                        = Shuffle([zeros(50, 1); ones(50,1)]); %1 for foreground 0 for background
    
    for tid = 1:length(tid)
        trainStruct(tid).tid            = tid;
    	trainStruct(tid).cueDir        	= cueDir(tid);
    
		if cueDir(tid) == 1
		   
			targetID          = 1; % foreground
			targetName        = poolFsounds(tid);
			targetSound       = array_Fsounds(tid);
			targetLog         = array_Flogs(tid);
			
		elseif cueDir(tid) == 0

			targetID          = 0; % background
			targetName        = poolBsounds(tid);
			targetSound       = array_Bsounds(tid);
			targetLog         = array_Blogs(tid);			
        end
        
        % _____FILL UP THE REST OF THE STRUCTURE_____               
        
        trainStruct(tid).targetID         = targetID;
        trainStruct(tid).targetName       = targetName;
        trainStruct(tid).targetLog        = targetLog;
        trainStruct(tid).targetSound      = targetSound;
        
	end
	
end
