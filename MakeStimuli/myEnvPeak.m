function [enve, absSignal] = myEnvPeak(inputSignal, minPeakSep)

%% my version of matlab envelope function to obtain an envelope just interpolòating maxima peaks method

% AUTHOR: GIORGIO MARINATO @ CIMeC - UNIVERSITY OF TRENTO


%% ===== Take the absolute value =====

absSignal = abs(inputSignal);

% pre-allocate space for results
nx = size(absSignal,1);
enve = zeros(size(absSignal),'like',absSignal);

%% ===== compute envelope =====

for chan=1:size(absSignal,2)
  if nx > minPeakSep+1
    % find local maxima separated by at least N samples
    [pks,locs] = findpeaks(double(absSignal(:,chan)),'MinPeakDistance',minPeakSep);
  else
    pks = [];
  end
  
  % include the first and last points
  locs = [1; locs; nx];

  % smoothly connect the maxima via a cubic interpolation (spline not very good for smooth envelope).
  enve(:,chan) = interp1(locs,absSignal(locs,chan),(1:nx)','pchip');
end
